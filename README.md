# **WordStrap is a website theme framework that provides an enhanced integration of WordPress + Twitter Bootstrap.**

- - -

**NOTE:  WordStrap is currently in a state of active development and not ready for production use at this time.**

- - -

## Standing on the Shoulders of Giants

### Twitter Bootstrap
As its name suggests, WordStrap's foremost purpose is to provides an enhanced integration of Twitter Bootstrap with WordPress. Bootstrap is a sleek, intuitive, and powerful front-end framework for faster and easier web development. It is a widely respected and utilized development platform for standardizing the default user interface components of web applications.

### HTML5 Boilerplate
Another significant resource that is often baked into the code base of modern web projects is HMTL5 Boilerplate. It draws from the combined knowledge and effort of 100s of developers. Its lightweight package properly implements key components like jQuery, Modernizr, and Normalize.css and offers optimized server configurations to boost performance. Most importantly, it provides insight into producing HTML5/CSS3 websites.

### WordStrap (WordPress Starter Theme)
To get a jump-start on its development, WordStrap was conceived as a fork from WordStrap, a WordPress starter theme made for developers based on HTML5 Boilerplate  & Twitter Bootstrap.

#### WordStrap Theme Features:

* HTML5 Boilerplate’s markup and .htaccess
* Bootstrap from Twitter
* Theme wrapper
* Root relative URLs
* Clean URLs (no more /wp-content/)
* All static theme assets are rewritten to the website root (/assets/css/, /assets/img/, and /assets/js/)
* Cleaner HTML output of navigation menus
* Cleaner output of wp_head and enqueued scripts/styles
* Posts use the hNews microformat

### Modernizr
Modernizr is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser. Modernizr tests which native CSS3 and HTML5 features are available in the current UA and makes the results available to you in two ways: as properties on a global `Modernizr` object, and as classes on the `<html>` element. This information allows you to progressively enhance your pages with a granular level of control over the experience.

### Normalize
A modern, HTML5-ready alternative to CSS resets. Normalize.css makes browsers render all elements more consistently and in line with modern standards. It precisely targets only the styles that need normalizing.

### jQuery
jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development. jQuery is designed to change the way that you write JavaScript.

### Core Components – Info, Repositories, + Licenses:

* Twitter Bootstrap: [Website](http://twitter.github.com/bootstrap/) | [Github Repository](https://github.com/twitter/bootstrap) | [Apache 2.0 License](https://github.com/twitter/bootstrap/blob/master/LICENSE)
* HTML 5 Boilerplate: [Website](http://html5boilerplate.com/) | [Github Repository](https://github.com/h5bp/html5-boilerplate) | [MIT License](https://github.com/h5bp/html5-boilerplate/blob/master/LICENSE.md)
* WordStrap Theme: [Website](http://www.wordstraptheme.com/) | [Github Repository](https://github.com/retlehs/wordstrap) | [MIT License](https://github.com/retlehs/wordstrap/blob/master/LICENSE.md)
* Modernizr: [Website](http://modernizr.com/) | [Github Repository](https://github.com/Modernizr/Modernizr) | [MIT License](http://modernizr.com/license/)
* Normalize: [Website](http://necolas.github.com/normalize.css/) | [Github Repository](https://github.com/necolas/normalize.css) | [MIT License](https://github.com/necolas/normalize.css/blob/master/LICENSE.md)
* jQuery: [Website](http://jquery.com/) | [Github Repository](https://github.com/jquery/jquery) | [MIT License](http://jquery.org/license/)


- - -

## Additional Features

### Parent/Child Theme Compatibility
Unlike the standard version of the WordStrap Theme, **WordStrap is intended to be a parent theme**. Special thanks to Github user @leoj3n for his fork of WordStrap for adding parent/child theme compatibility.

* ROOTS Parent Theme Fork: [Github Repository](https://github.com/leoj3n/wordstrap) | [MIT License](https://github.com/leoj3n/wordstrap/blob/parent/LICENSE.md)

### The Power of LESS
LESS is a dynamic stylesheet language that extends CSS with dynamic behavior such as variables, mixins, operations and functions. The best way to leverage Bootstrap's potential is to interface with its LESS files.

* LESS CSS: [Website](http://lesscss.org/) | [Github Repository](https://github.com/cloudhead/less.js) | [Apache 2.0 License](https://github.com/cloudhead/less/blob/master/LICENSE)

### Robust Options Panel
Most of the Bootstrap LESS variables may be set via a WordStrap theme options panel in the WordPress admin. In addition, there are LESS variables particular to WordStrap. These are used to set color and/or background textures/images for the body, header, content, sidebar, and footer. By tweaking the variables in the options panel, one may significantly influence the design and style of the theme. Special thanks goes to Devin Price [@devinsays](https://github.com/devinsays/) for his brilliant options framework.

* Options Framework: [Website](http://wptheming.com/options-framework-theme/) | [Github Repository](https://github.com/devinsays/options-framework-theme) | [GNU General Public License](https://github.com/devinsays/options-framework-theme/blob/master/license.txt)

### Included Child Theme (coming soon)
WordStrap may be used as a starter theme, but since it is best-suited to serve as a parent theme, a base child theme is provided that integrates with WordStrap's options panel and LESS files. A blank LESS file is provided in the child theme for further style customizations. This file is compiled last, so it will over-ride any other settings provided the right selectors are used. Ordinary CSS is always acceptable in a LESS file, so it is completely optional to include LESS properties.

### Server-side LESS Compilation w/ Cached CSS Output
LESS files may be compiled by a developer offline and the resulting CSS file uploaded to the website's server via FTP or deployment. Alternatively, LESS files may be compiled via Javascript on the server. But neither of these methods fit WordStrap's specifications. Instead, WordStrap utilizes a compiler for LESS written in PHP called LESSphp. It is implemented by a WordPress plugin called WP-LESS. The plugin is integrated into WordStrap, so there is no need to activate it separately. These tools allow the variables set in the theme options panel to be passed through the compiler as the CSS is generated. In addition, the resulting CSS files are cached and new CSS is only generated when the theme options are updated.

**NOTE:** If the child theme LESS file is used, additions and edits to it will not automatically trigger a recompilation of the LESS files into CSS. So, it is necessary to go into the theme options panel and re-save it (even though no changes were made via the control panel). That will initiate the LESS compiler.

* LESSphp: [Website](http://leafo.net/lessphp/) | [Github Repository](https://github.com/cloudhead/less.js) | [MIT License](https://github.com/leafo/lessphp/blob/master/LICENSE)
* WP-LESS Plugin: [WordPress.org Download](http://wordpress.org/extend/plugins/wp-less/) | [Github Repository](https://github.com/oncletom/wp-less)

### Mobile Devices + Responsive Design
Websites are viewed on all sorts of devices. There are two concerns raised by this fact:

1. Browser viewport dimensions vary: 320px wide (smartphones), 768px (tablet), 1024px (laptop), 1200+ (desktop). WordStrap addresses this by enabling Bootstrap's responsive design features. Everything is in a fluid grid.
2. Some devices have high resolution displays like Apple's "retina" technology. Thus, the user interface elements needs to scale accordingly. Therefore, it is ideal if a website's design minimizes its use of bitmap GIF, JPEG, and PNG images and implement them properly when necessary. WordStrap attempts to leverage CSS3 properties via the variables in the options panel to encourage "in-browser design" as much as possible where the browser itself does the heavy-lifting in rendering the website's appearance. In addition, icon fonts are available as an alternative to using PNGs and/or sprites to display iconography. Font Awesome and a custom font created using IcoMoon's online app are included. Since they are embedded in web fonts, the icons are vector images that may scale to any size. Furthermore, their color may be altered by setting a property in the theme options panel.

### Icon Web Fonts

* Font Awesome: [Website](http://fortawesome.github.com/Font-Awesome/) | [Github Repository](https://github.com/FortAwesome/Font-Awesome) | [Creative Commons Attribution 3.0](https://github.com/FortAwesome/Font-Awesome/blob/master/README.md)
* IcoMoon: [Web App for Generating Icon Fonts](http://icomoon.io/)


- - -

## Who is WordStrap for?

WordStrap is best suited for experienced web designers and developers familiar with WordPress.


- - -

## Who is behind WordStrap?

Jason Loftis [@jloft](https://bitbucket.org/jloft) is a web designer and developer in San Diego. He has worked with WordPress since its inception in 2003.

After specializing in the creation of numerous custom WordPress themes for his freelance clients, Jason concluded that he needed a WordPress framework to serve as the code base for all his projects. There are lots of great frameworks already available, but none suited his particular ideals. WordStrap is his first attempt at forging a framework that fits his particular tastes. Perhaps other like-minded developers will like it too.