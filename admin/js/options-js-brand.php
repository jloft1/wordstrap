<?php
/** Show/Hide Javascript for the Masthead Brand Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_brand");
function optionsframework_option_brand() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for brand options
		$("#ws_brand").change(function () {
			switch ($(this).val()) {
			case "one":
				$("#section-ws_brand_mark, #section-ws_brand_logo").hide().addClass("hidden");
				$("#section-ws_brand_font_text, #section-ws_brand_font_type").show().removeClass("hidden");
				break;
			case "two":
				$("#section-ws_brand_logo").hide().addClass("hidden");
				$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").show().removeClass("hidden");
				break;
			case "three":
				$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").hide().addClass("hidden");
				$("#section-ws_brand_logo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected brand option
		if ($("#ws_brand").val() === "one") {
			$("#section-ws_brand_mark, #section-ws_brand_logo").hide().addClass("hidden");
			$("#section-ws_brand_font_text, #section-ws_brand_font_type").show().removeClass("hidden");
		}
		if ($("#ws_brand").val() === "two") {
			$("#section-ws_brand_logo").hide().addClass("hidden");
			$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").show().removeClass("hidden");
		}
		if ($("#ws_brand").val() === "three") {
			$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").hide().addClass("hidden");
			$("#section-ws_brand_logo").show().removeClass("hidden");
		}
	});
</script>

<?php
}