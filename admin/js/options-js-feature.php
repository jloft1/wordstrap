<?php
/** Show/Hide Javascript for Feature Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_feature");
function optionsframework_option_feature() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for feature options
		$("#ws_featureoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach, #section-ws_featurebackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").hide().addClass("hidden");
				$("#section-ws_featurebackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_featureupload, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurepatternsheer").hide().addClass("hidden");
				$("#section-ws_featurepatternopaque, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected feature option
		if ($("#ws_featureoption").val() === "transparent") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach, #section-ws_featurebackground").hide().addClass("hidden");
		}		
		if ($("#ws_featureoption").val() === "color") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").hide().addClass("hidden");
			$("#section-ws_featurebackground").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "gradient") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "patternsheer") {
			$("#section-ws_featureupload, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "patternopaque") {
			$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurepatternsheer").hide().addClass("hidden");
			$("#section-ws_featurepatternopaque, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "uploadsheer") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "uploadopaque") {
			$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_featureboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_featureboxshadow").val() === "none") {
			$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_featureboxshadow").val() === "shadow") {
			$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").show().removeClass("hidden");
		}	
		// custom js for the border radius
		$("#ws_featureborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_featureborderradius").val() === "none") {
			$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_featureborderradius").val() === "radius") {
			$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_featuremargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_featuremargin").val() === "default") {
			$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").hide().addClass("hidden");
		}		
		if ($("#ws_featuremargin").val() === "margin") {
			$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_featurepadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_featurepadding").val() === "default") {
			$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_featurepadding").val() === "padding") {
			$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").show().removeClass("hidden");
		}				
	});
</script>

<?php
}