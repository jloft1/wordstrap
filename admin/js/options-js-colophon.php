<?php
/** Show/Hide Javascript for Colophon Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_colophon");
function optionsframework_option_colophon() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for colophon options
		$("#ws_colophonoption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").hide().addClass("hidden");
				$("#section-ws_colophonnote, #section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "transparent":
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				break;					
			case "color":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_colophonupload, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonpatternsheer, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonpatternopaque, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected colophon option 
		if ($("#ws_colophonoption").val() === "auto") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").hide().addClass("hidden");
			$("#section-ws_colophonnote, #section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "transparent") {
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
		}				
		if ($("#ws_colophonoption").val() === "color") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "gradient") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "patternsheer") {
			$("#section-ws_colophonupload, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");			}
		if ($("#ws_colophonoption").val() === "patternopaque") {
			$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonpatternsheer, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonpatternopaque, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "uploadsheer") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "uploadopaque") {
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_colophonboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_colophonboxshadow").val() === "none") {
			$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_colophonboxshadow").val() === "shadow") {
			$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").show().removeClass("hidden");
		}	
		// custom js for the border radius
		$("#ws_colophonborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_colophonborderradius").val() === "none") {
			$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_colophonborderradius").val() === "radius") {
			$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_colophonmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_colophonmargin").val() === "default") {
			$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").hide().addClass("hidden");
		}		
		if ($("#ws_colophonmargin").val() === "margin") {
			$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_colophonpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_colophonpadding").val() === "default") {
			$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_colophonpadding").val() === "padding") {
			$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").show().removeClass("hidden");
		}				
	});
</script>

<?php
}