<?php
/** Show/Hide Javascript for Masthead Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_mast");
function optionsframework_option_mast() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for mast options
		$("#ws_mastoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach, #section-ws_mastbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").hide().addClass("hidden");
				$("#section-ws_mastbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_mastupload, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastpatternsheer").hide().addClass("hidden");
				$("#section-ws_mastpatternopaque, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected mast option
		if ($("#ws_mastoption").val() === "transparent") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach, #section-ws_mastbackground").hide().addClass("hidden");
		}		
		if ($("#ws_mastoption").val() === "color") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").hide().addClass("hidden");
			$("#section-ws_mastbackground").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "gradient") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "patternsheer") {
			$("#section-ws_mastupload, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "patternopaque") {
			$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastpatternsheer").hide().addClass("hidden");
			$("#section-ws_mastpatternopaque, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "uploadsheer") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "uploadopaque") {
			$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_mastboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_mastboxshadow").val() === "none") {
			$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_mastboxshadow").val() === "shadow") {
			$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").show().removeClass("hidden");
		}		
		// custom js for the border radius
		$("#ws_mastborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_mastborderradius").val() === "none") {
			$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_mastborderradius").val() === "radius") {
			$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_mastmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_mastmargin").val() === "default") {
			$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").hide().addClass("hidden");
		}		
		if ($("#ws_mastmargin").val() === "margin") {
			$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_mastpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_mastpadding").val() === "default") {
			$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_mastpadding").val() === "padding") {
			$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").show().removeClass("hidden");
		}			
	});
</script>

<?php
}