<?php
/** Show/Hide Javascript for Footer Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_footer");
function optionsframework_option_footer() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for footer options
		$("#ws_footeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach, #section-ws_footerbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").hide().addClass("hidden");
				$("#section-ws_footerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_footerupload, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerpatternsheer").hide().addClass("hidden");
				$("#section-ws_footerpatternopaque, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected footer option
		if ($("#ws_footeroption").val() === "transparent") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach, #section-ws_footerbackground").hide().addClass("hidden");
		}		
		if ($("#ws_footeroption").val() === "color") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").hide().addClass("hidden");
			$("#section-ws_footerbackground").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "gradient") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "patternsheer") {
			$("#section-ws_footerupload, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "patternopaque") {
			$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerpatternsheer").hide().addClass("hidden");
			$("#section-ws_footerpatternopaque, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "uploadsheer") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "uploadopaque") {
			$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_footerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_footerboxshadow").val() === "none") {
			$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_footerboxshadow").val() === "shadow") {
			$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_footerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_footerborderradius").val() === "none") {
			$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_footerborderradius").val() === "radius") {
			$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_footermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_footermargin").val() === "default") {
			$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").hide().addClass("hidden");
		}		
		if ($("#ws_footermargin").val() === "margin") {
			$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_footerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_footerpadding").val() === "default") {
			$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_footerpadding").val() === "padding") {
			$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").show().removeClass("hidden");
		}					
	});
</script>

<?php
}