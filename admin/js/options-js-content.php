<?php
/** Show/Hide Javascript for Content Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_content");
function optionsframework_option_content() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for content options
		$("#ws_contentoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach, #section-ws_contentbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").hide().addClass("hidden");
				$("#section-ws_contentbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_contentupload, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentpatternsheer").hide().addClass("hidden");
				$("#section-ws_contentpatternopaque, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected content option
		if ($("#ws_contentoption").val() === "transparent") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach, #section-ws_contentbackground").hide().addClass("hidden");
		}		
		if ($("#ws_contentoption").val() === "color") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").hide().addClass("hidden");
			$("#section-ws_contentbackground").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "gradient") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "patternsheer") {
			$("#section-ws_contentupload, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "patternopaque") {
			$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentpatternsheer").hide().addClass("hidden");
			$("#section-ws_contentpatternopaque, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "uploadsheer") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "uploadopaque") {
			$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_contentboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_contentboxshadow").val() === "none") {
			$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_contentboxshadow").val() === "shadow") {
			$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").show().removeClass("hidden");
		}	
		// custom js for the border radius
		$("#ws_contentborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_contentborderradius").val() === "none") {
			$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_contentborderradius").val() === "radius") {
			$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_contentmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_contentmargin").val() === "default") {
			$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").hide().addClass("hidden");
		}		
		if ($("#ws_contentmargin").val() === "margin") {
			$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_contentpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_contentpadding").val() === "default") {
			$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_contentpadding").val() === "padding") {
			$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").show().removeClass("hidden");
		}				
	});
</script>

<?php
}