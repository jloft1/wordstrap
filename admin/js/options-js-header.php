<?php
/** Show/Hide Javascript for Header Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_header");
function optionsframework_option_header() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for header options
		$("#ws_headeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach, #section-ws_headerbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").hide().addClass("hidden");
				$("#section-ws_headerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_headerupload, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerpatternsheer").hide().addClass("hidden");
				$("#section-ws_headerpatternopaque, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#ws_headeroption").val() === "transparent") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach, #section-ws_headerbackground").hide().addClass("hidden");
		}		
		if ($("#ws_headeroption").val() === "color") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").hide().addClass("hidden");
			$("#section-ws_headerbackground").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "gradient") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "patternsheer") {
			$("#section-ws_headerupload, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "patternopaque") {
			$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerpatternsheer").hide().addClass("hidden");
			$("#section-ws_headerpatternopaque, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "uploadsheer") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "uploadopaque") {
			$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_headerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_headerboxshadow").val() === "none") {
			$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_headerboxshadow").val() === "shadow") {
			$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").show().removeClass("hidden");
		}	
		// custom js for the border radius
		$("#ws_headerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_headerborderradius").val() === "none") {
			$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_headerborderradius").val() === "radius") {
			$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_headermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_headermargin").val() === "default") {
			$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").hide().addClass("hidden");
		}		
		if ($("#ws_headermargin").val() === "margin") {
			$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_headerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_headerpadding").val() === "default") {
			$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_headerpadding").val() === "padding") {
			$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").show().removeClass("hidden");
		}				
	});
</script>

<?php
}