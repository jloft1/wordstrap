<?php
/** Show/Hide Javascript for Body Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_body");
function optionsframework_option_body() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for body options
		$("#ws_bodyoption").change(function () {
			switch ($(this).val()) {
			case "color":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").hide().addClass("hidden");
				$("#section-ws_bodybackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_bodyupload, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodypatternsheer").hide().addClass("hidden");
				$("#section-ws_bodypatternopaque, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected body option 
		if ($("#ws_bodyoption").val() === "color") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").hide().addClass("hidden");
			$("#section-ws_bodybackground").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "gradient") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "patternsheer") {
			$("#section-ws_bodyupload, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "patternopaque") {
			$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodypatternsheer").hide().addClass("hidden");
			$("#section-ws_bodypatternopaque, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "uploadsheer") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "uploadopaque") {
			$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}