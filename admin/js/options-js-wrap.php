<?php
/** Show/Hide Javascript for Wrapper Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_wrap");
function optionsframework_option_wrap() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for wrap options
		$("#ws_wrapoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach, #section-ws_wrapbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").hide().addClass("hidden");
				$("#section-ws_wrapbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_wrapupload, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wrappatternsheer").hide().addClass("hidden");
				$("#section-ws_wrappatternopaque, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected wrap option
		if ($("#ws_wrapoption").val() === "transparent") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach, #section-ws_wrapbackground").hide().addClass("hidden");
		}		
		if ($("#ws_wrapoption").val() === "color") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").hide().addClass("hidden");
			$("#section-ws_wrapbackground").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "gradient") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "patternsheer") {
			$("#section-ws_wrapupload, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "patternopaque") {
			$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wrappatternsheer").hide().addClass("hidden");
			$("#section-ws_wrappatternopaque, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "uploadsheer") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "uploadopaque") {
			$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_wrapboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_wrapboxshadow").val() === "none") {
			$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_wrapboxshadow").val() === "shadow") {
			$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").show().removeClass("hidden");
		}	
		// custom js for the border radius
		$("#ws_wrapborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_wrapborderradius").val() === "none") {
			$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_wrapborderradius").val() === "radius") {
			$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_wrapmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_wrapmargin").val() === "default") {
			$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").hide().addClass("hidden");
		}		
		if ($("#ws_wrapmargin").val() === "margin") {
			$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_wrappadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_wrappadding").val() === "default") {
			$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_wrappadding").val() === "padding") {
			$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").show().removeClass("hidden");
		}												
	});
</script>

<?php
}