<?php
/** Show/Hide Javascript for Navbar Brand Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_nav");
function optionsframework_option_nav() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for navbar bkgd options
		$("#ws_navbaroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach, #section-ws_navbarbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").hide().addClass("hidden");
				$("#section-ws_navbarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_navbarupload, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarpatternsheer").hide().addClass("hidden");
				$("#section-ws_navbarpatternopaque, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar option
		if ($("#ws_navbaroption").val() === "transparent") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach, #section-ws_navbarbackground").hide().addClass("hidden");
		}		
		if ($("#ws_navbaroption").val() === "color") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").hide().addClass("hidden");
			$("#section-ws_navbarbackground").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "gradient") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "patternsheer") {
			$("#section-ws_navbarupload, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "patternopaque") {
			$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarpatternsheer").hide().addClass("hidden");
			$("#section-ws_navbarpatternopaque, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "uploadsheer") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "uploadopaque") {
			$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		// custom js for navbar brand options		
		$("#ws_navbarbrand").change(function () {
			switch ($(this).val()) {
			case "masthead-brand":
				$("#section-ws_navbarbrandlogo").hide().addClass("hidden");
				break;			
			case "navbar-brand":
				$("#section-ws_navbarbrandlogo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#ws_navbarbrand").val() === "masthead-brand") {
			$("#section-ws_navbarbrandlogo").hide().addClass("hidden");
		}		
		if ($("#ws_navbarbrand").val() === "navbar-brand") {
			$("#section-ws_navbarbrandlogo").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_navbarboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_navbarboxshadow").val() === "none") {
			$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").hide().addClass("hidden");
		}		
		if ($("#ws_navbarboxshadow").val() === "shadow") {
			$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").show().removeClass("hidden");
		}		
		// custom js for the border radius
		$("#ws_navbarborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_navbarborderradius").val() === "none") {
			$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").hide().addClass("hidden");
		}		
		if ($("#ws_navbarborderradius").val() === "radius") {
			$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_navbarmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_navbarmargin").val() === "default") {
			$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").hide().addClass("hidden");
		}		
		if ($("#ws_navbarmargin").val() === "margin") {
			$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_navbarpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_navbarpadding").val() === "default") {
			$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").hide().addClass("hidden");
		}		
		if ($("#ws_navbarpadding").val() === "padding") {
			$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").show().removeClass("hidden");
		}			
	});
</script>

<?php
}