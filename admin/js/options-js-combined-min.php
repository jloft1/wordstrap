<?php
 add_action("optionsframework_custom_scripts", "optionsframework_option_body"); function optionsframework_option_body() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for body options
		$("#ws_bodyoption").change(function () {
			switch ($(this).val()) {
			case "color":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").hide().addClass("hidden");
				$("#section-ws_bodybackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_bodyupload, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodypatternsheer").hide().addClass("hidden");
				$("#section-ws_bodypatternopaque, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
				$("#section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected body option
		if ($("#ws_bodyoption").val() === "color") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").hide().addClass("hidden");
			$("#section-ws_bodybackground").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "gradient") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque, #section-ws_bodyupload").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "patternsheer") {
			$("#section-ws_bodyupload, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "patternopaque") {
			$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodypatternsheer").hide().addClass("hidden");
			$("#section-ws_bodypatternopaque, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "uploadsheer") {
			$("#section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodybackground, #section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#ws_bodyoption").val() === "uploadopaque") {
			$("#section-ws_bodybackground, #section-ws_bodypatternsheer, #section-ws_bodypatternopaque").hide().addClass("hidden");
			$("#section-ws_bodyupload, #section-ws_bodyrepeat, #section-ws_bodyattach").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_brand"); function optionsframework_option_brand() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for brand options
		$("#ws_brand").change(function () {
			switch ($(this).val()) {
			case "one":
				$("#section-ws_brand_mark, #section-ws_brand_logo").hide().addClass("hidden");
				$("#section-ws_brand_font_text, #section-ws_brand_font_type").show().removeClass("hidden");
				break;
			case "two":
				$("#section-ws_brand_logo").hide().addClass("hidden");
				$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").show().removeClass("hidden");
				break;
			case "three":
				$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").hide().addClass("hidden");
				$("#section-ws_brand_logo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected brand option
		if ($("#ws_brand").val() === "one") {
			$("#section-ws_brand_mark, #section-ws_brand_logo").hide().addClass("hidden");
			$("#section-ws_brand_font_text, #section-ws_brand_font_type").show().removeClass("hidden");
		}
		if ($("#ws_brand").val() === "two") {
			$("#section-ws_brand_logo").hide().addClass("hidden");
			$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").show().removeClass("hidden");
		}
		if ($("#ws_brand").val() === "three") {
			$("#section-ws_brand_font_text, #section-ws_brand_font_type, #section-ws_brand_mark").hide().addClass("hidden");
			$("#section-ws_brand_logo").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_colophon"); function optionsframework_option_colophon() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for colophon options
		$("#ws_colophonoption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").hide().addClass("hidden");
				$("#section-ws_colophonnote, #section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "transparent":
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_colophonupload, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonpatternsheer, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonpatternopaque, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote").hide().addClass("hidden");
				$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected colophon option
		if ($("#ws_colophonoption").val() === "auto") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").hide().addClass("hidden");
			$("#section-ws_colophonnote, #section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "transparent") {
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "color") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "gradient") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonupload, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "patternsheer") {
			$("#section-ws_colophonupload, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");			}
		if ($("#ws_colophonoption").val() === "patternopaque") {
			$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonpatternsheer, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonpatternopaque, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "uploadsheer") {
			$("#section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote").hide().addClass("hidden");
			$("#section-ws_colophonbackground, #section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach, #section-ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#ws_colophonoption").val() === "uploadopaque") {
			$("#section-ws_colophonbackground, #section-ws_colophonpatternsheer, #section-ws_colophonpatternopaque, #section-ws_colophonnote, #section-ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#section-ws_colophonupload, #section-ws_colophonrepeat, #section-ws_colophonattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_colophonboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_colophonboxshadow").val() === "none") {
			$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_colophonboxshadow").val() === "shadow") {
			$("#section-ws_colophonboxshadowinset, #section-ws_colophonboxshadowhorz, #section-ws_colophonboxshadowvert, #section-ws_colophonboxshadowblur, #section-ws_colophonboxshadowspread, #section-ws_colophonboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_colophonborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_colophonborderradius").val() === "none") {
			$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_colophonborderradius").val() === "radius") {
			$("#section-ws_colophonborderradiustopleft, #section-ws_colophonborderradiustopright, #section-ws_colophonborderradiusbtmright, #section-ws_colophonborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_colophonmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_colophonmargin").val() === "default") {
			$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").hide().addClass("hidden");
		}
		if ($("#ws_colophonmargin").val() === "margin") {
			$("#section-ws_colophonmargintop, #section-ws_colophonmarginbtm, #section-ws_colophonmarginleft, #section-ws_colophonmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_colophonpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_colophonpadding").val() === "default") {
			$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_colophonpadding").val() === "padding") {
			$("#section-ws_colophonpaddingtop, #section-ws_colophonpaddingbtm, #section-ws_colophonpaddingleft, #section-ws_colophonpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_content"); function optionsframework_option_content() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for content options
		$("#ws_contentoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach, #section-ws_contentbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").hide().addClass("hidden");
				$("#section-ws_contentbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_contentupload, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentpatternsheer").hide().addClass("hidden");
				$("#section-ws_contentpatternopaque, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
				$("#section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected content option
		if ($("#ws_contentoption").val() === "transparent") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach, #section-ws_contentbackground").hide().addClass("hidden");
		}
		if ($("#ws_contentoption").val() === "color") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").hide().addClass("hidden");
			$("#section-ws_contentbackground").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "gradient") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque, #section-ws_contentupload").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "patternsheer") {
			$("#section-ws_contentupload, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "patternopaque") {
			$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentpatternsheer").hide().addClass("hidden");
			$("#section-ws_contentpatternopaque, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "uploadsheer") {
			$("#section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentbackground, #section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		if ($("#ws_contentoption").val() === "uploadopaque") {
			$("#section-ws_contentbackground, #section-ws_contentpatternsheer, #section-ws_contentpatternopaque").hide().addClass("hidden");
			$("#section-ws_contentupload, #section-ws_contentrepeat, #section-ws_contentattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_contentboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_contentboxshadow").val() === "none") {
			$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_contentboxshadow").val() === "shadow") {
			$("#section-ws_contentboxshadowinset, #section-ws_contentboxshadowhorz, #section-ws_contentboxshadowvert, #section-ws_contentboxshadowblur, #section-ws_contentboxshadowspread, #section-ws_contentboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_contentborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_contentborderradius").val() === "none") {
			$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_contentborderradius").val() === "radius") {
			$("#section-ws_contentborderradiustopleft, #section-ws_contentborderradiustopright, #section-ws_contentborderradiusbtmright, #section-ws_contentborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_contentmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_contentmargin").val() === "default") {
			$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").hide().addClass("hidden");
		}
		if ($("#ws_contentmargin").val() === "margin") {
			$("#section-ws_contentmargintop, #section-ws_contentmarginbtm, #section-ws_contentmarginleft, #section-ws_contentmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_contentpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_contentpadding").val() === "default") {
			$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_contentpadding").val() === "padding") {
			$("#section-ws_contentpaddingtop, #section-ws_contentpaddingbtm, #section-ws_contentpaddingleft, #section-ws_contentpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_feature"); function optionsframework_option_feature() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for feature options
		$("#ws_featureoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach, #section-ws_featurebackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").hide().addClass("hidden");
				$("#section-ws_featurebackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_featureupload, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurepatternsheer").hide().addClass("hidden");
				$("#section-ws_featurepatternopaque, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
				$("#section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected feature option
		if ($("#ws_featureoption").val() === "transparent") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach, #section-ws_featurebackground").hide().addClass("hidden");
		}
		if ($("#ws_featureoption").val() === "color") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").hide().addClass("hidden");
			$("#section-ws_featurebackground").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "gradient") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque, #section-ws_featureupload").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "patternsheer") {
			$("#section-ws_featureupload, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "patternopaque") {
			$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurepatternsheer").hide().addClass("hidden");
			$("#section-ws_featurepatternopaque, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "uploadsheer") {
			$("#section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featurebackground, #section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		if ($("#ws_featureoption").val() === "uploadopaque") {
			$("#section-ws_featurebackground, #section-ws_featurepatternsheer, #section-ws_featurepatternopaque").hide().addClass("hidden");
			$("#section-ws_featureupload, #section-ws_featurerepeat, #section-ws_featureattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_featureboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_featureboxshadow").val() === "none") {
			$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_featureboxshadow").val() === "shadow") {
			$("#section-ws_featureboxshadowinset, #section-ws_featureboxshadowhorz, #section-ws_featureboxshadowvert, #section-ws_featureboxshadowblur, #section-ws_featureboxshadowspread, #section-ws_featureboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_featureborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_featureborderradius").val() === "none") {
			$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_featureborderradius").val() === "radius") {
			$("#section-ws_featureborderradiustopleft, #section-ws_featureborderradiustopright, #section-ws_featureborderradiusbtmright, #section-ws_featureborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_featuremargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_featuremargin").val() === "default") {
			$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").hide().addClass("hidden");
		}
		if ($("#ws_featuremargin").val() === "margin") {
			$("#section-ws_featuremargintop, #section-ws_featuremarginbtm, #section-ws_featuremarginleft, #section-ws_featuremarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_featurepadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_featurepadding").val() === "default") {
			$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").hide().addClass("hidden");
		}
		if ($("#ws_featurepadding").val() === "padding") {
			$("#section-ws_featurepaddingtop, #section-ws_featurepaddingbtm, #section-ws_featurepaddingleft, #section-ws_featurepaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_footer"); function optionsframework_option_footer() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for footer options
		$("#ws_footeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach, #section-ws_footerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").hide().addClass("hidden");
				$("#section-ws_footerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_footerupload, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerpatternsheer").hide().addClass("hidden");
				$("#section-ws_footerpatternopaque, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
				$("#section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected footer option
		if ($("#ws_footeroption").val() === "transparent") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach, #section-ws_footerbackground").hide().addClass("hidden");
		}
		if ($("#ws_footeroption").val() === "color") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").hide().addClass("hidden");
			$("#section-ws_footerbackground").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "gradient") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque, #section-ws_footerupload").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "patternsheer") {
			$("#section-ws_footerupload, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "patternopaque") {
			$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerpatternsheer").hide().addClass("hidden");
			$("#section-ws_footerpatternopaque, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "uploadsheer") {
			$("#section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerbackground, #section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		if ($("#ws_footeroption").val() === "uploadopaque") {
			$("#section-ws_footerbackground, #section-ws_footerpatternsheer, #section-ws_footerpatternopaque").hide().addClass("hidden");
			$("#section-ws_footerupload, #section-ws_footerrepeat, #section-ws_footerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_footerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_footerboxshadow").val() === "none") {
			$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_footerboxshadow").val() === "shadow") {
			$("#section-ws_footerboxshadowinset, #section-ws_footerboxshadowhorz, #section-ws_footerboxshadowvert, #section-ws_footerboxshadowblur, #section-ws_footerboxshadowspread, #section-ws_footerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_footerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_footerborderradius").val() === "none") {
			$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_footerborderradius").val() === "radius") {
			$("#section-ws_footerborderradiustopleft, #section-ws_footerborderradiustopright, #section-ws_footerborderradiusbtmright, #section-ws_footerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_footermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_footermargin").val() === "default") {
			$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").hide().addClass("hidden");
		}
		if ($("#ws_footermargin").val() === "margin") {
			$("#section-ws_footermargintop, #section-ws_footermarginbtm, #section-ws_footermarginleft, #section-ws_footermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_footerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_footerpadding").val() === "default") {
			$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_footerpadding").val() === "padding") {
			$("#section-ws_footerpaddingtop, #section-ws_footerpaddingbtm, #section-ws_footerpaddingleft, #section-ws_footerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_header"); function optionsframework_option_header() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for header options
		$("#ws_headeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach, #section-ws_headerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").hide().addClass("hidden");
				$("#section-ws_headerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_headerupload, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerpatternsheer").hide().addClass("hidden");
				$("#section-ws_headerpatternopaque, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
				$("#section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#ws_headeroption").val() === "transparent") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach, #section-ws_headerbackground").hide().addClass("hidden");
		}
		if ($("#ws_headeroption").val() === "color") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").hide().addClass("hidden");
			$("#section-ws_headerbackground").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "gradient") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque, #section-ws_headerupload").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "patternsheer") {
			$("#section-ws_headerupload, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "patternopaque") {
			$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerpatternsheer").hide().addClass("hidden");
			$("#section-ws_headerpatternopaque, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "uploadsheer") {
			$("#section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerbackground, #section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		if ($("#ws_headeroption").val() === "uploadopaque") {
			$("#section-ws_headerbackground, #section-ws_headerpatternsheer, #section-ws_headerpatternopaque").hide().addClass("hidden");
			$("#section-ws_headerupload, #section-ws_headerrepeat, #section-ws_headerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_headerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_headerboxshadow").val() === "none") {
			$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_headerboxshadow").val() === "shadow") {
			$("#section-ws_headerboxshadowinset, #section-ws_headerboxshadowhorz, #section-ws_headerboxshadowvert, #section-ws_headerboxshadowblur, #section-ws_headerboxshadowspread, #section-ws_headerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_headerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_headerborderradius").val() === "none") {
			$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_headerborderradius").val() === "radius") {
			$("#section-ws_headerborderradiustopleft, #section-ws_headerborderradiustopright, #section-ws_headerborderradiusbtmright, #section-ws_headerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_headermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_headermargin").val() === "default") {
			$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").hide().addClass("hidden");
		}
		if ($("#ws_headermargin").val() === "margin") {
			$("#section-ws_headermargintop, #section-ws_headermarginbtm, #section-ws_headermarginleft, #section-ws_headermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_headerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_headerpadding").val() === "default") {
			$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_headerpadding").val() === "padding") {
			$("#section-ws_headerpaddingtop, #section-ws_headerpaddingbtm, #section-ws_headerpaddingleft, #section-ws_headerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_mast"); function optionsframework_option_mast() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for mast options
		$("#ws_mastoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach, #section-ws_mastbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").hide().addClass("hidden");
				$("#section-ws_mastbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_mastupload, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastpatternsheer").hide().addClass("hidden");
				$("#section-ws_mastpatternopaque, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
				$("#section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected mast option
		if ($("#ws_mastoption").val() === "transparent") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach, #section-ws_mastbackground").hide().addClass("hidden");
		}
		if ($("#ws_mastoption").val() === "color") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").hide().addClass("hidden");
			$("#section-ws_mastbackground").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "gradient") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque, #section-ws_mastupload").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "patternsheer") {
			$("#section-ws_mastupload, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "patternopaque") {
			$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastpatternsheer").hide().addClass("hidden");
			$("#section-ws_mastpatternopaque, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "uploadsheer") {
			$("#section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastbackground, #section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		if ($("#ws_mastoption").val() === "uploadopaque") {
			$("#section-ws_mastbackground, #section-ws_mastpatternsheer, #section-ws_mastpatternopaque").hide().addClass("hidden");
			$("#section-ws_mastupload, #section-ws_mastrepeat, #section-ws_mastattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_mastboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_mastboxshadow").val() === "none") {
			$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_mastboxshadow").val() === "shadow") {
			$("#section-ws_mastboxshadowinset, #section-ws_mastboxshadowhorz, #section-ws_mastboxshadowvert, #section-ws_mastboxshadowblur, #section-ws_mastboxshadowspread, #section-ws_mastboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_mastborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_mastborderradius").val() === "none") {
			$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_mastborderradius").val() === "radius") {
			$("#section-ws_mastborderradiustopleft, #section-ws_mastborderradiustopright, #section-ws_mastborderradiusbtmright, #section-ws_mastborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_mastmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_mastmargin").val() === "default") {
			$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").hide().addClass("hidden");
		}
		if ($("#ws_mastmargin").val() === "margin") {
			$("#section-ws_mastmargintop, #section-ws_mastmarginbtm, #section-ws_mastmarginleft, #section-ws_mastmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_mastpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_mastpadding").val() === "default") {
			$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_mastpadding").val() === "padding") {
			$("#section-ws_mastpaddingtop, #section-ws_mastpaddingbtm, #section-ws_mastpaddingleft, #section-ws_mastpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_nav"); function optionsframework_option_nav() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for navbar bkgd options
		$("#ws_navbaroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach, #section-ws_navbarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").hide().addClass("hidden");
				$("#section-ws_navbarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_navbarupload, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarpatternsheer").hide().addClass("hidden");
				$("#section-ws_navbarpatternopaque, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
				$("#section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar option
		if ($("#ws_navbaroption").val() === "transparent") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach, #section-ws_navbarbackground").hide().addClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "color") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").hide().addClass("hidden");
			$("#section-ws_navbarbackground").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "gradient") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque, #section-ws_navbarupload").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "patternsheer") {
			$("#section-ws_navbarupload, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "patternopaque") {
			$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarpatternsheer").hide().addClass("hidden");
			$("#section-ws_navbarpatternopaque, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "uploadsheer") {
			$("#section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarbackground, #section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#ws_navbaroption").val() === "uploadopaque") {
			$("#section-ws_navbarbackground, #section-ws_navbarpatternsheer, #section-ws_navbarpatternopaque").hide().addClass("hidden");
			$("#section-ws_navbarupload, #section-ws_navbarrepeat, #section-ws_navbarattach").show().removeClass("hidden");
		}
		// custom js for navbar brand options
		$("#ws_navbarbrand").change(function () {
			switch ($(this).val()) {
			case "masthead-brand":
				$("#section-ws_navbarbrandlogo").hide().addClass("hidden");
				break;
			case "navbar-brand":
				$("#section-ws_navbarbrandlogo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#ws_navbarbrand").val() === "masthead-brand") {
			$("#section-ws_navbarbrandlogo").hide().addClass("hidden");
		}
		if ($("#ws_navbarbrand").val() === "navbar-brand") {
			$("#section-ws_navbarbrandlogo").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_navbarboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_navbarboxshadow").val() === "none") {
			$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_navbarboxshadow").val() === "shadow") {
			$("#section-ws_navbarboxshadowinset, #section-ws_navbarboxshadowhorz, #section-ws_navbarboxshadowvert, #section-ws_navbarboxshadowblur, #section-ws_navbarboxshadowspread, #section-ws_navbarboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_navbarborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_navbarborderradius").val() === "none") {
			$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_navbarborderradius").val() === "radius") {
			$("#section-ws_navbarborderradiustopleft, #section-ws_navbarborderradiustopright, #section-ws_navbarborderradiusbtmright, #section-ws_navbarborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_navbarmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_navbarmargin").val() === "default") {
			$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").hide().addClass("hidden");
		}
		if ($("#ws_navbarmargin").val() === "margin") {
			$("#section-ws_navbarmargintop, #section-ws_navbarmarginbtm, #section-ws_navbarmarginleft, #section-ws_navbarmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_navbarpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_navbarpadding").val() === "default") {
			$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").hide().addClass("hidden");
		}
		if ($("#ws_navbarpadding").val() === "padding") {
			$("#section-ws_navbarpaddingtop, #section-ws_navbarpaddingbtm, #section-ws_navbarpaddingleft, #section-ws_navbarpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_sidebar"); function optionsframework_option_sidebar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for sidebar options
		$("#ws_sidebaroption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "transparent":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").hide().addClass("hidden");
				$("#section-ws_sidebarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_sidebarupload, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarpatternsheer").hide().addClass("hidden");
				$("#section-ws_sidebarpatternopaque, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected sidebar option
		if ($("#ws_sidebaroption").val() === "auto") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "transparent") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "color") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").hide().addClass("hidden");
			$("#section-ws_sidebarbackground").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "gradient") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "patternsheer") {
			$("#section-ws_sidebarupload, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "patternopaque") {
			$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarpatternsheer").hide().addClass("hidden");
			$("#section-ws_sidebarpatternopaque, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "uploadsheer") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "uploadopaque") {
			$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
	});
</script>

<?php
} add_action("optionsframework_custom_scripts", "optionsframework_option_wrap"); function optionsframework_option_wrap() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for wrap options
		$("#ws_wrapoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach, #section-ws_wrapbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").hide().addClass("hidden");
				$("#section-ws_wrapbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_wrapupload, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wrappatternsheer").hide().addClass("hidden");
				$("#section-ws_wrappatternopaque, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
				$("#section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected wrap option
		if ($("#ws_wrapoption").val() === "transparent") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach, #section-ws_wrapbackground").hide().addClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "color") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").hide().addClass("hidden");
			$("#section-ws_wrapbackground").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "gradient") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque, #section-ws_wrapupload").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "patternsheer") {
			$("#section-ws_wrapupload, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "patternopaque") {
			$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wrappatternsheer").hide().addClass("hidden");
			$("#section-ws_wrappatternopaque, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "uploadsheer") {
			$("#section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapbackground, #section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#ws_wrapoption").val() === "uploadopaque") {
			$("#section-ws_wrapbackground, #section-ws_wrappatternsheer, #section-ws_wrappatternopaque").hide().addClass("hidden");
			$("#section-ws_wrapupload, #section-ws_wraprepeat, #section-ws_wrapattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#ws_wrapboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#ws_wrapboxshadow").val() === "none") {
			$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#ws_wrapboxshadow").val() === "shadow") {
			$("#section-ws_wrapboxshadowinset, #section-ws_wrapboxshadowhorz, #section-ws_wrapboxshadowvert, #section-ws_wrapboxshadowblur, #section-ws_wrapboxshadowspread, #section-ws_wrapboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#ws_wrapborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#ws_wrapborderradius").val() === "none") {
			$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#ws_wrapborderradius").val() === "radius") {
			$("#section-ws_wrapborderradiustopleft, #section-ws_wrapborderradiustopright, #section-ws_wrapborderradiusbtmright, #section-ws_wrapborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#ws_wrapmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#ws_wrapmargin").val() === "default") {
			$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").hide().addClass("hidden");
		}
		if ($("#ws_wrapmargin").val() === "margin") {
			$("#section-ws_wrapmargintop, #section-ws_wrapmarginbtm, #section-ws_wrapmarginleft, #section-ws_wrapmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#ws_wrappadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#ws_wrappadding").val() === "default") {
			$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").hide().addClass("hidden");
		}
		if ($("#ws_wrappadding").val() === "padding") {
			$("#section-ws_wrappaddingtop, #section-ws_wrappaddingbtm, #section-ws_wrappaddingleft, #section-ws_wrappaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}