<?php
/** Show/Hide Javascript for Sidebar Background Options **/

add_action("optionsframework_custom_scripts", "optionsframework_option_sidebar");
function optionsframework_option_sidebar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for sidebar options
		$("#ws_sidebaroption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
				break;				
			case "transparent":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
				break;			
			case "color":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").hide().addClass("hidden");
				$("#section-ws_sidebarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#section-ws_sidebarupload, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarpatternsheer").hide().addClass("hidden");
				$("#section-ws_sidebarpatternopaque, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected sidebar option
		if ($("#ws_sidebaroption").val() === "auto") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
		}				
		if ($("#ws_sidebaroption").val() === "transparent") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach, #section-ws_sidebarbackground").hide().addClass("hidden");
		}		
		if ($("#ws_sidebaroption").val() === "color") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").hide().addClass("hidden");
			$("#section-ws_sidebarbackground").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "gradient") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque, #section-ws_sidebarupload").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "patternsheer") {
			$("#section-ws_sidebarupload, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "patternopaque") {
			$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarpatternsheer").hide().addClass("hidden");
			$("#section-ws_sidebarpatternopaque, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "uploadsheer") {
			$("#section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarbackground, #section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#ws_sidebaroption").val() === "uploadopaque") {
			$("#section-ws_sidebarbackground, #section-ws_sidebarpatternsheer, #section-ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#section-ws_sidebarupload, #section-ws_sidebarrepeat, #section-ws_sidebarattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}