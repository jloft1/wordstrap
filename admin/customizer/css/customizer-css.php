<?php

// WordStrap CSS for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_css() { ?>

<style>
input[type="radio"].radio-img-input+div { display:block; float:left; width:50px; height:50px; margin:3px; padding:2px; border:1px solid #ccc; }
input[type="radio"].radio-img-input+div span { display:block; width:50px; height:50px; background-repeat:repeat; }
input[type="radio"].radio-img-input:checked+div { margin:1px; padding:2px; border:3px solid #21759b; }
input[type="radio"].radio-img-input+div:hover { margin:1px; padding:2px; border:3px solid #D54E21; }
ul.customize-section-content>li.customize-control { display:block; overflow:hidden; clear:both; margin:10px 0; }
span.customize-control-title { line-height:20px; margin-bottom:7px; }
ul.customize-section-content>li>ul>li { margin-bottom:2px; height:auto; }
</style>

<?php
}

add_action('customize_controls_print_styles', 'ws_customizer_css');