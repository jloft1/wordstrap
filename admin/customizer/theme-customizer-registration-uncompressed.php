<?php

/**
 * Front End Theme Customizer Registration
 * WordPress 3.4 Required
 */

function wordstrap_customize_register( $wp_customize ) {

	// Remove Default Sections
	$wp_customize->remove_section( 'title_tagline');
	$wp_customize->remove_section( 'colors');
	$wp_customize->remove_section( 'header_image');
	$wp_customize->remove_section( 'background_image');
	$wp_customize->remove_section( 'nav');
	$wp_customize->remove_section( 'static_front_page');

	// Customizer Sections + Options
	$customizer_array = array(
		'ws_section_typography_base' => array(
			'name'   => __( 'Typography - Defaults/Base Font', 'wordstrap'),
			'priority'  => 101,
			'settings'  => array(
				'ws_linkcolor',
				'ws_linkcolorhover',
				'ws_sansfontfamily',
				'ws_seriffontfamily',
				'ws_monofontfamily',
				'ws_altfontfamily',
				'ws_basefont',
				'ws_baselineheight'
			)
		),
		'ws_section_typography_headings' => array(
			'name'   => __( 'Typography - Headings', 'wordstrap'),
			'priority'  => 102,
			'settings'  => array(
				'ws_headingsfont',
				'ws_h1font',
				'ws_h1lineheight'

			)
		),
		'ws_section_typography_icons' => array(
			'name'   => __( 'Typography - Icons/Embelishments', 'wordstrap'),
			'priority'  => 103,
			'settings'  => array(
				'ws_iconfontcolor',
				'ws_hrborder'
			)
		),
		'ws_section_body_bkgd' => array(
			'name'   => __( 'Body - Background', 'wordstrap'),
			'priority'  => 201,
			'settings'  => array(
				'ws_bodyoption',
				'ws_bodybackground',
				'ws_bodyrepeat',
				'ws_bodyattach',
				'ws_bodypatternsheer',
				'ws_bodypatternopaque',
				'ws_bodyupload'
			)
		),
		'ws_section_wrap_display' => array(
			'name'  => __( 'Wrap - Display Properties', 'wordstrap'),
			'priority' => 301,
			'settings' => array(
				'ws_wrapcontainer',
				'ws_wrapboxshadow',
				'ws_wrapboxshadowinset',
				'ws_wrapboxshadowhorz',
				'ws_wrapboxshadowvert',
				'ws_wrapboxshadowblur',
				'ws_wrapboxshadowspread',
				'ws_wrapboxshadowopacity',
				'ws_wrapborderradius',
				'ws_wrapborderradiustopleft',
				'ws_wrapborderradiustopright',
				'ws_wrapborderradiusbtmright',
				'ws_wrapborderradiusbtmleft',
				'ws_wrapmargin',
				'ws_wrapmargintop',
				'ws_wrapmarginright',
				'ws_wrapmarginleft',
				'ws_wrapmarginbtm',
				'ws_wrappadding',
				'ws_wrappaddingtop',
				'ws_wrappaddingright',
				'ws_wrappaddingleft',
				'ws_wrappaddingbtm'
			)
		),
		'ws_section_wrap_bkgd' => array(
			'name'  => __( 'Wrap - Background', 'wordstrap'),
			'priority' => 302,
			'settings' => array(
				'ws_wrapoption',
				'ws_wrapbackground',
				'ws_wraprepeat',
				'ws_wrapattach',
				'ws_wrappatternsheer',
				'ws_wrappatternopaque',
				'ws_wrapupload'
			)
		),
		'ws_section_header_display' => array(
			'name'  => __( 'Header - Display Properties', 'wordstrap'),
			'priority' => 401,
			'settings' => array(
				'ws_headercontainer',
				'ws_headerboxshadow',
				'ws_headerboxshadowinset',
				'ws_headerboxshadowhorz',
				'ws_headerboxshadowvert',
				'ws_headerboxshadowblur',
				'ws_headerboxshadowspread',
				'ws_headerboxshadowopacity',
				'ws_headerborderradius',
				'ws_headerborderradiustopleft',
				'ws_headerborderradiustopright',
				'ws_headerborderradiusbtmright',
				'ws_headerborderradiusbtmleft',
				'ws_headermargin',
				'ws_headermargintop',
				'ws_headermarginright',
				'ws_headermarginleft',
				'ws_headermarginbtm',
				'ws_headerpadding',
				'ws_headerpaddingtop',
				'ws_headerpaddingright',
				'ws_headerpaddingleft',
				'ws_headerpaddingbtm'
			)
		),
		'ws_section_header_bkgd' => array(
			'name'  => __( 'Header - Background', 'wordstrap'),
			'priority' => 403,
			'settings' => array(
				'ws_headeroption',
				'ws_headerbackground',
				'ws_headerrepeat',
				'ws_headerattach',
				'ws_headerpatternsheer',
				'ws_headerpatternopaque',
				'ws_headerupload'
			)
		),
		'ws_section_mast_display' => array(
			'name'  => __( 'Masthead - Display Properties', 'wordstrap'),
			'priority' => 501,
			'settings' => array(
				'ws_mastheight',
				'ws_mastboxshadow',
				'ws_mastboxshadowinset',
				'ws_mastboxshadowhorz',
				'ws_mastboxshadowvert',
				'ws_mastboxshadowblur',
				'ws_mastboxshadowspread',
				'ws_mastboxshadowopacity',
				'ws_mastborderradius',
				'ws_mastborderradiustopleft',
				'ws_mastborderradiustopright',
				'ws_mastborderradiusbtmright',
				'ws_mastborderradiusbtmleft',
				'ws_mastmargin',
				'ws_mastmargintop',
				'ws_mastmarginbtm',
				'ws_mastpadding',
				'ws_mastpaddingtop',
				'ws_mastpaddingbtm'
			)
		),
		'ws_section_mast_bkgd' => array(
			'name'  => __( 'Masthead - Background', 'wordstrap'),
			'priority' => 503,
			'settings' => array(
				'ws_mastoption',
				'ws_mastbackground',
				'ws_mastrepeat',
				'ws_mastattach',
				'ws_mastpatternsheer',
				'ws_mastpatternopaque',
				'ws_mastupload'
			)
		),
		'ws_section_mast_brand' => array(
			'name'  => __( 'Masthead - Branding', 'wordstrap'),
			'priority' => 505,
			'settings' => array(
				'ws_brand',
				'ws_brand_font_text',
				'ws_brand_font_type',
				'ws_brand_mark',
				'ws_brand_logo'
			)
		),
		'ws_section_mast_leaderboard' => array(
			'name'  => __( 'Masthead - Leaderboard', 'wordstrap'),
			'priority' => 507,
			'settings' => array(
				'ws_mastleaderboard'
			)
		),
		'ws_section_navbar_display' => array(
			'name'  => __( 'Navbar - Display Properties', 'wordstrap'),
			'priority' => 601,
			'settings' => array(
				'ws_navbarcontainer',
				'ws_navbarcollapsewidth',
				'ws_navbarheight',
				'ws_navbarposition',
				'ws_navbartype',
				'ws_navbarscheme',
				'ws_navbarfixed',
				'ws_navbartextshadow',
				'ws_navbarvertdividers',
				'ws_navbarmenusidemargins',
				'ws_navbarboxshadow',
				'ws_navbarboxshadowinset',
				'ws_navbarboxshadowhorz',
				'ws_navbarboxshadowvert',
				'ws_navbarboxshadowblur',
				'ws_navbarboxshadowspread',
				'ws_navbarboxshadowopacity',
				'ws_navbarborderradius',
				'ws_navbarborderradiustopleft',
				'ws_navbarborderradiustopright',
				'ws_navbarborderradiusbtmright',
				'ws_navbarborderradiusbtmleft',
				'ws_navbarmargin',
				'ws_navbarmargintop',
				'ws_navbarmarginright',
				'ws_navbarmarginleft',
				'ws_navbarmarginbtm',
				'ws_navbarpadding',
				'ws_navbarpaddingtop',
				'ws_navbarpaddingright',
				'ws_navbarpaddingleft',
				'ws_navbarpaddingbtm'
			)
		),
		'ws_section_navbar_brand' => array(
			'name'  => __( 'Navbar - Branding', 'wordstrap'),
			'priority' => 602,
			'settings' => array(
				'ws_navbarbrand',
				'ws_navbarbrandlogo'
			)
		),
		'ws_section_navbar_default' => array(
			'name'  => __( 'Navbar - Light Scheme', 'wordstrap'),
			'priority' => 603,
			'settings' => array(
				'ws_navbardefaulttext',
				'ws_navbardefaultlinkcolor',
				'ws_navbardefaultbackground',
				'ws_navbardefaultlinkcolorhover',
				'ws_navbardefaultlinkbackgroundhover',
				'ws_navbardefaultlinkcoloractive',
				'ws_navbardefaultlinkbackgroundactive',
				'ws_navbardefaultborder',
				'ws_navbardefaultbrandcolor'
			)
		),
		'ws_section_navbar_inverse' => array(
			'name'  => __( 'Navbar - Dark Scheme', 'wordstrap'),
			'priority' => 604,
			'settings' => array(
				'ws_navbarinversetext',
				'ws_navbarinverselinkcolor',
				'ws_navbarinversebackground',
				'ws_navbarinverselinkcolorhover',
				'ws_navbarinverselinkbackgroundhover',
				'ws_navbarinverselinkcoloractive',
				'ws_navbarinverselinkbackgroundactive',
				'ws_navbarinverseborder',
				'ws_navbarinversebrandcolor'
			)
		),
		'ws_section_navbar_dropdown' => array(
			'name'  => __( 'Navbar - Dropdown Menu', 'wordstrap'),
			'priority' => 605,
			'settings' => array(
				'ws_navbarcaret',
				'ws_navbardropdowncaret',
				'ws_navbardropdownborderradius',
				'ws_navbardropdownmargintop'
			)
		),
		'ws_section_navbar_bkgd' => array(
			'name'  => __( 'Navbar - Background', 'wordstrap'),
			'priority' => 607,
			'settings' => array(
				'ws_navbaroption',
				'ws_navbarbackground',
				'ws_navbarrepeat',
				'ws_navbarattach',
				'ws_navbarpatternsheer',
				'ws_navbarpatternopaque',
				'ws_navbarupload'
			)
		),
		'ws_section_feature_display' => array(
			'name'  => __( 'Feature - Display Properties', 'wordstrap'),
			'priority' => 701,
			'settings' => array(
				'ws_featurecontainer',
				'ws_featureboxshadow',
				'ws_featureboxshadowinset',
				'ws_featureboxshadowhorz',
				'ws_featureboxshadowvert',
				'ws_featureboxshadowblur',
				'ws_featureboxshadowspread',
				'ws_featureboxshadowopacity',
				'ws_featureborderradius',
				'ws_featureborderradiustopleft',
				'ws_featureborderradiustopright',
				'ws_featureborderradiusbtmright',
				'ws_featureborderradiusbtmleft',
				'ws_featuremargin',
				'ws_featuremargintop',
				'ws_featuremarginright',
				'ws_featuremarginleft',
				'ws_featuremarginbtm',
				'ws_featurepadding',
				'ws_featurepaddingtop',
				'ws_featurepaddingright',
				'ws_featurepaddingleft',
				'ws_featurepaddingbtm'
			)
		),
		'ws_section_feature_bkgd' => array(
			'name'  => __( 'Feature - Background', 'wordstrap'),
			'priority' => 703,
			'settings' => array(
				'ws_featureoption',
				'ws_featurebackground',
				'ws_featurerepeat',
				'ws_featureattach',
				'ws_featurepatternsheer',
				'ws_featurepatternopaque',
				'ws_featureupload'
			)
		),
		'ws_section_content_display' => array(
			'name'  => __( 'Content - Display Properties', 'wordstrap'),
			'priority' => 801,
			'settings' => array(
				'ws_contentboxshadow',
				'ws_contentboxshadowinset',
				'ws_contentboxshadowhorz',
				'ws_contentboxshadowvert',
				'ws_contentboxshadowblur',
				'ws_contentboxshadowspread',
				'ws_contentboxshadowopacity',
				'ws_contentborderradius',
				'ws_contentborderradiustopleft',
				'ws_contentborderradiustopright',
				'ws_contentborderradiusbtmright',
				'ws_contentborderradiusbtmleft',
				'ws_contentmargin',
				'ws_contentmargintop',
				'ws_contentmarginright',
				'ws_contentmarginleft',
				'ws_contentmarginbtm',
				'ws_contentpadding',
				'ws_contentpaddingtop',
				'ws_contentpaddingright',
				'ws_contentpaddingleft',
				'ws_contentpaddingbtm'
			)
		),
		'ws_section_content_bkgd' => array(
			'name'  => __( 'Content - Background', 'wordstrap'),
			'priority' => 802,
			'settings' => array(
				'ws_contentoption',
				'ws_contentbackground',
				'ws_contentrepeat',
				'ws_contentattach',
				'ws_contentpatternsheer',
				'ws_contentpatternopaque',
				'ws_contentupload'
			)
		),
		'ws_section_sidebar_bkgd' => array(
			'name'  => __( 'Sidebar - Background', 'wordstrap'),
			'priority' => 901,
			'settings' => array(
				'ws_sidebaroption',
				'ws_sidebarbackground',
				'ws_sidebarrepeat',
				'ws_sidebarattach',
				'ws_sidebarpatternsheer',
				'ws_sidebarpatternopaque',
				'ws_sidebarupload'
			)
		),
		'ws_section_footer_display' => array(
			'name'  => __( 'Super Footer - Display Properties', 'wordstrap'),
			'priority' => 1001,
			'settings' => array(
				'ws_footercontainer',
				'ws_footerboxshadow',
				'ws_footerboxshadowinset',
				'ws_footerboxshadowhorz',
				'ws_footerboxshadowvert',
				'ws_footerboxshadowblur',
				'ws_footerboxshadowspread',
				'ws_footerboxshadowopacity',
				'ws_footerborderradius',
				'ws_footerborderradiustopleft',
				'ws_footerborderradiustopright',
				'ws_footerborderradiusbtmright',
				'ws_footerborderradiusbtmleft',
				'ws_footermargin',
				'ws_footermargintop',
				'ws_footermarginright',
				'ws_footermarginleft',
				'ws_footermarginbtm',
				'ws_footerpadding',
				'ws_footerpaddingtop',
				'ws_footerpaddingright',
				'ws_footerpaddingleft',
				'ws_footerpaddingbtm'
			)
		),
		'ws_section_footer_typography' => array(
			'name'  => __( 'Super Footer - Typography', 'wordstrap'),
			'priority' => 1002,
			'settings' => array(
				'ws_footercolor',
				'ws_footerlinkcolor',
				'ws_footerheadingscolor'
			)
		),
		'ws_section_footer_bkgd' => array(
			'name'  => __( 'Super Footer - Background', 'wordstrap'),
			'priority' => 1003,
			'settings' => array(
				'ws_footeroption',
				'ws_footerbackground',
				'ws_footerrepeat',
				'ws_footerattach',
				'ws_footerpatternsheer',
				'ws_footerpatternopaque',
				'ws_footerupload'
			)
		),
		'ws_section_colophon_display' => array(
			'name'  => __( 'Colophon - Display Properties', 'wordstrap'),
			'priority' => 1101,
			'settings' => array(
				'ws_colophoncontainer',
				'ws_colophonboxshadow',
				'ws_colophonboxshadowinset',
				'ws_colophonboxshadowhorz',
				'ws_colophonboxshadowvert',
				'ws_colophonboxshadowblur',
				'ws_colophonboxshadowspread',
				'ws_colophonboxshadowopacity',
				'ws_colophonborderradius',
				'ws_colophonborderradiustopleft',
				'ws_colophonborderradiustopright',
				'ws_colophonborderradiusbtmright',
				'ws_colophonborderradiusbtmleft',
				'ws_colophonmargin',
				'ws_colophonmargintop',
				'ws_colophonmarginright',
				'ws_colophonmarginleft',
				'ws_colophonmarginbtm',
				'ws_colophonpadding',
				'ws_colophonpaddingtop',
				'ws_colophonpaddingright',
				'ws_colophonpaddingleft',
				'ws_colophonpaddingbtm'
			)
		),
		'ws_section_colophon_typography' => array(
			'name'  => __( 'Colophon - Typography', 'wordstrap'),
			'priority' => 1102,
			'settings' => array(
				'ws_colophoncolor',
				'ws_colophonlinkcolor'
			)
		),
		'ws_section_colophon_bkgd' => array(
			'name'  => __( 'Colophon - Background', 'wordstrap'),
			'priority' => 1103,
			'settings' => array(
				'ws_colophonoption',
				'ws_colophonnote',
				'ws_colophonbackground',
				'ws_colophonrepeat',
				'ws_colophonattach',
				'ws_colophonbackgroundopacity',
				'ws_colophonpatternsheer',
				'ws_colophonpatternopaque',
				'ws_colophonupload'
			)
		),
		'ws_section_styles_basesizing' => array(
			'name'   => __( 'Styles - Base Sizing', 'wordstrap'),
			'priority'  => 2001,
			'settings'  => array(
				'ws_baseborderradius',
				'ws_borderradiuslarge',
				'ws_borderradiussmall'
			)
		),
		'ws_section_styles_definecolors' => array(
			'name'   => __( 'Styles - Define Colors', 'wordstrap'),
			'priority'  => 2003,
			'settings'  => array(
				'ws_blue',
				'ws_bluedark',
				'ws_green',
				'ws_red',
				'ws_yellow',
				'ws_orange',
				'ws_pink',
				'ws_purple'
			)
		),
		'ws_section_styles_dropdowns' => array(
			'name'   => __( 'Styles - Drop Downs', 'wordstrap'),
			'priority'  => 2005,
			'settings'  => array(
				'ws_dropdownbackground',
				'ws_dropdowndividertop',
				'ws_dropdowndividerbottom',
				'ws_dropdownlinkcolor',
				'ws_dropdownlinkcolorhover',
				'ws_dropdownlinkbackgroundhover',
				'ws_dropdownlinkcoloractive',
				'ws_dropdownlinkbackgroundactive'
			)
		),
		'ws_section_styles_forms' => array(
			'name'   => __( 'Styles - Forms', 'wordstrap'),
			'priority'  => 2007,
			'settings' => array(
				'ws_placeholdertext',
				'ws_inputbackground',
				'ws_inputborder',
				'ws_inputdisabledbackground',
				'ws_inputactionsbackground',
				'ws_warningtext',
				'ws_warningbackground',
				'ws_errortext',
				'ws_errorbackground',
				'ws_successtext',
				'ws_successbackground',
				'ws_infotext',
				'ws_infobackground'
			)
		),
		'ws_section_styles_buttons' => array(
			'name'   => __( 'Styles - Buttons', 'wordstrap'),
			'priority'  => 2009,
			'settings'  => array(
				'ws_btnbackground',
				'ws_btnprimarybackground',
				'ws_btninfobackground',
				'ws_btnsuccessbackground',
				'ws_btnwarningbackground',
				'ws_btndangerbackground',
				'ws_btninversebackground'
			)
		),
		'ws_section_styles_tables' => array(
			'name'   => __( 'Styles - Tables', 'wordstrap'),
			'priority'  => 2011,
			'settings'  => array(
				'ws_tablebackground',
				'ws_tablebackgroundaccent',
				'ws_tablebackgroundhover',
				'ws_tableborder'
			)
		),
		'ws_section_styles_tooltip' => array(
			'name'   => __( 'Styles - Tooltips + Popovers', 'wordstrap'),
			'priority'  => 2013,
			'settings'  => array(
				'ws_tooltipcolor',
				'ws_tooltipbackground',
				'ws_tooltiparrowwidth'
			)
		),
		'ws_section_styles_popover' => array(
			'name'   => __( 'Styles - Popovers', 'wordstrap'),
			'priority'  => 2015,
			'settings'  => array(
				'ws_popoverbackground',
				'ws_popoverarrowwidth'
			)
		),
		'ws_section_styles_wells' => array(
			'name'   => __( 'Styles - Wells', 'wordstrap'),
			'priority'  => 2017,
			'settings'  => array(
				'ws_wellbackground'
			)
		),
		'ws_section_styles_pagination' => array(
			'name'   => __( 'Styles - Pagination', 'wordstrap'),
			'priority'  => 2019,
			'settings'  => array(
				'ws_paginationbackground',
				'ws_paginationborder',
				'ws_paginationactivebackground'
			)
		),
		'ws_section_styles_hero' => array(
			'name'   => __( 'Styles - Hero Layout', 'wordstrap'),
			'priority'  => 2021,
			'settings'  => array(
				'ws_herounitbackground',
				'ws_herounitheadingcolor',
				'ws_herounitleadcolor'
			)
		),
	);

	// WordStrap Control Classes for Theme Customizer
	require_once dirname( __FILE__ ) . "/control/customizer-control-typography-open.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-typography-sizes.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-typography-faces.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-typography-styles.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-typography-close.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-body.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-wrap.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-header.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-mast.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-navbar.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-feature.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-content.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-sidebar.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-footer.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-patternbkgd-colophon.php";
	require_once dirname( __FILE__ ) . "/control/customizer-control-select-showhide.php";

	// WordStrap Javascript for Theme Customizer
	include_once dirname( __FILE__ ) . "/js/customizer-js-body.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-wrap.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-header.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-mast.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-navbar.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-feature.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-content.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-sidebar.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-footer.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-colophon.php";
	include_once dirname( __FILE__ ) . "/js/customizer-js-brand.php";


	// WordStrap CSS for Theme Customizer
	include_once dirname( __FILE__ ) . "/css/customizer-css.php";

	// Options Types Registration Loop
	$options = optionsframework_options();

	foreach ( $customizer_array as $name => $val ) {

		$i = 0;
		$wp_customize->add_section( "wordstrap_$name", array(
				'title'   => $val['name'],
				'priority'  => $val['priority']
			) );

		foreach ( $val['settings'] as $setting ) {

			$i++;

			if ( $options[$setting]['type'] == 'typography' ) {

				$wp_customize->add_setting( "wordstrap[$setting][open]", array(
						'default' => $options[$setting],
						'type'  => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_Typography_Control_open( $wp_customize, "wordstrap_".$setting."_open", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting][open]",
							'priority'  => $i.'a'
						) ) );

				$typography_variables = array(
					'size'  => 'sizes',
					'face'  => 'faces',
					'style'  => 'styles'
				);

				if (is_array($typography_variables)) {

					$typography_counter = 'b';

					foreach ( $typography_variables as $typography_variable => $typography_variable_plural ) {

						$typography_counter++;

						$wp_customize->add_setting( "wordstrap[$setting][$typography_variable]", array(
								'default' => $options[$setting]['std'][$typography_variable],
								'type'  => 'option'
							) );

						$typography_control_id = "WP_Customize_Typography_Control_".$typography_variable_plural;

						$wp_customize->add_control( new $typography_control_id ( $wp_customize, "wordstrap_".$setting."_".$typography_variable, array(
									'label'   => '',
									'section'  => "wordstrap_$name",
									'settings'  => "wordstrap[$setting][$typography_variable]",
									'type'   => "typography_$typography_variable_plural",
									'choices'  =>  $options[$setting]['options'][$typography_variable_plural],
									'priority'  => $i.$typography_counter
								) ) );
					}
				}

				$wp_customize->add_setting( "wordstrap[$setting][color]", array(
						'default' => $options[$setting]['std']['color'],
						'type' => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "wordstrap_".$setting."_color", array(
							'label'   => '',
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting][color]",
							'priority'  => $i.'f'
						) ) );

				$wp_customize->add_setting( "wordstrap[$setting][close]", array(
						'default' => $options[$setting],
						'type'  => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_Typography_Control_close( $wp_customize, "wordstrap_".$setting."_close", array(
							'label'   => '',
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting][close]",
							'priority'  => $i.'g'
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'bodypattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_body( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'bodypattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'wrappattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_wrap( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'wrappattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'headerpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_header( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'headerpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'mastpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_mast( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'mastpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'navbarpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_navbar( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'navbarpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'featurepattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_feature( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'featurepattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'contentpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_content( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'contentpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'sidebarpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_sidebar( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'sidebarpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'footerpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_footer( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'footerpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			elseif  ( $options[$setting]['type'] == 'colophonpattern' ) {

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );

				$wp_customize->add_control( new WP_Customize_PatternBkgd_Control_colophon( $wp_customize, "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => 'colophonpattern',
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) ) );

			}
			else
			{

				$wp_customize->add_setting( "wordstrap[$setting]", array(
						'default'  => $options[$setting]['std'],
						'type'   => 'option'
					) );
				if ( $options[$setting]['type'] == 'radio' || $options[$setting]['type'] == 'select' ) {
					$wp_customize->add_control( "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => $options[$setting]['type'],
							'choices'  => $options[$setting]['options'],
							'priority'  => $i
						) );
				} elseif ( $options[$setting]['type'] == 'text' || $options[$setting]['type'] == 'checkbox' ) {
					$wp_customize->add_control( "wordstrap_$setting", array(
							'label'   => $options[$setting]['name'],
							'section'  => "wordstrap_$name",
							'settings'  => "wordstrap[$setting]",
							'type'   => $options[$setting]['type'],
							'priority'  => $i
						) );
				} elseif ( $options[$setting]['type'] == 'color' ) {
					$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "wordstrap_$setting", array(
								'label'   => $options[$setting]['name'],
								'section'  => "wordstrap_$name",
								'settings'  => "wordstrap[$setting]",
								'priority'  => $i
							) ) );
				} elseif ( $options[$setting]['type'] == 'upload' ) { // Make sure that all upload options have the default defined as empty ("std" => "",).
					$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, "wordstrap_$setting", array(
								'label'   => $options[$setting]['name'],
								'section'  => "wordstrap_$name",
								'settings'  => "wordstrap[$setting]",
								'priority'  => $i
							) ) );
				} elseif ( $options[$setting]['type'] == 'selectshowhide' ) {
					$wp_customize->add_control( new WP_Customize_SelectShowHide_Control( $wp_customize, "wordstrap_$setting", array(
								'label'   => $options[$setting]['name'],
								'section'  => "wordstrap_$name",
								'settings'  => "wordstrap[$setting]",
								'type'   => $options[$setting]['type'],
								'choices'  => $options[$setting]['options'],
								'priority'  => $i
							) ) );
				}

			}

			$i++;
		}

	}

}

add_action( 'customize_register', 'wordstrap_customize_register' );