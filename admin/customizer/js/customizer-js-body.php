<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_body() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for body options
		$("#wordstrap_ws_bodyoption").change(function () {
			switch ($(this).val()) {
			case "color":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected body option
		if ($("#wordstrap_ws_bodyoption").val() === "color") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_body');