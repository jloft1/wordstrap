<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_mast() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for mast options
		$("#wordstrap_ws_mastoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach, #customize-control-wordstrap_ws_mastbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected mast option
		if ($("#wordstrap_ws_mastoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach, #customize-control-wordstrap_ws_mastbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "color") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_mastboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_mastboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_mastborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_mastborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_mastmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_mastmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_mastpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_mastpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_mast');