<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_content() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for content options
		$("#wordstrap_ws_contentoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach, #customize-control-wordstrap_ws_contentbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected content option
		if ($("#wordstrap_ws_contentoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach, #customize-control-wordstrap_ws_contentbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "color") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_contentboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_contentboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_contentborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_contentborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_contentmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_contentmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_contentpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_contentpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_content');