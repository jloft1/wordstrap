<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_wrap() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for wrap options
		$("#wordstrap_ws_wrapoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach, #customize-control-wordstrap_ws_wrapbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected wrap option
		if ($("#wordstrap_ws_wrapoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach, #customize-control-wordstrap_ws_wrapbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "color") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_wrapboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_wrapboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_wrapborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_wrapborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_wrapmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_wrapmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_wrappadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_wrappadding").val() === "default") {
			$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrappadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_wrap');