<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_feature() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for feature options
		$("#wordstrap_ws_featureoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach, #customize-control-wordstrap_ws_featurebackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected feature option
		if ($("#wordstrap_ws_featureoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach, #customize-control-wordstrap_ws_featurebackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "color") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_featureboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_featureboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_featureborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_featureborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_featuremargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_featuremargin").val() === "default") {
			$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featuremargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_featurepadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_featurepadding").val() === "default") {
			$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featurepadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_feature');