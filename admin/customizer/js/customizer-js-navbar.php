<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_navbar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for navbar bkgd options
		$("#wordstrap_ws_navbaroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach, #customize-control-wordstrap_ws_navbarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar option
		if ($("#wordstrap_ws_navbaroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach, #customize-control-wordstrap_ws_navbarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "color") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		// custom js for navbar brand options
		$("#wordstrap_ws_navbarbrand").change(function () {
			switch ($(this).val()) {
			case "masthead-brand":
				$("#customize-control-wordstrap_ws_navbarbrandlogo").hide().addClass("hidden");
				break;
			case "navbar-brand":
				$("#customize-control-wordstrap_ws_navbarbrandlogo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar brand option
		if ($("#wordstrap_ws_navbarbrand").val() === "masthead-brand") {
			$("#customize-control-wordstrap_ws_navbarbrandlogo").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarbrand").val() === "navbar-brand") {
			$("#customize-control-wordstrap_ws_navbarbrandlogo").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_navbarboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_navbarboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_navbarborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_navbarborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_navbarmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_navbarmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_navbarpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_navbarpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_navbar');