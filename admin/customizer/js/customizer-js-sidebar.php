<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_sidebar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for sidebar options
		$("#wordstrap_ws_sidebaroption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "transparent":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected sidebar option
		if ($("#wordstrap_ws_sidebaroption").val() === "auto") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "color") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_sidebar');