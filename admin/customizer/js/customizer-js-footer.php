<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_footer() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for footer options
		$("#wordstrap_ws_footeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach, #customize-control-wordstrap_ws_footerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected footer option
		if ($("#wordstrap_ws_footeroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach, #customize-control-wordstrap_ws_footerbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "color") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_footerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_footerboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_footerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_footerborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_footermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_footermargin").val() === "default") {
			$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footermargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_footerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_footerpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_footer');