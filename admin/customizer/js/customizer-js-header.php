<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_header() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for header options
		$("#wordstrap_ws_headeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach, #customize-control-wordstrap_ws_headerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#wordstrap_ws_headeroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach, #customize-control-wordstrap_ws_headerbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "color") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_headerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_headerboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_headerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_headerborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_headermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_headermargin").val() === "default") {
			$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headermargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_headerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_headerpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_header');