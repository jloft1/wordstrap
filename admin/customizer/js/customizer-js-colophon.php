<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_colophon() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for colophon options
		$("#wordstrap_ws_colophonoption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "transparent":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected colophon option
		if ($("#wordstrap_ws_colophonoption").val() === "auto") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "color") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");			}
		if ($("#wordstrap_ws_colophonoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_colophonboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_colophonboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_colophonborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_colophonborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_colophonmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_colophonmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_colophonpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_colophonpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").show().removeClass("hidden");
		}
	});
</script>


<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_colophon');