<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_body() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for body options
		$("#wordstrap_ws_bodyoption").change(function () {
			switch ($(this).val()) {
			case "color":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected body option
		if ($("#wordstrap_ws_bodyoption").val() === "color") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodypatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodypatternopaque, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_bodyoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_bodybackground, #customize-control-wordstrap_ws_bodypatternsheer, #customize-control-wordstrap_ws_bodypatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_bodyupload, #customize-control-wordstrap_ws_bodyrepeat, #customize-control-wordstrap_ws_bodyattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_body');

function ws_customizer_option_brand() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for brand options
		$("#wordstrap_ws_brand").change(function () {
			switch ($(this).val()) {
			case "one":
				$("#customize-control-wordstrap_ws_brand_mark, #customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_]").show().removeClass("hidden");
				break;
			case "two":
				$("#customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").show().removeClass("hidden");
				break;
			case "three":
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_logo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected brand option
		if ($("#wordstrap_ws_brand").val() === "one") {
			$("#customize-control-wordstrap_ws_brand_mark, #customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_]").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_brand").val() === "two") {
			$("#customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_brand").val() === "three") {
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_logo").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_brand');

function ws_customizer_option_colophon() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for colophon options
		$("#wordstrap_ws_colophonoption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "transparent":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected colophon option
		if ($("#wordstrap_ws_colophonoption").val() === "auto") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "color") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");			}
		if ($("#wordstrap_ws_colophonoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach, #customize-control-wordstrap_ws_colophonbackgroundopacity").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_colophonoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_colophonbackground, #customize-control-wordstrap_ws_colophonpatternsheer, #customize-control-wordstrap_ws_colophonpatternopaque, #customize-control-wordstrap_ws_colophonnote, #customize-control-wordstrap_ws_colophonbackgroundopacity").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_colophonupload, #customize-control-wordstrap_ws_colophonrepeat, #customize-control-wordstrap_ws_colophonattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_colophonboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_colophonboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_colophonboxshadowinset, #customize-control-wordstrap_ws_colophonboxshadowhorz, #customize-control-wordstrap_ws_colophonboxshadowvert, #customize-control-wordstrap_ws_colophonboxshadowblur, #customize-control-wordstrap_ws_colophonboxshadowspread, #customize-control-wordstrap_ws_colophonboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_colophonborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_colophonborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_colophonborderradiustopleft, #customize-control-wordstrap_ws_colophonborderradiustopright, #customize-control-wordstrap_ws_colophonborderradiusbtmright, #customize-control-wordstrap_ws_colophonborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_colophonmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_colophonmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_colophonmargintop, #customize-control-wordstrap_ws_colophonmarginbtm, #customize-control-wordstrap_ws_colophonmarginleft, #customize-control-wordstrap_ws_colophonmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_colophonpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_colophonpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_colophonpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_colophonpaddingtop, #customize-control-wordstrap_ws_colophonpaddingbtm, #customize-control-wordstrap_ws_colophonpaddingleft, #customize-control-wordstrap_ws_colophonpaddingright").show().removeClass("hidden");
		}
	});
</script>


<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_colophon');

function ws_customizer_option_content() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for content options
		$("#wordstrap_ws_contentoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach, #customize-control-wordstrap_ws_contentbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected content option
		if ($("#wordstrap_ws_contentoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach, #customize-control-wordstrap_ws_contentbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "color") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentpatternopaque, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_contentoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_contentbackground, #customize-control-wordstrap_ws_contentpatternsheer, #customize-control-wordstrap_ws_contentpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_contentupload, #customize-control-wordstrap_ws_contentrepeat, #customize-control-wordstrap_ws_contentattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_contentboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_contentboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_contentboxshadowinset, #customize-control-wordstrap_ws_contentboxshadowhorz, #customize-control-wordstrap_ws_contentboxshadowvert, #customize-control-wordstrap_ws_contentboxshadowblur, #customize-control-wordstrap_ws_contentboxshadowspread, #customize-control-wordstrap_ws_contentboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_contentborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_contentborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_contentborderradiustopleft, #customize-control-wordstrap_ws_contentborderradiustopright, #customize-control-wordstrap_ws_contentborderradiusbtmright, #customize-control-wordstrap_ws_contentborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_contentmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_contentmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_contentmargintop, #customize-control-wordstrap_ws_contentmarginbtm, #customize-control-wordstrap_ws_contentmarginleft, #customize-control-wordstrap_ws_contentmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_contentpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_contentpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_contentpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_contentpaddingtop, #customize-control-wordstrap_ws_contentpaddingbtm, #customize-control-wordstrap_ws_contentpaddingleft, #customize-control-wordstrap_ws_contentpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_content');

function ws_customizer_option_feature() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for feature options
		$("#wordstrap_ws_featureoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach, #customize-control-wordstrap_ws_featurebackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected feature option
		if ($("#wordstrap_ws_featureoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach, #customize-control-wordstrap_ws_featurebackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "color") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featureupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurepatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurepatternopaque, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_featureoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_featurebackground, #customize-control-wordstrap_ws_featurepatternsheer, #customize-control-wordstrap_ws_featurepatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_featureupload, #customize-control-wordstrap_ws_featurerepeat, #customize-control-wordstrap_ws_featureattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_featureboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_featureboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_featureboxshadowinset, #customize-control-wordstrap_ws_featureboxshadowhorz, #customize-control-wordstrap_ws_featureboxshadowvert, #customize-control-wordstrap_ws_featureboxshadowblur, #customize-control-wordstrap_ws_featureboxshadowspread, #customize-control-wordstrap_ws_featureboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_featureborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_featureborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featureborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_featureborderradiustopleft, #customize-control-wordstrap_ws_featureborderradiustopright, #customize-control-wordstrap_ws_featureborderradiusbtmright, #customize-control-wordstrap_ws_featureborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_featuremargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_featuremargin").val() === "default") {
			$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featuremargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_featuremargintop, #customize-control-wordstrap_ws_featuremarginbtm, #customize-control-wordstrap_ws_featuremarginleft, #customize-control-wordstrap_ws_featuremarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_featurepadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_featurepadding").val() === "default") {
			$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_featurepadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_featurepaddingtop, #customize-control-wordstrap_ws_featurepaddingbtm, #customize-control-wordstrap_ws_featurepaddingleft, #customize-control-wordstrap_ws_featurepaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_feature');

function ws_customizer_option_footer() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for footer options
		$("#wordstrap_ws_footeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach, #customize-control-wordstrap_ws_footerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected footer option
		if ($("#wordstrap_ws_footeroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach, #customize-control-wordstrap_ws_footerbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "color") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerpatternopaque, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_footeroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_footerbackground, #customize-control-wordstrap_ws_footerpatternsheer, #customize-control-wordstrap_ws_footerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_footerupload, #customize-control-wordstrap_ws_footerrepeat, #customize-control-wordstrap_ws_footerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_footerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_footerboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_footerboxshadowinset, #customize-control-wordstrap_ws_footerboxshadowhorz, #customize-control-wordstrap_ws_footerboxshadowvert, #customize-control-wordstrap_ws_footerboxshadowblur, #customize-control-wordstrap_ws_footerboxshadowspread, #customize-control-wordstrap_ws_footerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_footerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_footerborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_footerborderradiustopleft, #customize-control-wordstrap_ws_footerborderradiustopright, #customize-control-wordstrap_ws_footerborderradiusbtmright, #customize-control-wordstrap_ws_footerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_footermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_footermargin").val() === "default") {
			$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footermargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_footermargintop, #customize-control-wordstrap_ws_footermarginbtm, #customize-control-wordstrap_ws_footermarginleft, #customize-control-wordstrap_ws_footermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_footerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_footerpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_footerpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_footerpaddingtop, #customize-control-wordstrap_ws_footerpaddingbtm, #customize-control-wordstrap_ws_footerpaddingleft, #customize-control-wordstrap_ws_footerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_footer');

function ws_customizer_option_header() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for header options
		$("#wordstrap_ws_headeroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach, #customize-control-wordstrap_ws_headerbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected header option
		if ($("#wordstrap_ws_headeroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach, #customize-control-wordstrap_ws_headerbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "color") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerpatternopaque, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_headeroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_headerbackground, #customize-control-wordstrap_ws_headerpatternsheer, #customize-control-wordstrap_ws_headerpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_headerupload, #customize-control-wordstrap_ws_headerrepeat, #customize-control-wordstrap_ws_headerattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_headerboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_headerboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_headerboxshadowinset, #customize-control-wordstrap_ws_headerboxshadowhorz, #customize-control-wordstrap_ws_headerboxshadowvert, #customize-control-wordstrap_ws_headerboxshadowblur, #customize-control-wordstrap_ws_headerboxshadowspread, #customize-control-wordstrap_ws_headerboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_headerborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_headerborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_headerborderradiustopleft, #customize-control-wordstrap_ws_headerborderradiustopright, #customize-control-wordstrap_ws_headerborderradiusbtmright, #customize-control-wordstrap_ws_headerborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_headermargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_headermargin").val() === "default") {
			$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headermargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_headermargintop, #customize-control-wordstrap_ws_headermarginbtm, #customize-control-wordstrap_ws_headermarginleft, #customize-control-wordstrap_ws_headermarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_headerpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_headerpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_headerpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_headerpaddingtop, #customize-control-wordstrap_ws_headerpaddingbtm, #customize-control-wordstrap_ws_headerpaddingleft, #customize-control-wordstrap_ws_headerpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_header');

function ws_customizer_option_mast() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for mast options
		$("#wordstrap_ws_mastoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach, #customize-control-wordstrap_ws_mastbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected mast option
		if ($("#wordstrap_ws_mastoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach, #customize-control-wordstrap_ws_mastbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "color") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastpatternopaque, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_mastoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_mastbackground, #customize-control-wordstrap_ws_mastpatternsheer, #customize-control-wordstrap_ws_mastpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_mastupload, #customize-control-wordstrap_ws_mastrepeat, #customize-control-wordstrap_ws_mastattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_mastboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_mastboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_mastboxshadowinset, #customize-control-wordstrap_ws_mastboxshadowhorz, #customize-control-wordstrap_ws_mastboxshadowvert, #customize-control-wordstrap_ws_mastboxshadowblur, #customize-control-wordstrap_ws_mastboxshadowspread, #customize-control-wordstrap_ws_mastboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_mastborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_mastborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_mastborderradiustopleft, #customize-control-wordstrap_ws_mastborderradiustopright, #customize-control-wordstrap_ws_mastborderradiusbtmright, #customize-control-wordstrap_ws_mastborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_mastmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_mastmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_mastmargintop, #customize-control-wordstrap_ws_mastmarginbtm, #customize-control-wordstrap_ws_mastmarginleft, #customize-control-wordstrap_ws_mastmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_mastpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_mastpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_mastpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_mastpaddingtop, #customize-control-wordstrap_ws_mastpaddingbtm, #customize-control-wordstrap_ws_mastpaddingleft, #customize-control-wordstrap_ws_mastpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_mast');

function ws_customizer_option_navbar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for navbar bkgd options
		$("#wordstrap_ws_navbaroption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach, #customize-control-wordstrap_ws_navbarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar option
		if ($("#wordstrap_ws_navbaroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach, #customize-control-wordstrap_ws_navbarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "color") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarpatternopaque, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_navbaroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_navbarbackground, #customize-control-wordstrap_ws_navbarpatternsheer, #customize-control-wordstrap_ws_navbarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_navbarupload, #customize-control-wordstrap_ws_navbarrepeat, #customize-control-wordstrap_ws_navbarattach").show().removeClass("hidden");
		}
		// custom js for navbar brand options
		$("#wordstrap_ws_navbarbrand").change(function () {
			switch ($(this).val()) {
			case "masthead-brand":
				$("#customize-control-wordstrap_ws_navbarbrandlogo").hide().addClass("hidden");
				break;
			case "navbar-brand":
				$("#customize-control-wordstrap_ws_navbarbrandlogo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected navbar brand option
		if ($("#wordstrap_ws_navbarbrand").val() === "masthead-brand") {
			$("#customize-control-wordstrap_ws_navbarbrandlogo").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarbrand").val() === "navbar-brand") {
			$("#customize-control-wordstrap_ws_navbarbrandlogo").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_navbarboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_navbarboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_navbarboxshadowinset, #customize-control-wordstrap_ws_navbarboxshadowhorz, #customize-control-wordstrap_ws_navbarboxshadowvert, #customize-control-wordstrap_ws_navbarboxshadowblur, #customize-control-wordstrap_ws_navbarboxshadowspread, #customize-control-wordstrap_ws_navbarboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_navbarborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_navbarborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_navbarborderradiustopleft, #customize-control-wordstrap_ws_navbarborderradiustopright, #customize-control-wordstrap_ws_navbarborderradiusbtmright, #customize-control-wordstrap_ws_navbarborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_navbarmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_navbarmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_navbarmargintop, #customize-control-wordstrap_ws_navbarmarginbtm, #customize-control-wordstrap_ws_navbarmarginleft, #customize-control-wordstrap_ws_navbarmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_navbarpadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_navbarpadding").val() === "default") {
			$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_navbarpadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_navbarpaddingtop, #customize-control-wordstrap_ws_navbarpaddingbtm, #customize-control-wordstrap_ws_navbarpaddingleft, #customize-control-wordstrap_ws_navbarpaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_navbar');

function ws_customizer_option_sidebar() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for sidebar options
		$("#wordstrap_ws_sidebaroption").change(function () {
			switch ($(this).val()) {
			case "auto":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "transparent":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected sidebar option
		if ($("#wordstrap_ws_sidebaroption").val() === "auto") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach, #customize-control-wordstrap_ws_sidebarbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "color") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarpatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarpatternopaque, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_sidebaroption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_sidebarbackground, #customize-control-wordstrap_ws_sidebarpatternsheer, #customize-control-wordstrap_ws_sidebarpatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_sidebarupload, #customize-control-wordstrap_ws_sidebarrepeat, #customize-control-wordstrap_ws_sidebarattach").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_sidebar');

function ws_customizer_option_wrap() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for wrap options
		$("#wordstrap_ws_wrapoption").change(function () {
			switch ($(this).val()) {
			case "transparent":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach, #customize-control-wordstrap_ws_wrapbackground").hide().addClass("hidden");
				break;
			case "color":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground").show().removeClass("hidden");
				break;
			case "gradient":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternsheer":
				$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "patternopaque":
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternsheer").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadsheer":
				$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			case "uploadopaque":
				$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected wrap option
		if ($("#wordstrap_ws_wrapoption").val() === "transparent") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach, #customize-control-wordstrap_ws_wrapbackground").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "color") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "gradient") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wrapupload").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "patternsheer") {
			$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "patternopaque") {
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wrappatternsheer").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrappatternopaque, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "uploadsheer") {
			$("#customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_wrapoption").val() === "uploadopaque") {
			$("#customize-control-wordstrap_ws_wrapbackground, #customize-control-wordstrap_ws_wrappatternsheer, #customize-control-wordstrap_ws_wrappatternopaque").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_wrapupload, #customize-control-wordstrap_ws_wraprepeat, #customize-control-wordstrap_ws_wrapattach").show().removeClass("hidden");
		}
		// custom js for the box shadow
		$("#wordstrap_ws_wrapboxshadow").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").hide().addClass("hidden");
				break;
			case "shadow":
				$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected box shadow option
		if ($("#wordstrap_ws_wrapboxshadow").val() === "none") {
			$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapboxshadow").val() === "shadow") {
			$("#customize-control-wordstrap_ws_wrapboxshadowinset, #customize-control-wordstrap_ws_wrapboxshadowhorz, #customize-control-wordstrap_ws_wrapboxshadowvert, #customize-control-wordstrap_ws_wrapboxshadowblur, #customize-control-wordstrap_ws_wrapboxshadowspread, #customize-control-wordstrap_ws_wrapboxshadowopacity").show().removeClass("hidden");
		}
		// custom js for the border radius
		$("#wordstrap_ws_wrapborderradius").change(function () {
			switch ($(this).val()) {
			case "none":
				$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").hide().addClass("hidden");
				break;
			case "radius":
				$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected border radius option
		if ($("#wordstrap_ws_wrapborderradius").val() === "none") {
			$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapborderradius").val() === "radius") {
			$("#customize-control-wordstrap_ws_wrapborderradiustopleft, #customize-control-wordstrap_ws_wrapborderradiustopright, #customize-control-wordstrap_ws_wrapborderradiusbtmright, #customize-control-wordstrap_ws_wrapborderradiusbtmleft").show().removeClass("hidden");
		}
		// custom js for the margin
		$("#wordstrap_ws_wrapmargin").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").hide().addClass("hidden");
				break;
			case "margin":
				$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected margin option
		if ($("#wordstrap_ws_wrapmargin").val() === "default") {
			$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrapmargin").val() === "margin") {
			$("#customize-control-wordstrap_ws_wrapmargintop, #customize-control-wordstrap_ws_wrapmarginbtm, #customize-control-wordstrap_ws_wrapmarginleft, #customize-control-wordstrap_ws_wrapmarginright").show().removeClass("hidden");
		}
		// custom js for the padding
		$("#wordstrap_ws_wrappadding").change(function () {
			switch ($(this).val()) {
			case "default":
				$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").hide().addClass("hidden");
				break;
			case "padding":
				$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected padding option
		if ($("#wordstrap_ws_wrappadding").val() === "default") {
			$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").hide().addClass("hidden");
		}
		if ($("#wordstrap_ws_wrappadding").val() === "padding") {
			$("#customize-control-wordstrap_ws_wrappaddingtop, #customize-control-wordstrap_ws_wrappaddingbtm, #customize-control-wordstrap_ws_wrappaddingleft, #customize-control-wordstrap_ws_wrappaddingright").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_wrap');