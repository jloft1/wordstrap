<?php

// WordStrap Javascript for Theme Customizer
// http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-overview/
function ws_customizer_option_brand() { ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		"use strict";
		// custom js for brand options
		$("#wordstrap_ws_brand").change(function () {
			switch ($(this).val()) {
			case "one":
				$("#customize-control-wordstrap_ws_brand_mark, #customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_]").show().removeClass("hidden");
				break;
			case "two":
				$("#customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").show().removeClass("hidden");
				break;
			case "three":
				$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").hide().addClass("hidden");
				$("#customize-control-wordstrap_ws_brand_logo").show().removeClass("hidden");
				break;
			}
		});
		// show and hide sections on page load based off of the currently selected brand option
		if ($("#wordstrap_ws_brand").val() === "one") {
			$("#customize-control-wordstrap_ws_brand_mark, #customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_]").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_brand").val() === "two") {
			$("#customize-control-wordstrap_ws_brand_logo").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").show().removeClass("hidden");
		}
		if ($("#wordstrap_ws_brand").val() === "three") {
			$("#customize-control-wordstrap_ws_brand_font_text, [id^=customize-control-wordstrap_ws_brand_font_type_], #customize-control-wordstrap_ws_brand_mark").hide().addClass("hidden");
			$("#customize-control-wordstrap_ws_brand_logo").show().removeClass("hidden");
		}
	});
</script>

<?php
}

add_action('customize_controls_print_footer_scripts', 'ws_customizer_option_brand');