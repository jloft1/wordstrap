<?php
class WP_Customize_SelectShowHide_Control extends WP_Customize_Control {

    public $type = 'selectshowhide';
    
    public function render_content() {
    
	    if ( empty( $this->choices ) )
					return;

				?>
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<select <?php $this->link(); ?> id="<?php echo esc_html( $this->id ); ?>">
						<?php
						foreach ( $this->choices as $value => $label )
							echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . '>' . $label . '</option>';
						?>
					</select>
				</label>
				<?php

    }
}