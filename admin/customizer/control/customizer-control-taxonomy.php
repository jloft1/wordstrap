<?php
class WP_Customize_Taxonomy_Control extends WP_Customize_Control {
    public $type = 'taxonomy_dropdown';
    var $defaults = array();
    public $args = array();

    public function render_content(){
        // Call wp_dropdown_cats to ad data-customize-setting-link to select tag
        add_action('wp_dropdown_cats', array($this, 'wp_dropdown_cats'));

        // Set some defaults for our control
        $this->defaults = array(
            'show_option_none' => __('None'),
        	'orderby' => 'name',
            'hide_empty' => 0,
            'id' => $this->id,
            'selected' => $this->value(),
        );

        // Parse defaults against what the user submitted
        $r = wp_parse_args($this->args, $this->defaults);

?>
		<label><span class="customize-control-title"><?php echo esc_html($this->label); ?></span></label>
<?php
        // Generate our select box
        wp_dropdown_categories($r);
    }

    function wp_dropdown_cats($output){
        // Search for <select and replace it with <select data-customize=setting-link="my_control_id"
        $output = str_replace('<select', '<select ' . $this->get_link(), $output);
        return $output;
    }
}