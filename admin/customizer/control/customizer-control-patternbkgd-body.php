<?php
class WP_Customize_PatternBkgd_Control_body extends WP_Customize_Control {

    public $type = 'bodypattern';
    
    public function render_content() {
    
			if ( empty( $this->choices ) )
			return;    
		
			$name = 'ws_bodypattern' .'['. $this->id .']';
			$bodybkgdcolor = of_get_option('ws_bodybackground');
	
			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $bodybkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>	
					</label>
					<?php
				endforeach;

    }
}