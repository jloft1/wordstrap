<?php
class WP_Customize_Typography_Control_styles extends WP_Customize_Control {

    public $type = 'typography_styles';
    
    public function render_content() {
    
			if ( empty( $this->choices ) )
			return;    
	
			?>
				<select <?php $this->link(); ?>>
					<?php
					foreach ( $this->choices as $value => $label )
						echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . '>' . $label . '</option>';
					?>
				</select>
			<?php

    }
}