<?php
class WP_Customize_Typography_Control_sizes extends WP_Customize_Control {

    public $type = 'typography_sizes';
    
    public function render_content() {
    
			if ( empty( $this->choices ) )
			return;    
	
			?>
				<select <?php $this->link(); ?>>
					<?php
					foreach ( $this->choices as $value => $label )
						echo '<option value="' . esc_attr( $label.'px' ) . '"' . selected( $this->value(), $label.'px', false ) . '>' . $label.'px' . '</option>';
					?>
				</select>
			<?php

    }
}