<?php
class WP_Customize_PatternBkgd_Control_body extends WP_Customize_Control {

    public $type = 'bodypattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_bodypattern' .'['. $this->id .']';
			$bodybkgdcolor = of_get_option('ws_bodybackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $bodybkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_colophon extends WP_Customize_Control {

    public $type = 'colophonpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_colophonpattern' .'['. $this->id .']';
			$colophonbkgdcolor = of_get_option('ws_colophonbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $colophonbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_content extends WP_Customize_Control {

    public $type = 'contentpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_contentpattern' .'['. $this->id .']';
			$contentbkgdcolor = of_get_option('ws_contentbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $contentbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_feature extends WP_Customize_Control {

    public $type = 'featurepattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_featurepattern' .'['. $this->id .']';
			$featurebkgdcolor = of_get_option('ws_featurebackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $featurebkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_footer extends WP_Customize_Control {

    public $type = 'footerpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_footerpattern' .'['. $this->id .']';
			$footerbkgdcolor = of_get_option('ws_footerbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $footerbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_header extends WP_Customize_Control {

    public $type = 'headerpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_headerpattern' .'['. $this->id .']';
			$headerbkgdcolor = of_get_option('ws_headerbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $headerbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_mast extends WP_Customize_Control {

    public $type = 'mastpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_mastpattern' .'['. $this->id .']';
			$mastbkgdcolor = of_get_option('ws_mastbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $mastbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_navbar extends WP_Customize_Control {

    public $type = 'navbarpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_navbarpattern' .'['. $this->id .']';
			$ws_navbarscheme = of_get_option('ws_navbarscheme');
				if ( $ws_navbarscheme == 'navbar-light' ) {
					$navbarbkgdcolor = of_get_option('ws_navbardefaultbackground');
				}
				if ( $ws_navbarscheme == 'navbar-dark' ) {
					$navbarbkgdcolor = of_get_option('ws_navbarinversebackground');
				}

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $navbarbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_sidebar extends WP_Customize_Control {

    public $type = 'sidebarpattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_sidebarpattern' .'['. $this->id .']';
			$sidebarbkgdcolor = of_get_option('ws_sidebarbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $sidebarbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_PatternBkgd_Control_wrap extends WP_Customize_Control {

    public $type = 'wrappattern';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			$name = 'ws_wrappattern' .'['. $this->id .']';
			$wrapbkgdcolor = of_get_option('ws_wrapbackground');

			?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php
				foreach ( $this->choices as $value => $option ) :
					?>
					<label>
						<input type="radio" class="radio-img-input" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> style="display:none;" />
						<div class="radio-img-div">
							<span style="background-image:url('<?php echo esc_url( $option ) ?>'); background-color:<?php echo esc_attr( $wrapbkgdcolor ) ?>;" class="radio-img-css-bkgd of-radio-img-img" onclick="document.getElementbyId( <?php echo esc_attr( $this->id.'_'.$value); ?> ).checked=true;"></span>
						</div>
					</label>
					<?php
				endforeach;

    }
}
class WP_Customize_SelectShowHide_Control extends WP_Customize_Control {

    public $type = 'selectshowhide';

    public function render_content() {

	    if ( empty( $this->choices ) )
					return;

				?>
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<select <?php $this->link(); ?> id="<?php echo esc_html( $this->id ); ?>">
						<?php
						foreach ( $this->choices as $value => $label )
							echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . '>' . $label . '</option>';
						?>
					</select>
				</label>
				<?php

    }
}
class WP_Customize_Taxonomy_Control extends WP_Customize_Control {
    public $type = 'taxonomy_dropdown';
    var $defaults = array();
    public $args = array();

    public function render_content(){
        // Call wp_dropdown_cats to ad data-customize-setting-link to select tag
        add_action('wp_dropdown_cats', array($this, 'wp_dropdown_cats'));

        // Set some defaults for our control
        $this->defaults = array(
            'show_option_none' => __('None'),
        	'orderby' => 'name',
            'hide_empty' => 0,
            'id' => $this->id,
            'selected' => $this->value(),
        );

        // Parse defaults against what the user submitted
        $r = wp_parse_args($this->args, $this->defaults);

?>
		<label><span class="customize-control-title"><?php echo esc_html($this->label); ?></span></label>
<?php
        // Generate our select box
        wp_dropdown_categories($r);
    }

    function wp_dropdown_cats($output){
        // Search for <select and replace it with <select data-customize=setting-link="my_control_id"
        $output = str_replace('<select', '<select ' . $this->get_link(), $output);
        return $output;
    }
}
class WP_Customize_Typography_Control_close extends WP_Customize_Control {

    public $type = 'typography_close';

    public function render_content() {

			?>
			</ul>
			<?php

    }
}
class WP_Customize_Typography_Control_faces extends WP_Customize_Control {

    public $type = 'typography_faces';

    public function render_content() {

    			if ( empty( $this->choices ) )
			return;

			?>
				<select <?php $this->link(); ?>>
					<?php
					foreach ( $this->choices as $value => $label )
						echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . '>' . $label . '</option>';
					?>
				</select>
			<?php

    }
}
class WP_Customize_Typography_Control_open extends WP_Customize_Control {

    public $type = 'typography_open';

    public function render_content() {

			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			</label>
			<ul>
			<?php

    }
}
class WP_Customize_Typography_Control_sizes extends WP_Customize_Control {

    public $type = 'typography_sizes';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			?>
				<select <?php $this->link(); ?>>
					<?php
					foreach ( $this->choices as $value => $label )
						echo '<option value="' . esc_attr( $label.'px' ) . '"' . selected( $this->value(), $label.'px', false ) . '>' . $label.'px' . '</option>';
					?>
				</select>
			<?php

    }
}
class WP_Customize_Typography_Control_styles extends WP_Customize_Control {

    public $type = 'typography_styles';

    public function render_content() {

			if ( empty( $this->choices ) )
			return;

			?>
				<select <?php $this->link(); ?>>
					<?php
					foreach ( $this->choices as $value => $label )
						echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . '>' . $label . '</option>';
					?>
				</select>
			<?php

    }
}