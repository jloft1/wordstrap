<?php

//** GENERAL CUSTOM FUNCTIONS **//

if (!defined('__DIR__')) { define('__DIR__', dirname(__FILE__)); }

// Set the PHP Memory Limit higher in order to make the Theme Customizer work properly.
ini_set("memory_limit","256M");

// WordStrap Theme Options
	require_once locate_template('/admin/functions.php');
// Include the options.php file explicitly in order to enable compatibility with the WordPress Front End Theme Customizer.
	require_once locate_template('/admin/options.php');
// Make the Theme Customizer more visible
	add_action ('admin_menu', 'theme_customizer_menu');
		function theme_customizer_menu() {
   		// add the Customize link to the admin menu
   		add_theme_page( 'Theme Customizer', 'Theme Customizer', 'edit_theme_options', 'customize.php' );
   	}

// WP-LESS
add_action('wp_enqueue_scripts', 'ws_wp_less');
if ( ! function_exists( 'ws_wp_less' ) ):
	function ws_wp_less() {
		/* Load Bootstrap CSS */
		require_once locate_template('/lib/wp-less/bootstrap-for-theme.php');
		$less = WPLessPlugin::getInstance();
		$less->dispatch();
		register_activation_hook(__FILE__, array($less, 'install'));
		register_deactivation_hook(__FILE__, array($less, 'uninstall'));
	}
endif;

/*-----------------------------------------------------------------------------------*/

//** SIDEBARS - Register Additional Sidebars (Dynamic Widget Areas) **//

function ws_widgets_register() {
	register_sidebar(array(
	'name'          => __('Super Footer Col 1', 'wordstrap'),
	'id'            => 'sidebar-superfooter-col1',
	'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
	'after_widget'  => '</div></section>',
	'before_title'  => '<h3>',
	'after_title'   => '</h3>',
	));
	register_sidebar(array(
	'name'          => __('Super Footer Col 2', 'wordstrap'),
	'id'            => 'sidebar-superfooter-col2',
	'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
	'after_widget'  => '</div></section>',
	'before_title'  => '<h3>',
	'after_title'   => '</h3>',
	));
	register_sidebar(array(
	'name'          => __('Super Footer Col 3', 'wordstrap'),
	'id'            => 'sidebar-superfooter-col3',
	'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
	'after_widget'  => '</div></section>',
	'before_title'  => '<h3>',
	'after_title'   => '</h3>',
	));
}
add_action('widgets_init', 'ws_widgets_register');

/*-----------------------------------------------------------------------------------*/

//** CUSTOM Image Sizes **//

function ws_add_image_sizes() {
    add_image_size( 'ws-thumb43', 160, 120, true );
    add_image_size( 'ws-thumb64', 160, 107, true );
    add_image_size( 'ws-postwidth', 770 );
    add_image_size( 'ws-fullwidth', 1170 );
}
add_action( 'init', 'ws_add_image_sizes' );

function ws_show_image_sizes($sizes) {
    $sizes['ws-thumb43'] = __( '4:3 Aspect Ratio Thumbnail', 'wordstrap' );
    $sizes['ws-thumb64'] = __( '6:4 Aspect Ratio Thumbnail', 'wordstrap' );
    $sizes['ws-postwidth'] = __( 'Post Width with Sidebar', 'wordstrap' );
    $sizes['ws-fullwidth'] = __( 'Full Width with No Sidebar', 'wordstrap' );

    return $sizes;
}
add_filter('image_size_names_choose', 'ws_show_image_sizes');

/*-----------------------------------------------------------------------------------*/

//** CUSTOM Button Classes **//

function ws_button_class() {
	$buttonclass = get_theme_mod( 'default_button_style', 'btn-primary' );
	echo $buttonclass;
}

/*-----------------------------------------------------------------------------------*/

//** GRID CONTENT **//
// @param int $columns Number of columns
// @return string $class class for each column of grid

if( ! function_exists( 'ws_grid_class' ) ) {
	function ws_grid_class( $columns ) {
		$class = 'span3'; // default
		if( $columns == 2 )
			$class = 'span6';
		else if( $columns == 3 )
			$class = 'span4';
		else if( $columns == 4 )
			$class = 'span3';
		else if( $columns == 5 )
			$class = 'span5 offset1';
		return $class;
	}
}

// Open a row in a post grid
if( ! function_exists( 'ws_open_row' ) ) {
	function ws_open_row() {
		echo apply_filters( 'ws_open_row', '<div class="row">' );
	}
}

// Close a row in a post grid
if( ! function_exists( 'ws_close_row' ) ) {
	function ws_close_row() {
		echo apply_filters( 'ws_close_row', '<div class="clearfix"></div></div><!-- /.row -->' );
	}
}


/*-----------------------------------------------------------------------------------*/

//** CUSTOM HOOKS, ACTIONS, + FILTERS **//

// Get Some Global Variable Options
$ws_bodyoption 		= of_get_option('ws_bodyoption');

$ws_wrapcontainer 		= of_get_option('ws_wrapcontainer');
$ws_wrapoption 		= of_get_option('ws_wrapoption');
$ws_wrappaddingtop 		= of_get_option('ws_wrappaddingtop');
$ws_wrapmargintop 		= of_get_option('ws_wrapmargintop');

$ws_headercontainer 	= of_get_option('ws_headercontainer');
$ws_headeroption 		= of_get_option('ws_headeroption');
$ws_headerpaddingtop 	= of_get_option('ws_headerpaddingtop');
$ws_headermargintop 	= of_get_option('ws_headermargintop');

$ws_mastcontainer 		= of_get_option('ws_mastcontainer');
$ws_mastoption 		= of_get_option('ws_mastoption');
$ws_mastheight 		= of_get_option('ws_mastheight');
$ws_mastpaddingtop 		= of_get_option('ws_mastpaddingtop');
$ws_mastmargintop 		= of_get_option('ws_mastmargintop');
$ws_mastpaddingbtm 		= of_get_option('ws_mastpaddingbtm');
$ws_mastmarginbtm 		= of_get_option('ws_mastmarginbtm');

$ws_navbarcontainer 	= of_get_option('ws_navbarcontainer');
$ws_navbartype 		= of_get_option('ws_navbartype');
$ws_navbarscheme 		= of_get_option('ws_navbarscheme');
$ws_navbarfixed 		= of_get_option('ws_navbarfixed');
$ws_navbarposition 		= of_get_option('ws_navbarposition');
$ws_navbarheight 		= of_get_option('ws_navbarheight');
$ws_navbaroption 		= of_get_option('ws_navbaroption');
$ws_navbartextshadow 	= of_get_option('ws_navbartextshadow');
$ws_navbarvertdividers 	= of_get_option('ws_navbarvertdividers');
$ws_navbarmargintop 	= of_get_option('ws_navbarmargintop');

$ws_featurecontainer 	= of_get_option('ws_featurecontainer');
$ws_featureoption 		= of_get_option('ws_featureoption');

$ws_contentcontainer 	= of_get_option('ws_contentcontainer');
$ws_contentoption 		= of_get_option('ws_contentoption');

$ws_sidebaroption 		= of_get_option('ws_sidebaroption');

$ws_footercontainer 	= of_get_option('ws_footercontainer');
$ws_footeroption 		= of_get_option('ws_footeroption');

$ws_colophoncontainer 	= of_get_option('ws_colophoncontainer');
$ws_colophonoption 		= of_get_option('ws_colophonoption');


// Register WordStrap Hooks
define( 'WS_HOOKS_VERSION', '1.0' );
add_theme_support( 'ws_hooks', array ( 'all','ws_wrap_class','ws_banner_class','ws_masthead_class','ws_navbar_class','ws_affix','ws_feature_class','ws_content_class','ws_sidebar_class','ws_footer_class','ws_colophon_class' ) );
function ws_current_theme_supports ( $bool, $args, $registered ) {
	return in_array( $args[0], $registered[0] ) || in_array( 'all', $registered[0] );
}
add_filter( 'current_theme_supports-ws_hooks', 'ws_current_theme_supports', 10, 3 );


// Filter Body Class (this filters an existing WP class)

function ws_body_class($classes) {
	global $ws_bodyoption, $ws_navbarfixed;
	if ( $ws_bodyoption == 'gradient' ) : $classes[] = 'gradient'; endif;
	//if ( $ws_navbarfixed == 'navbar-fixed' ) : $classes[] = 'body-navbar-fixed-top'; endif;
	return $classes;
}

add_filter('body_class', 'ws_body_class');


// Register WRAP class

	function ws_wrap_class( $class = '') {
		// Separates classes with a single space, collates classes for the wrap element
		echo 'class="' . join( ' ', ws_wrap_data( $class ) ) . '"';
	}

	function ws_wrap_data( $class = '' ) {
		$classes = array();
		$classes[] = 'wrap';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_wrap_class', $classes, $class );
	}

// Filter WRAP class

	function ws_wrap_variables ( $classes ) {
		global $ws_wrapcontainer, $ws_wrapoption, $ws_navbarfixed;
		if ( $ws_wrapcontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_wrapcontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_wrapoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		//if ( $ws_navbarfixed == 'navbar-fixed') : $classes[] = 'wrap-navbar-fixed-top'; endif;
		return $classes;
	}

	add_filter ('ws_wrap_class','ws_wrap_variables');


// Register BANNER class

	function ws_banner_class( $class = '') {
		// Separates classes with a single space, collates classes for the banner element
		echo 'class="' . join( ' ', ws_banner_data( $class ) ) . '"';
	}

	function ws_banner_data( $class = '' ) {
		$classes = array();
		$classes[] = 'banner';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_banner_class', $classes, $class );
	}

// Filter BANNER class

	function ws_banner_variables ( $classes ) {
		global $ws_headercontainer, $ws_headeroption;
		if ( $ws_headercontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_headercontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_headeroption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_banner_class','ws_banner_variables');


// Register MASTHEAD class

	function ws_masthead_class( $class = '') {
		// Separates classes with a single space, collates classes for the masthead element
		echo 'class="' . join( ' ', ws_masthead_data( $class ) ) . '"';
	}

	function ws_masthead_data( $class = '' ) {
		$classes = array();
		$classes[] = 'masthead mast';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_masthead_class', $classes, $class );
	}

// Filter MASTHEAD class

	function ws_masthead_variables ( $classes ) {
		global $ws_mastcontainer, $ws_mastoption;
		if ( $ws_mastcontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_mastcontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_mastoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_masthead_class','ws_masthead_variables');


// Register NAVBAR class

	function ws_navbar_class( $class = '') {
		// Separates classes with a single space, collates classes for the navbar element
		echo 'class="' . join( ' ', ws_navbar_data( $class ) ) . '"';
	}

	function ws_navbar_data( $class = '' ) {
		$classes = array();
		$classes[] = 'navbar wait-till-load';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_navbar_class', $classes, $class );
	}

// Filter NAVBAR class

	function ws_navbar_variables( $classes ) {

		global $ws_navbartype, $ws_navbarscheme, $ws_navbarfixed, $ws_navbarposition, $ws_navbaroption, $ws_navbartextshadow, $ws_navbarvertdividers;

		if ($ws_navbartype == 'navbar-std') : $classes[] = 'navbar-std'; endif;
		if ($ws_navbartype == 'navbar-pills') : $classes[] = 'navbar-pills'; endif;
		if ($ws_navbartype == 'navbar-tabs') : $classes[] = 'navbar-tabs'; endif;
		if ($ws_navbarscheme == 'navbar-light') : $classes[] = 'navbar-default'; endif;
		if ($ws_navbarscheme == 'navbar-dark') : $classes[] = 'navbar-inverse'; endif;
		if ($ws_navbarscheme == 'navbar-transparent') : $classes[] = 'navbar-transparent'; endif;
		if ($ws_navbarfixed == 'navbar-fixed') : $classes[] = 'navbar-fixed-top'; endif;
		if ($ws_navbarfixed == 'navbar-static') : $classes[] = 'navbar-static-top'; endif;
		if ($ws_navbarposition == 'navbar-pos-top') : $classes[] = 'navbar-pos-top'; endif;
		if ($ws_navbarposition == 'navbar-pos-btm') : $classes[] = 'navbar-pos-btm'; endif;
		if ($ws_navbaroption == 'gradient' ) : $classes[] = 'gradient'; endif;
		if ($ws_navbaroption == 'transparent' ) : $classes[] = 'transparent'; endif;
		if ($ws_navbartextshadow == 'navbar-textshadow-no' ) : $classes[] = 'textshadow-no'; endif;
		if ($ws_navbarvertdividers == 'navbar-vertdividers-yes' ) : $classes[] = 'vertdividers'; endif;

		return $classes;
	}

	add_filter('ws_navbar_class', 'ws_navbar_variables');

// Register NAVBAR-INNER class

	function ws_navbar_inner_class( $class = '') {
		// Separates classes with a single space, collates classes for the navbar-inner element
		echo 'class="' . join( ' ', ws_navbar_inner_data( $class ) ) . '"';
	}

	function ws_navbar_inner_data( $class = '' ) {
		$classes = array();
		$classes[] = 'navbar-inner';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_navbar_inner_class', $classes, $class );
	}

// Filter NAVBAR-INNER class

	function ws_navbar_inner_variables( $classes ) {

		global $ws_navbarcontainer;

		if ($ws_navbarcontainer == 'contain') : $classes[] = 'contain'; endif;
		if ( $ws_navbarcontainer == 'span' ) : $classes[] = 'fullwidth'; endif;

		return $classes;
	}

	add_filter('ws_navbar_inner_class', 'ws_navbar_inner_variables');


// Place the NAVBAR via the appropriate HEADER 'THA' hook

	function ws_navbar_placement ( $ws_navbar_placement ) {
		$ws_navbar_placement = get_template_part('templates/banner/topnav');
	}

	if ( $ws_navbarposition == 'navbar-pos-top' ) {
		add_filter ('tha_header_top','ws_navbar_placement');
	}
	if ( $ws_navbarposition == 'navbar-pos-btm' ) {
		add_filter ('tha_header_bottom','ws_navbar_placement');
	}


// Filter MENU class (this filters an existing WP class)

	function ws_menu_variables( $classes ) {

		global $ws_navbartype;

		if ($ws_navbartype == 'navbar-std') : $classes['menu_class'] = 'nav nav-std'; endif;
		if ($ws_navbartype == 'navbar-pills') : $classes['menu_class'] = 'nav nav-pills'; endif;
		if ($ws_navbartype == 'navbar-tabs') : $classes['menu_class'] = 'nav nav-tabs'; endif;

		return $classes;
	}

	add_filter('wp_nav_menu_args', 'ws_menu_variables');


// Register AFFIX class

	function ws_affix( $ws_affix = '') {
		// Separates properties with a single space, collates properties for affix data element
		echo ws_affix_data( $ws_affix );
	}

	function ws_affix_data( $ws_affix = '' ) {
		$ws_affix = ' data-spy="affix" data-offset-top="100"';
		return apply_filters( 'ws_affix', $ws_affix );
	}

// Filter AFFIX class

	function ws_affix_navbar( $ws_affix = '' ) {

		global $ws_navbarfixed, $ws_navbarposition, $ws_navbarheight, $ws_navbarmargintop, $ws_mastheight, $ws_mastmargintop, $ws_mastmarginbtm, $ws_mastpaddingtop, $ws_mastpaddingbtm, $ws_headerpaddingtop, $ws_headermargintop, $ws_wrappaddingtop, $ws_wrapmargintop;

		// By subtracting zero, we are able to remove the "px" from the ws_mastheight option
		$ws_data_offset_top = $ws_navbarmargintop + $ws_mastmarginbtm + $ws_mastpaddingbtm + $ws_mastheight + $ws_mastpaddingtop + $ws_mastmargintop + $ws_headerpaddingtop + $ws_headermargintop + $ws_wrappaddingtop + $ws_wrapmargintop - 0;

		if ( $ws_navbarfixed == 'navbar-fixed' ) {
			if ($ws_navbarposition == 'navbar-pos-top') : $ws_affix = ' data-spy="affix" data-offset-top="'.$ws_data_offset_top.'"'; endif;
			if ($ws_navbarposition == 'navbar-pos-btm') : $ws_affix = ' data-spy="affix" data-offset-top="'.$ws_data_offset_top.'"'; endif;
		}
		else {
		$ws_affix = '';
		}
		return $ws_affix;
	}

	add_filter('ws_affix', 'ws_affix_navbar');

// Register FEATURE class

	function ws_feature_class( $class = '') {
		// Separates classes with a single space, collates classes for the feature element
		echo 'class="' . join( ' ', ws_feature_data( $class ) ) . '"';
	}

	function ws_feature_data( $class = '' ) {
		$classes = array();
		$classes[] = 'feature';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_feature_class', $classes, $class );
	}

// Filter FEATURE class

	function ws_feature_variables ( $classes ) {
		global $ws_featurecontainer, $ws_featureoption;
		if ( $ws_featurecontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_featurecontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_featureoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_feature_class','ws_feature_variables');


// Register CONTENT class

	function ws_content_class( $class = '') {
		// Separates classes with a single space, collates classes for the content element
		echo 'class="' . join( ' ', ws_content_data( $class ) ) . '"';
	}

	function ws_content_data( $class = '' ) {
		$classes = array();
		$classes[] = 'content';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_content_class', $classes, $class );
	}

// Filter CONTENT class

	function ws_content_variables ( $classes ) {
		global $ws_contentcontainer, $ws_contentoption;
		if ( $ws_contentcontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_contentcontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_contentoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_content_class','ws_content_variables');


// Register MAIN class

	function ws_main_class( $class = '') {
		// Separates classes with a single space, collates classes for the main element
		echo 'class="' . join( ' ', ws_main_data( $class ) ) . '"';
	}

	function ws_main_data( $class = '' ) {
		$classes = array();
		$classes[] = 'main';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_main_class', $classes, $class );
	}

// Filter MAIN class

	function ws_main_variables ( $classes ) {
		global $ws_mainoption;
		if ( $ws_mainoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_main_class','ws_main_variables');


// Register SIDEBAR class

	function ws_sidebar_class( $class = '') {
		// Separates classes with a single space, collates classes for the sidebar element
		echo 'class="' . join( ' ', ws_sidebar_data( $class ) ) . '"';
	}

	function ws_sidebar_data( $class = '' ) {
		$classes = array();
		$classes[] = 'sidebar';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_sidebar_class', $classes, $class );
	}

// Filter SIDEBAR class

	function ws_sidebar_variables ( $classes ) {
		global $ws_sidebaroption;
		if ( $ws_sidebaroption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_sidebar_class','ws_sidebar_variables');


// Register FOOTER class

	function ws_footer_class( $class = '') {
		// Separates classes with a single space, collates classes for the footer element
		echo 'class="' . join( ' ', ws_footer_data( $class ) ) . '"';
	}

	function ws_footer_data( $class = '' ) {
		$classes = array();
		$classes[] = 'superfooter footer';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_footer_class', $classes, $class );
	}

// Filter FOOTER class

	function ws_footer_variables ( $classes ) {
		global $ws_footercontainer, $ws_footeroption;
		if ( $ws_footercontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_footercontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_footeroption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_footer_class','ws_footer_variables');


// Register COLOPHON class

	function ws_colophon_class( $class = '') {
		// Separates classes with a single space, collates classes for the colophon element
		echo 'class="' . join( ' ', ws_colophon_data( $class ) ) . '"';
	}

	function ws_colophon_data( $class = '' ) {
		$classes = array();
		$classes[] = 'colophon';
		if ( ! empty( $class ) ) {
	                if ( !is_array( $class ) )
	                        $class = preg_split( '#\s+#', $class );
	                $classes = array_merge( $classes, $class );
	        } else {
	                // Ensure that we always coerce class to being an array.
	                $class = array();
	        }
		$classes = array_map( 'esc_attr', $classes );
		return apply_filters( 'ws_colophon_class', $classes, $class );
	}

// Filter COLOPHON class

	function ws_colophon_variables ( $classes ) {
		global $ws_colophoncontainer, $ws_colophonoption;
		if ( $ws_colophoncontainer == 'contain' ) : $classes[] = 'contain'; endif;
		if ( $ws_colophoncontainer == 'span' ) : $classes[] = 'fullwidth'; endif;
		if ( $ws_colophonoption == 'gradient' ) : $classes[] = 'gradient'; endif;
		return $classes;
	}

	add_filter ('ws_colophon_class','ws_colophon_variables');

/*-----------------------------------------------------------------------------------*/

//** CUSTOM COMMENTS (From the geniuses at 320Press and WPBootstrap) **//

// Comment Layout
function ws_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="well clearfix">
			<div class="comment-author vcard row-fluid clearfix">
				<div class="avatar span2">
					<?php echo get_avatar($comment,$size='75',$default='<path_to_url>' ); ?>
				</div>
				<div class="span10 comment-text">

					<?php printf(__('<h4>%s</h4>','ws'), get_comment_author_link()) ?>

					<i class="icon-calendar">&nbsp;</i><time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>

					<?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert-message success">
          				<p><?php _e('Your comment is awaiting moderation.','ws') ?></p>
          				</div>
					<?php endif; ?>

					<?php comment_text() ?>

					<?php edit_comment_link(__('Edit','wordstrap'),'<span class="edit-comment btn btn-mini"><i class="icon-pencil"></i>','</span>') ?>
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

	function ws_comment_button_classes() {?>
	<script>
	jQuery(document).ready(function($) {
	  /* Add to cart buttons */
	  $(".comment-reply-link").addClass("btn btn-mini");
	  $('.comment-reply-link').prepend('<i class="icon-share-alt"></i> ');
	  $("#cancel-comment-reply-link").addClass("btn btn-danger btn-mini");
	  $('#cancel-comment-reply-link').prepend('<i class="icon-remove"></i> ');
	});
	</script>
	<?php
	}
?>