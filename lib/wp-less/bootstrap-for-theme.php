<?php
/*
 * Defines LESS variables based on the options panel settings and compiles all LESS into cached CSS files.
 */
if (!class_exists('WPLessPlugin'))
{
  require_once get_template_directory() . '/lib/wp-less/lib/Plugin.class.php';
  $WPLessPlugin = WPPluginToolkitPlugin::create('WPLess', __FILE__, 'WPLessPlugin');
  include_once get_template_directory() . '/lib/wp-less/lib/helper/ThemeHelper.php';
  $WPLessHelper = WPPluginToolkitPlugin::create('WPLess', __FILE__, 'WPLessHelper');  

  // This gets the theme name from the stylesheet
  $theme_name = get_option( 'stylesheet' );
  // Sets variable for theme root
  $theme_root_uri = get_theme_root_uri();
  $theme_root = get_theme_root();  
  // Sets the upload directory
  $WPLessPlugin->getConfiguration()->setUploadUrl($theme_root_uri.'/'.$theme_name.'/assets/css/');
  $WPLessPlugin->getConfiguration()->setUploadDir($theme_root.'/'.$theme_name.'/assets/css/');
}

if (!is_admin())
{

/* Options for the Framework LESS file */

// Component Sizing
	$ws_baseBorderRadius = of_get_option('ws_baseborderradius');
		$WPLessPlugin->addVariable('@baseBorderRadius', $ws_baseBorderRadius);
	$ws_borderRadiusLarge = of_get_option('ws_borderradiuslarge');
		$WPLessPlugin->addVariable('@borderRadiusLarge', $ws_borderRadiusLarge);	
	$ws_borderRadiusSmall = of_get_option('ws_borderradiussmall');
		$WPLessPlugin->addVariable('@borderRadiusSmall', $ws_borderRadiusSmall);			
	
// Links
    	$ws_linkColor = of_get_option('ws_linkcolor');
		$WPLessPlugin->addVariable('@linkColor', $ws_linkColor);
    	$ws_linkColorHover = of_get_option('ws_linkcolorhover');
    		if ( $ws_linkColorHover == '' ) {
		    	$WPLessPlugin->addVariable('@linkColorHover', 'darken(@linkColor, 15%)');
	    	}
	    	if ( $ws_linkColorHover != '' ) {
		    	$WPLessPlugin->addVariable('@linkColorHover', $ws_linkColorHover);
	    	}
	
// Colors
    	$ws_blue = of_get_option('ws_blue');
		$WPLessPlugin->addVariable('@blue', $ws_blue);	
    	$ws_blueDark = of_get_option('ws_bluedark');
		$WPLessPlugin->addVariable('@blueDark', $ws_blueDark);		
    	$ws_green = of_get_option('ws_green');
		$WPLessPlugin->addVariable('@green', $ws_green);
	$ws_red = of_get_option('ws_red');
		$WPLessPlugin->addVariable('@red', $ws_red);
	$ws_yellow = of_get_option('ws_yellow');
		$WPLessPlugin->addVariable('@yellow', $ws_yellow);
	$ws_orange = of_get_option('ws_orange');
		$WPLessPlugin->addVariable('@orange', $ws_orange);
	$ws_pink = of_get_option('ws_pink');
		$WPLessPlugin->addVariable('@pink', $ws_pink);
	$ws_purple = of_get_option('ws_purple');
		$WPLessPlugin->addVariable('@purple', $ws_purple);
	
// Typography
	$ws_sansFontFamily = of_get_option('ws_sansfontfamily');
		$WPLessPlugin->addVariable('@sansFontFamily', $ws_sansFontFamily);
	$ws_serifFontFamily = of_get_option('ws_seriffontfamily');
		$WPLessPlugin->addVariable('@serifFontFamily', $ws_serifFontFamily);
	$ws_monoFontFamily = of_get_option('ws_monofontfamily');
		$WPLessPlugin->addVariable('@monoFontFamily', $ws_monoFontFamily);
	$ws_altFontFamily = of_get_option('ws_altfontfamily');
		$WPLessPlugin->addVariable('@altFontFamily', $ws_altFontFamily);
	
	// Base Font
	$ws_baseFont = of_get_option('ws_basefont');
		$ws_baseFontSize = $ws_baseFont['size'];
		$ws_baseFontFamily = $ws_baseFont['face'];
		$ws_textColor = $ws_baseFont['color']; 
	$ws_baseLineHeight = of_get_option('ws_baselineheight');	
		$WPLessPlugin->addVariable('@baseFontSize', $ws_baseFontSize);
		$WPLessPlugin->addVariable('@baseFontFamily', $ws_baseFontFamily);
		$WPLessPlugin->addVariable('@baseLineHeight', $ws_baseLineHeight);
		$WPLessPlugin->addVariable('@textColor', $ws_textColor);
	
	// Headings
	$ws_headingsFont = of_get_option('ws_headingsfont');
		$ws_headingsFontFamily = $ws_headingsFont['face'];
		$ws_headingsFontWeight = $ws_headingsFont['style'];	
		$ws_headingsColor = $ws_headingsFont['color'];
			$WPLessPlugin->addVariable('@headingsFontFamily', $ws_headingsFontFamily);
			$WPLessPlugin->addVariable('@headingsFontWeight', $ws_headingsFontWeight);
			$WPLessPlugin->addVariable('@headingsColor', $ws_headingsColor);	
	
	// H1 Font - /* NOTE: The h1 variables are custom additions and require the ws.type.less file. */
	$ws_h1Font = of_get_option('ws_h1font');
		$ws_h1FontSize = $ws_h1Font['size'];
		$ws_h1FontFamily = $ws_h1Font['face'];
		$ws_h1FontWeight = $ws_h1Font['style'];	
		$ws_h1Color = $ws_h1Font['color'];
		$ws_h1LineHeight = of_get_option('ws_h1lineheight');	
			$WPLessPlugin->addVariable('@h1FontSize', $ws_h1FontSize);
			$WPLessPlugin->addVariable('@h1FontFamily', $ws_h1FontFamily);
			$WPLessPlugin->addVariable('@h1FontWeight', $ws_h1FontWeight);
			$WPLessPlugin->addVariable('@h1LineHeight', $ws_h1LineHeight);	
			$WPLessPlugin->addVariable('@h1Color', $ws_h1Color);	
	
	// Brand Font	
	$ws_brandFont = of_get_option('ws_brand_font_type');
		$ws_brandFontSize = $ws_brandFont['size'];
		$ws_brandFontFamily = $ws_brandFont['face'];
		$ws_brandFontWeight = $ws_brandFont['style'];
		$ws_brandColor = $ws_brandFont['color'];
			$WPLessPlugin->addVariable('@brandFontSize', $ws_brandFontSize);	
			$WPLessPlugin->addVariable('@brandFontFamily', $ws_brandFontFamily);
			$WPLessPlugin->addVariable('@brandFontWeight', $ws_brandFontWeight);
			$WPLessPlugin->addVariable('@brandColor', $ws_brandColor);	
			
	// Icon Fonts (sets the color of the icons)	
	$ws_iconFontColor = of_get_option('ws_iconfontcolor');
		$WPLessPlugin->addVariable('@iconFontColor', $ws_iconFontColor);				

// Hero Unit		
	$ws_heroUnitBackground = of_get_option('ws_herounitbackground');
		$WPLessPlugin->addVariable('@heroUnitBackground', $ws_heroUnitBackground);
	$ws_heroUnitHeadingColor = of_get_option('ws_herounitheadingcolor');
		if ( $ws_heroUnitHeadingColor == '' ) {
		    	$WPLessPlugin->addVariable('@heroUnitHeadingColor', 'transparent');
	    	}
	    	if ( $ws_heroUnitHeadingColor != '' ) {
		    	$WPLessPlugin->addVariable('@heroUnitHeadingColor', $ws_heroUnitHeadingColor);
	    	}
	$ws_heroUnitLeadColor = of_get_option('ws_herounitleadcolor');
		if ( $ws_heroUnitLeadColor == '' ) {
		    	$WPLessPlugin->addVariable('@heroUnitLeadColor', 'transparent');
	    	}
	    	if ( $ws_heroUnitLeadColor != '' ) {
		    	$WPLessPlugin->addVariable('@heroUnitLeadColor', $ws_heroUnitLeadColor);
	    	}
	
// Tables	
	$ws_tableBackground = of_get_option('ws_tablebackground');
		if ( $ws_tableBackground == '' ) {
		    	$WPLessPlugin->addVariable('@tableBackground', 'transparent');
	    	}
	    	if ( $ws_tableBackground != '' ) {
		    	$WPLessPlugin->addVariable('@tableBackground', $ws_tableBackground);
	    	}
	$ws_tableBackgroundAccent = of_get_option('ws_tablebackgroundaccent');
		$WPLessPlugin->addVariable('@tableBackgroundAccent', $ws_tableBackgroundAccent);	
	$ws_tableBackgroundHover = of_get_option('ws_tablebackgroundhover');
		$WPLessPlugin->addVariable('@tableBackgroundHover', $ws_tableBackgroundHover);	
	$ws_tableBorder = of_get_option('ws_tableborder');
		$WPLessPlugin->addVariable('@tableBorder', $ws_tableBorder);							
	
// Dropdowns
    	$ws_dropdownBackground = of_get_option('ws_dropdownbackground');
		$WPLessPlugin->addVariable('@dropdownBackground', $ws_dropdownBackground);
	$ws_dropdownDividerTop = of_get_option('ws_dropdowndividertop');
		$WPLessPlugin->addVariable('@dropdownDividerTop', $ws_dropdownDividerTop);		
	$ws_dropdownDividerBottom = of_get_option('ws_dropdowndividerbottom');
		$WPLessPlugin->addVariable('@dropdownDividerBottom', $ws_dropdownDividerBottom);			
	$ws_dropdownLinkColor = of_get_option('ws_dropdownlinkcolor');
		$WPLessPlugin->addVariable('@dropdownLinkColor', $ws_dropdownLinkColor);
	$ws_dropdownLinkColorHover = of_get_option('ws_dropdownlinkcolorhover');
		$WPLessPlugin->addVariable('@dropdownLinkColorHover', $ws_dropdownLinkColorHover);	
	$ws_dropdownLinkBackgroundHover = of_get_option('ws_dropdownlinkbackgroundhover');
		$WPLessPlugin->addVariable('@dropdownLinkBackgroundHover', $ws_dropdownLinkBackgroundHover);
	$ws_dropdownLinkColorActive = of_get_option('ws_dropdownlinkcoloractive');
		$WPLessPlugin->addVariable('@dropdownLinkColorActive', $ws_dropdownLinkColorActive);	
	$ws_dropdownLinkBackgroundActive = of_get_option('ws_dropdownlinkbackgroundactive');
		$WPLessPlugin->addVariable('@dropdownLinkBackgroundActive', $ws_dropdownLinkBackgroundActive);		
	
	/* NOTE: The dropdowns border is auto-generated in the ws.variables.less file. */	
	
// Forms
	$ws_placeholderText = of_get_option('ws_placeholdertext');
		$WPLessPlugin->addVariable('@placeholderText', $ws_placeholderText);
	$ws_inputBackground = of_get_option('ws_inputbackground');
		$WPLessPlugin->addVariable('@inputBackground', $ws_inputBackground);
	$ws_inputBorder = of_get_option('ws_inputborder');
		$WPLessPlugin->addVariable('@inputBorder', $ws_inputBorder);
	$ws_inputDisabledBackground = of_get_option('ws_inputdisabledbackground');
		$WPLessPlugin->addVariable('@inputDisabledBackground', $ws_inputDisabledBackground);
	$ws_inputActionsBackground = of_get_option('ws_inputactionsbackground');
		$WPLessPlugin->addVariable('@inputActionsBackground', $ws_inputActionsBackground);
	
	/* NOTE: Moved the btnPrimary variables to the Buttons section. */
	
// Form States & Alerts
	$ws_warningText = of_get_option('ws_warningtext');
		$WPLessPlugin->addVariable('@warningText', $ws_warningText);
	$ws_warningBackground = of_get_option('ws_warningbackground');
		$WPLessPlugin->addVariable('@warningBackground', $ws_warningBackground);
	$ws_errorText = of_get_option('ws_errortext');
		$WPLessPlugin->addVariable('@errorText', $ws_errorText);
	$ws_errorBackground = of_get_option('ws_errorbackground');
		$WPLessPlugin->addVariable('@errorBackground', $ws_errorBackground);
	$ws_successText = of_get_option('ws_successtext');
		$WPLessPlugin->addVariable('@successText', $ws_successText);
	$ws_successBackground = of_get_option('ws_successbackground');
		$WPLessPlugin->addVariable('@successBackground', $ws_successBackground);
	$ws_infoText = of_get_option('ws_infotext');
		$WPLessPlugin->addVariable('@infoText', $ws_infoText);
	$ws_infoBackground = of_get_option('ws_infobackground');
		$WPLessPlugin->addVariable('@infoBackground', $ws_infoBackground);	
	
// Buttons
	$ws_btnBackground = of_get_option('ws_btnbackground');
		$WPLessPlugin->addVariable('@btnBackground', $ws_btnBackground);	
	$ws_btnPrimaryBackground = of_get_option('ws_btnprimarybackground');
		$WPLessPlugin->addVariable('@btnPrimaryBackground', $ws_btnPrimaryBackground);	
	$ws_btnInfoBackground = of_get_option('ws_btninfobackground');
		$WPLessPlugin->addVariable('@btnInfoBackground', $ws_btnInfoBackground);	
	$ws_btnSuccessBackground = of_get_option('ws_btnsuccessbackground');
		$WPLessPlugin->addVariable('@btnSuccessBackground', $ws_btnSuccessBackground);		
	$ws_btnWarningBackground = of_get_option('ws_btnwarningbackground');
		$WPLessPlugin->addVariable('@btnWarningBackground', $ws_btnWarningBackground);	
	$ws_btnDangerBackground = of_get_option('ws_btndangerbackground');
		$WPLessPlugin->addVariable('@btnDangerBackground', $ws_btnDangerBackground);	
	$ws_btnInverseBackground = of_get_option('ws_btninversebackground');
		$WPLessPlugin->addVariable('@btnInverseBackground', $ws_btnInverseBackground);	

	/* NOTE: The button borders and all button background highlights are auto-generated in the ws.variables.less file.
		They are derived from each button's base color. */
	
// Pagination
	$ws_paginationBackground = of_get_option('ws_paginationbackground');
		$WPLessPlugin->addVariable('@paginationBackground', $ws_paginationBackground);
	$ws_paginationBorder = of_get_option('ws_paginationborder');
		$WPLessPlugin->addVariable('@paginationBorder', $ws_paginationBorder);
	$ws_paginationActiveBackground = of_get_option('ws_paginationactivebackground');
		$WPLessPlugin->addVariable('@paginationActiveBackground', $ws_paginationActiveBackground);	
	
// Tooltips + Popovers
	$ws_tooltipColor = of_get_option('ws_tooltipcolor');
		$WPLessPlugin->addVariable('@tooltipColor', $ws_tooltipColor);
	$ws_tooltipBackground = of_get_option('ws_tooltipbackground');
		$WPLessPlugin->addVariable('@tooltipBackground', $ws_tooltipBackground);	
	$ws_tooltipArrowWidth = of_get_option('ws_tooltiparrowwidth');
		$WPLessPlugin->addVariable('@tooltipArrowWidth', $ws_tooltipArrowWidth);		
	$ws_popoverBackground = of_get_option('ws_popoverbackground');
		$WPLessPlugin->addVariable('@popoverBackground', $ws_popoverBackground);
	$ws_popoverArrowWidth = of_get_option('ws_popoverarrowwidth');
		$WPLessPlugin->addVariable('@popoverArrowWidth', $ws_popoverArrowWidth);

	/* NOTE: Several of the Tooltip + Popover variables are auto-generated in the ws.variables.less file.
		They are derived from the base background color. */
		
// Wells
	$ws_wellBackground = of_get_option('ws_wellbackground');
		$WPLessPlugin->addVariable('@wellBackground', $ws_wellBackground);		
								
// Misc
    	$ws_hrBorder = of_get_option('ws_hrborder');
		$WPLessPlugin->addVariable('@hrBorder', $ws_hrBorder);
	
/* Options for the App LESS file */

// Background Colors + Patterns
	
// BODY
	$ws_bodyOption = of_get_option('ws_bodyoption');
	$ws_bodyBackground = of_get_option('ws_bodybackground');
	$ws_bodyPatternSheer = of_get_option('ws_bodypatternsheer');
		$ws_bodyPatternSheerURL = "url('../img/patterns-sheer/$ws_bodyPatternSheer')";
	$ws_bodyPatternOpaque = of_get_option('ws_bodypatternopaque');
		$ws_bodyPatternOpaqueURL = "url('../img/patterns-opaque/$ws_bodyPatternOpaque')";
	$ws_bodyUpload = of_get_option('ws_bodyupload');
		$ws_bodyUploadURL = "url('$ws_bodyUpload')";
	$ws_bodyRepeat = of_get_option('ws_bodyrepeat');
	$ws_bodyAttach = of_get_option('ws_bodyattach');
	
	$WPLessPlugin->addVariable('@bodyGradient', $ws_bodyBackground);
	
		if ( $ws_bodyOption == 'color' || $ws_bodyOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@bodyBackground', $ws_bodyBackground);
			$WPLessPlugin->addVariable('@bodyPattern', 'none');
			$WPLessPlugin->addVariable('@bodyRepeat', $ws_bodyRepeat);
			$WPLessPlugin->addVariable('@bodyAttach', $ws_bodyAttach);
	    	};  	    	
	    	if ( $ws_bodyOption == 'patternsheer' ) {
	    		$WPLessPlugin->addVariable('@bodyBackground', $ws_bodyBackground);
			$WPLessPlugin->addVariable('@bodyPattern', $ws_bodyPatternSheerURL);
			$WPLessPlugin->addVariable('@bodyRepeat', $ws_bodyRepeat);
			$WPLessPlugin->addVariable('@bodyAttach', $ws_bodyAttach);
		};
	    	if ( $ws_bodyOption == 'patternopaque' ) {
	    		$WPLessPlugin->addVariable('@bodyBackground', $ws_bodyBackground);
			$WPLessPlugin->addVariable('@bodyPattern', $ws_bodyPatternOpaqueURL);
			$WPLessPlugin->addVariable('@bodyRepeat', $ws_bodyRepeat);
			$WPLessPlugin->addVariable('@bodyAttach', $ws_bodyAttach);
		};		
	    	if ( $ws_bodyOption == 'uploadsheer' || $ws_bodyOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@bodyBackground', $ws_bodyBackground);
			$WPLessPlugin->addVariable('@bodyPattern', $ws_bodyUploadURL);
			$WPLessPlugin->addVariable('@bodyRepeat', $ws_bodyRepeat);
			$WPLessPlugin->addVariable('@bodyAttach', $ws_bodyAttach);
		};	
		
// WRAP - Display
	$ws_wrapContainer = of_get_option('ws_wrapcontainer');
		$WPLessPlugin->addVariable('@wrapContainer', $ws_wrapContainer);		
		
	$ws_wrapBoxShadow = of_get_option('ws_wrapboxshadow');	
		if ( $ws_wrapBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@wrapBoxShadow', 'none');		
		}	
		if ( $ws_wrapBoxShadow == 'shadow' ) {
			$ws_wrapBoxShadowInset = of_get_option('ws_wrapboxshadowinset');
			$ws_wrapBoxShadowHorz = of_get_option('ws_wrapboxshadowhorz');
			$ws_wrapBoxShadowVert = of_get_option('ws_wrapboxshadowvert');
			$ws_wrapBoxShadowBlur = of_get_option('ws_wrapboxshadowblur');
			$ws_wrapBoxShadowSpread = of_get_option('ws_wrapboxshadowspread');
			$ws_wrapBoxShadowOpacity = of_get_option('ws_wrapboxshadowopacity');
			if ( $ws_wrapBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@wrapBoxShadow', 'inset '.$ws_wrapBoxShadowHorz.' '.$ws_wrapBoxShadowVert.' '.$ws_wrapBoxShadowBlur.' '.$ws_wrapBoxShadowSpread.' rgba(0,0,0,'.$ws_wrapBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@wrapBoxShadow', ''.$ws_wrapBoxShadowHorz.' '.$ws_wrapBoxShadowVert.' '.$ws_wrapBoxShadowBlur.' '.$ws_wrapBoxShadowSpread.' rgba(0,0,0,'.$ws_wrapBoxShadowOpacity.')');
			 }
		}
	
	$ws_wrapBorderRadius = of_get_option('ws_wrapborderradius');	
		if ( $ws_wrapBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@wrapBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@wrapBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@wrapBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@wrapBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_wrapBorderRadius == 'radius' ) {
			$ws_wrapBorderRadiusTopLeft = of_get_option('ws_wrapborderradiustopleft');
				$WPLessPlugin->addVariable('@wrapBorderRadiusTopLeft', $ws_wrapBorderRadiusTopLeft);
			$ws_wrapBorderRadiusTopRight = of_get_option('ws_wrapborderradiustopright');
				$WPLessPlugin->addVariable('@wrapBorderRadiusTopRight', $ws_wrapBorderRadiusTopRight);	
			$ws_wrapBorderRadiusBtmLeft = of_get_option('ws_wrapborderradiusbtmleft');
				$WPLessPlugin->addVariable('@wrapBorderRadiusBtmLeft', $ws_wrapBorderRadiusBtmLeft);
			$ws_wrapBorderRadiusBtmRight = of_get_option('ws_wrapborderradiusbtmright');
				$WPLessPlugin->addVariable('@wrapBorderRadiusBtmRight', $ws_wrapBorderRadiusBtmRight);	
		}

	$ws_wrapPadding = of_get_option('ws_wrappadding');	
		if ( $ws_wrapPadding == 'default' ) {
			$WPLessPlugin->addVariable('@wrapPaddingTop', '0px');
			$WPLessPlugin->addVariable('@wrapPaddingBtm', '0px');
			$WPLessPlugin->addVariable('@wrapPaddingLeft', '30px');
			$WPLessPlugin->addVariable('@wrapPaddingRight', '30px');
		}
		if ( $ws_wrapPadding == 'padding' ) {
			$ws_wrapPaddingTop = of_get_option('ws_wrappaddingtop');
				$WPLessPlugin->addVariable('@wrapPaddingTop', $ws_wrapPaddingTop);	
			$ws_wrapPaddingBtm = of_get_option('ws_wrappaddingbtm');
				$WPLessPlugin->addVariable('@wrapPaddingBtm', $ws_wrapPaddingBtm);
			$ws_wrapPaddingLeft = of_get_option('ws_wrappaddingleft');
				$WPLessPlugin->addVariable('@wrapPaddingLeft', $ws_wrapPaddingLeft);	
			$ws_wrapPaddingRight = of_get_option('ws_wrappaddingright');
				$WPLessPlugin->addVariable('@wrapPaddingRight', $ws_wrapPaddingRight);	
		}
		
	$ws_wrapMargin = of_get_option('ws_wrapmargin');	
		if ( $ws_wrapMargin == 'default' ) {
			$WPLessPlugin->addVariable('@wrapMarginTop', '0px');
			$WPLessPlugin->addVariable('@wrapMarginBtm', '0px');
			$WPLessPlugin->addVariable('@wrapMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@wrapMarginRight', 'auto');
		}
		if ( $ws_wrapMargin == 'margin' ) {
			$ws_wrapMarginTop = of_get_option('ws_wrapmargintop');
				$WPLessPlugin->addVariable('@wrapMarginTop', $ws_wrapMarginTop);	
			$ws_wrapMarginBtm = of_get_option('ws_wrapmarginbtm');
				$WPLessPlugin->addVariable('@wrapMarginBtm', $ws_wrapMarginBtm);
			$ws_wrapMarginLeft = of_get_option('ws_wrapmarginleft');
				$WPLessPlugin->addVariable('@wrapMarginLeft', $ws_wrapMarginLeft);	
			$ws_wrapMarginRight = of_get_option('ws_wrapmarginright');
				$WPLessPlugin->addVariable('@wrapMarginRight', $ws_wrapMarginRight);	
		}
		
// WRAP - Background		
	$ws_wrapOption = of_get_option('ws_wrapoption');
	$ws_wrapBackground = of_get_option('ws_wrapbackground');
	$ws_wrapPatternSheer = of_get_option('ws_wrappatternsheer');
		$ws_wrapPatternSheerURL = "url('../img/patterns-sheer/$ws_wrapPatternSheer')";
	$ws_wrapPatternOpaque = of_get_option('ws_wrappatternopaque');
		$ws_wrapPatternOpaqueURL = "url('../img/patterns-opaque/$ws_wrapPatternOpaque')";
	$ws_wrapUpload = of_get_option('ws_wrapupload');
		$ws_wrapUploadURL = "url('$ws_wrapUpload')";
	$ws_wrapRepeat = of_get_option('ws_wraprepeat');
	$ws_wrapAttach = of_get_option('ws_wrapattach');
	
	$WPLessPlugin->addVariable('@wrapGradient', $ws_wrapBackground);
	
		if ( $ws_wrapOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@wrapBackground', 'transparent');
			$WPLessPlugin->addVariable('@wrapPattern', 'none');
			$WPLessPlugin->addVariable('@wrapRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@wrapAttach', $ws_wrapAttach);
		};
		if ( $ws_wrapOption == 'color' || $ws_wrapOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@wrapBackground', $ws_wrapBackground);
			$WPLessPlugin->addVariable('@wrapPattern', 'none');
			$WPLessPlugin->addVariable('@wrapRepeat', $ws_wrapRepeat);
			$WPLessPlugin->addVariable('@wrapAttach', $ws_wrapAttach);
		}; 
		if ( $ws_wrapOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@wrapBackground', $ws_wrapBackground);
			$WPLessPlugin->addVariable('@wrapPattern', $ws_wrapPatternSheerURL);
			$WPLessPlugin->addVariable('@wrapRepeat', $ws_wrapRepeat);
			$WPLessPlugin->addVariable('@wrapAttach', $ws_wrapAttach);
		};
		if ( $ws_wrapOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@wrapBackground', $ws_wrapBackground);
			$WPLessPlugin->addVariable('@wrapPattern', $ws_wrapPatternOpaqueURL);
			$WPLessPlugin->addVariable('@wrapRepeat', $ws_wrapRepeat);
			$WPLessPlugin->addVariable('@wrapAttach', $ws_wrapAttach);
		};
	    	if ( $ws_wrapOption == 'uploadsheer' || $ws_wrapOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@wrapBackground', $ws_wrapBackground);
			$WPLessPlugin->addVariable('@wrapPattern', $ws_wrapUploadURL);
			$WPLessPlugin->addVariable('@wrapRepeat', $ws_wrapRepeat);
			$WPLessPlugin->addVariable('@wrapAttach', $ws_wrapAttach);
		};		
		
// HEADER - Display
	$ws_headerContainer = of_get_option('ws_headercontainer');
		$WPLessPlugin->addVariable('@headerContainer', $ws_headerContainer);	
			
	$ws_headerBoxShadow = of_get_option('ws_headerboxshadow');	
		if ( $ws_headerBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@headerBoxShadow', 'none');		
		}	
		if ( $ws_headerBoxShadow == 'shadow' ) {
			$ws_headerBoxShadowInset = of_get_option('ws_headerboxshadowinset');
			$ws_headerBoxShadowHorz = of_get_option('ws_headerboxshadowhorz');
			$ws_headerBoxShadowVert = of_get_option('ws_headerboxshadowvert');
			$ws_headerBoxShadowBlur = of_get_option('ws_headerboxshadowblur');
			$ws_headerBoxShadowSpread = of_get_option('ws_headerboxshadowspread');
			$ws_headerBoxShadowOpacity = of_get_option('ws_headerboxshadowopacity');
			if ( $ws_headerBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@headerBoxShadow', 'inset '.$ws_headerBoxShadowHorz.' '.$ws_headerBoxShadowVert.' '.$ws_headerBoxShadowBlur.' '.$ws_headerBoxShadowSpread.' rgba(0,0,0,'.$ws_headerBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@headerBoxShadow', ''.$ws_headerBoxShadowHorz.' '.$ws_headerBoxShadowVert.' '.$ws_headerBoxShadowBlur.' '.$ws_headerBoxShadowSpread.' rgba(0,0,0,'.$ws_headerBoxShadowOpacity.')');
			 }
		}
	
	$ws_headerBorderRadius = of_get_option('ws_headerborderradius');	
		if ( $ws_headerBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@headerBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@headerBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@headerBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@headerBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_headerBorderRadius == 'radius' ) {
			$ws_headerBorderRadiusTopLeft = of_get_option('ws_headerborderradiustopleft');
				$WPLessPlugin->addVariable('@headerBorderRadiusTopLeft', $ws_headerBorderRadiusTopLeft);
			$ws_headerBorderRadiusTopRight = of_get_option('ws_headerborderradiustopright');
				$WPLessPlugin->addVariable('@headerBorderRadiusTopRight', $ws_headerBorderRadiusTopRight);	
			$ws_headerBorderRadiusBtmLeft = of_get_option('ws_headerborderradiusbtmleft');
				$WPLessPlugin->addVariable('@headerBorderRadiusBtmLeft', $ws_headerBorderRadiusBtmLeft);
			$ws_headerBorderRadiusBtmRight = of_get_option('ws_headerborderradiusbtmright');
				$WPLessPlugin->addVariable('@headerBorderRadiusBtmRight', $ws_headerBorderRadiusBtmRight);	
		}

	$ws_headerPadding = of_get_option('ws_headerpadding');	
		if ( $ws_headerPadding == 'default' ) {
			$WPLessPlugin->addVariable('@headerPaddingTop', '0px');
			$WPLessPlugin->addVariable('@headerPaddingBtm', '0px');
			$WPLessPlugin->addVariable('@headerPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@headerPaddingRight', '0px');
		}
		if ( $ws_headerPadding == 'padding' ) {
			$ws_headerPaddingTop = of_get_option('ws_headerpaddingtop');
				$WPLessPlugin->addVariable('@headerPaddingTop', $ws_headerPaddingTop);	
			$ws_headerPaddingBtm = of_get_option('ws_headerpaddingbtm');
				$WPLessPlugin->addVariable('@headerPaddingBtm', $ws_headerPaddingBtm);
			$ws_headerPaddingLeft = of_get_option('ws_headerpaddingleft');
				$WPLessPlugin->addVariable('@headerPaddingLeft', $ws_headerPaddingLeft);	
			$ws_headerPaddingRight = of_get_option('ws_headerpaddingright');
				$WPLessPlugin->addVariable('@headerPaddingRight', $ws_headerPaddingRight);	
		}
		
	$ws_headerMargin = of_get_option('ws_headermargin');	
		if ( $ws_headerMargin == 'default' ) {
			$WPLessPlugin->addVariable('@headerMarginTop', '0px');
			$WPLessPlugin->addVariable('@headerMarginBtm', '0px');
			$WPLessPlugin->addVariable('@headerMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@headerMarginRight', 'auto');
		}
		if ( $ws_headerMargin == 'margin' ) {
			$ws_headerMarginTop = of_get_option('ws_headermargintop');
				$WPLessPlugin->addVariable('@headerMarginTop', $ws_headerMarginTop);	
			$ws_headerMarginBtm = of_get_option('ws_headermarginbtm');
				$WPLessPlugin->addVariable('@headerMarginBtm', $ws_headerMarginBtm);
			$ws_headerMarginLeft = of_get_option('ws_headermarginleft');
				$WPLessPlugin->addVariable('@headerMarginLeft', $ws_headerMarginLeft);	
			$ws_headerMarginRight = of_get_option('ws_headermarginright');
				$WPLessPlugin->addVariable('@headerMarginRight', $ws_headerMarginRight);	
		}	

// HEADER - Background		
	$ws_headerOption = of_get_option('ws_headeroption');
	$ws_headerBackground = of_get_option('ws_headerbackground');
	$ws_headerPatternSheer = of_get_option('ws_headerpatternsheer');
		$ws_headerPatternSheerURL = "url('../img/patterns-sheer/$ws_headerPatternSheer')";
	$ws_headerPatternOpaque = of_get_option('ws_headerpatternopaque');
		$ws_headerPatternOpaqueURL = "url('../img/patterns-opaque/$ws_headerPatternOpaque')";	
	$ws_headerUpload = of_get_option('ws_headerupload');
		$ws_headerUploadURL = "url('$ws_headerUpload')";
	$ws_headerRepeat = of_get_option('ws_headerrepeat');
	$ws_headerAttach = of_get_option('ws_headerattach');
	
	$WPLessPlugin->addVariable('@headerGradient', $ws_headerBackground);
	
		if ( $ws_headerOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@headerBackground', 'transparent');
			$WPLessPlugin->addVariable('@headerPattern', 'none');
			$WPLessPlugin->addVariable('@headerRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@headerAttach', $ws_headerAttach);
		};
		if ( $ws_headerOption == 'color' || $ws_headerOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@headerBackground', $ws_headerBackground);
			$WPLessPlugin->addVariable('@headerPattern', 'none');
			$WPLessPlugin->addVariable('@headerRepeat', $ws_headerRepeat);
			$WPLessPlugin->addVariable('@headerAttach', $ws_headerAttach);
		};
		if ( $ws_headerOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@headerBackground', $ws_headerBackground);
			$WPLessPlugin->addVariable('@headerPattern', $ws_headerPatternSheerURL);
			$WPLessPlugin->addVariable('@headerRepeat', $ws_headerRepeat);
			$WPLessPlugin->addVariable('@headerAttach', $ws_headerAttach);
		};
		if ( $ws_headerOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@headerBackground', $ws_headerBackground);
			$WPLessPlugin->addVariable('@headerPattern', $ws_headerPatternOpaqueURL);
			$WPLessPlugin->addVariable('@headerRepeat', $ws_headerRepeat);
			$WPLessPlugin->addVariable('@headerAttach', $ws_headerAttach);
		};		
	    	if ( $ws_headerOption == 'uploadsheer' || $ws_headerOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@headerBackground', $ws_headerBackground);
			$WPLessPlugin->addVariable('@headerPattern', $ws_headerUploadURL);
			$WPLessPlugin->addVariable('@headerRepeat', $ws_headerRepeat);
			$WPLessPlugin->addVariable('@headerAttach', $ws_headerAttach);
		};		
		
// MASTHEAD - Display
	$ws_mastHeight = of_get_option('ws_mastheight');
		$WPLessPlugin->addVariable('@mastHeight', $ws_mastHeight);
	
	$ws_mastLeaderboard = of_get_option('ws_mastleaderboard');
		$ws_mastLeaderboardURL = "url('$ws_mastLeaderboard')";		
		
	$ws_mastBoxShadow = of_get_option('ws_mastboxshadow');	
		if ( $ws_mastBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@mastBoxShadow', 'none');		
		}	
		if ( $ws_mastBoxShadow == 'shadow' ) {
			$ws_mastBoxShadowInset = of_get_option('ws_mastboxshadowinset');
			$ws_mastBoxShadowHorz = of_get_option('ws_mastboxshadowhorz');
			$ws_mastBoxShadowVert = of_get_option('ws_mastboxshadowvert');
			$ws_mastBoxShadowBlur = of_get_option('ws_mastboxshadowblur');
			$ws_mastBoxShadowSpread = of_get_option('ws_mastboxshadowspread');
			$ws_mastBoxShadowOpacity = of_get_option('ws_mastboxshadowopacity');
			if ( $ws_mastBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@mastBoxShadow', 'inset '.$ws_mastBoxShadowHorz.' '.$ws_mastBoxShadowVert.' '.$ws_mastBoxShadowBlur.' '.$ws_mastBoxShadowSpread.' rgba(0,0,0,'.$ws_mastBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@mastBoxShadow', ''.$ws_mastBoxShadowHorz.' '.$ws_mastBoxShadowVert.' '.$ws_mastBoxShadowBlur.' '.$ws_mastBoxShadowSpread.' rgba(0,0,0,'.$ws_mastBoxShadowOpacity.')');
			 }
		}
		
	$ws_mastBorderRadius = of_get_option('ws_mastborderradius');	
		if ( $ws_mastBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@mastBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@mastBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@mastBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@mastBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_mastBorderRadius == 'radius' ) {
			$ws_mastBorderRadiusTopLeft = of_get_option('ws_mastborderradiustopleft');
				$WPLessPlugin->addVariable('@mastBorderRadiusTopLeft', $ws_mastBorderRadiusTopLeft);
			$ws_mastBorderRadiusTopRight = of_get_option('ws_mastborderradiustopright');
				$WPLessPlugin->addVariable('@mastBorderRadiusTopRight', $ws_mastBorderRadiusTopRight);	
			$ws_mastBorderRadiusBtmLeft = of_get_option('ws_mastborderradiusbtmleft');
				$WPLessPlugin->addVariable('@mastBorderRadiusBtmLeft', $ws_mastBorderRadiusBtmLeft);
			$ws_mastBorderRadiusBtmRight = of_get_option('ws_mastborderradiusbtmright');
				$WPLessPlugin->addVariable('@mastBorderRadiusBtmRight', $ws_mastBorderRadiusBtmRight);	
		}

	$ws_mastPadding = of_get_option('ws_mastpadding');	
		if ( $ws_mastPadding == 'default' ) {
			$WPLessPlugin->addVariable('@mastPaddingTop', '0px');
			$WPLessPlugin->addVariable('@mastPaddingBtm', '0px');
			$WPLessPlugin->addVariable('@mastPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@mastPaddingRight', '0px');			
		}
		if ( $ws_mastPadding == 'padding' ) {
			$ws_mastPaddingTop = of_get_option('ws_mastpaddingtop');
				$WPLessPlugin->addVariable('@mastPaddingTop', $ws_mastPaddingTop);	
			$ws_mastPaddingBtm = of_get_option('ws_mastpaddingbtm');
				$WPLessPlugin->addVariable('@mastPaddingBtm', $ws_mastPaddingBtm);
			$ws_mastPaddingLeft = of_get_option('ws_mastpaddingleft');
				$WPLessPlugin->addVariable('@mastPaddingLeft', $ws_mastPaddingLeft);	
			$ws_mastPaddingRight = of_get_option('ws_mastpaddingright');
				$WPLessPlugin->addVariable('@mastPaddingRight', $ws_mastPaddingRight);					
		}
		
	$ws_mastMargin = of_get_option('ws_mastmargin');	
		if ( $ws_mastMargin == 'default' ) {
			$WPLessPlugin->addVariable('@mastMarginTop', '0px');
			$WPLessPlugin->addVariable('@mastMarginBtm', '0px');
			$WPLessPlugin->addVariable('@mastMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@mastMarginRight', 'auto');
		}
		if ( $ws_mastMargin == 'margin' ) {
			$ws_mastMarginTop = of_get_option('ws_mastmargintop');
				$WPLessPlugin->addVariable('@mastMarginTop', $ws_mastMarginTop);	
			$ws_mastMarginBtm = of_get_option('ws_mastmarginbtm');
				$WPLessPlugin->addVariable('@mastMarginBtm', $ws_mastMarginBtm);
			$ws_mastMarginLeft = of_get_option('ws_mastmarginleft');
				$WPLessPlugin->addVariable('@mastMarginLeft', $ws_mastMarginLeft);	
			$ws_mastMarginRight = of_get_option('ws_mastmarginright');
				$WPLessPlugin->addVariable('@mastMarginRight', $ws_mastMarginRight);				
		}

// MASTHEAD - Background		
	$ws_mastOption = of_get_option('ws_mastoption');
	$ws_mastBackground = of_get_option('ws_mastbackground');
	$ws_mastPatternSheer = of_get_option('ws_mastpatternsheer');
		$ws_mastPatternSheerURL = "url('../img/patterns-sheer/$ws_mastPatternSheer')";
	$ws_mastPatternOpaque = of_get_option('ws_mastpatternopaque');
		$ws_mastPatternOpaqueURL = "url('../img/patterns-opaque/$ws_mastPatternOpaque')";
	$ws_mastUpload = of_get_option('ws_mastupload');
		$ws_mastUploadURL = "url('$ws_mastUpload')";
	$ws_mastRepeat = of_get_option('ws_mastrepeat');
	$ws_mastAttach = of_get_option('ws_mastattach');
	
	$WPLessPlugin->addVariable('@mastGradient', $ws_mastBackground);
	
		if ( $ws_mastOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@mastBackground', 'transparent');
			$WPLessPlugin->addVariable('@mastPattern', 'none');
			$WPLessPlugin->addVariable('@mastRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@mastAttach', $ws_mastAttach);
		};
		if ( $ws_mastOption == 'color' || $ws_mastOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@mastBackground', $ws_mastBackground);
			$WPLessPlugin->addVariable('@mastPattern', 'none');
			$WPLessPlugin->addVariable('@mastRepeat', $ws_mastRepeat);
			$WPLessPlugin->addVariable('@mastAttach', $ws_mastAttach);
		};
		if ( $ws_mastOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@mastBackground', $ws_mastBackground);
			$WPLessPlugin->addVariable('@mastPattern', $ws_mastPatternSheerURL);
			$WPLessPlugin->addVariable('@mastRepeat', $ws_mastRepeat);
			$WPLessPlugin->addVariable('@mastAttach', $ws_mastAttach);
		};
		if ( $ws_mastOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@mastBackground', $ws_mastBackground);
			$WPLessPlugin->addVariable('@mastPattern', $ws_mastPatternOpaqueURL);
			$WPLessPlugin->addVariable('@mastRepeat', $ws_mastRepeat);
			$WPLessPlugin->addVariable('@mastAttach', $ws_mastAttach);
		};
	    	if ( $ws_mastOption == 'uploadsheer' || $ws_mastOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@mastBackground', $ws_mastBackground);
			$WPLessPlugin->addVariable('@mastPattern', $ws_mastUploadURL);
			$WPLessPlugin->addVariable('@mastRepeat', $ws_mastRepeat);
			$WPLessPlugin->addVariable('@mastAttach', $ws_mastAttach);
		};	
		
// NAVBAR - Display
	$ws_navbarCollapseWidth = of_get_option('ws_navbarcollapsewidth');
		$WPLessPlugin->addVariable('@navbarCollapseWidth', $ws_navbarCollapseWidth);
    	$ws_navbarHeight = of_get_option('ws_navbarheight');
		$WPLessPlugin->addVariable('@navbarHeight', $ws_navbarHeight);			
		
	$ws_navbarPosition = of_get_option('ws_navbarposition');
	$ws_navbarFixed = of_get_option('ws_navbarfixed');
		if ( $ws_navbarPosition == 'navbar-pos-top' && $ws_navbarFixed == 'navbar-fixed' ) {
			//$WPLessPlugin->addVariable('@mastMarginTop', $ws_navbarHeight); 
			$WPLessPlugin->addVariable('@mastMarginTop', '0px'); 
		}
		if ( $ws_navbarPosition == 'navbar-pos-btm' || $ws_navbarFixed == 'navbar-static' ) {
			$WPLessPlugin->addVariable('@mastMarginTop', '0px'); 
		}
		
	$ws_navbarMenuSideMargins = of_get_option('ws_navbarmenusidemargins');
		$WPLessPlugin->addVariable('@navbarMenuSideMargins', $ws_navbarMenuSideMargins);		
		
	$ws_navbarBoxShadow = of_get_option('ws_navbarboxshadow');	
		if ( $ws_navbarBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@navbarBoxShadow', 'none');		
		}	
		if ( $ws_navbarBoxShadow == 'shadow' ) {
			$ws_navbarBoxShadowInset = of_get_option('ws_navbarboxshadowinset');
			$ws_navbarBoxShadowHorz = of_get_option('ws_navbarboxshadowhorz');
			$ws_navbarBoxShadowVert = of_get_option('ws_navbarboxshadowvert');
			$ws_navbarBoxShadowBlur = of_get_option('ws_navbarboxshadowblur');
			$ws_navbarBoxShadowSpread = of_get_option('ws_navbarboxshadowspread');
			$ws_navbarBoxShadowOpacity = of_get_option('ws_navbarboxshadowopacity');
			if ( $ws_navbarBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@navbarBoxShadow', 'inset '.$ws_navbarBoxShadowHorz.' '.$ws_navbarBoxShadowVert.' '.$ws_navbarBoxShadowBlur.' '.$ws_navbarBoxShadowSpread.' rgba(0,0,0,'.$ws_navbarBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@navbarBoxShadow', ''.$ws_navbarBoxShadowHorz.' '.$ws_navbarBoxShadowVert.' '.$ws_navbarBoxShadowBlur.' '.$ws_navbarBoxShadowSpread.' rgba(0,0,0,'.$ws_navbarBoxShadowOpacity.')');
			 }
			 
		}	 
		 
	$ws_navbarBorderRadius = of_get_option('ws_navbarborderradius');	
		if ( $ws_navbarBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@navbarBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@navbarBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@navbarBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@navbarBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_navbarBorderRadius == 'radius' ) {
			$ws_navbarBorderRadiusTopLeft = of_get_option('ws_navbarborderradiustopleft');
				$WPLessPlugin->addVariable('@navbarBorderRadiusTopLeft', $ws_navbarBorderRadiusTopLeft);
			$ws_navbarBorderRadiusTopRight = of_get_option('ws_navbarborderradiustopright');
				$WPLessPlugin->addVariable('@navbarBorderRadiusTopRight', $ws_navbarBorderRadiusTopRight);	
			$ws_navbarBorderRadiusBtmLeft = of_get_option('ws_navbarborderradiusbtmleft');
				$WPLessPlugin->addVariable('@navbarBorderRadiusBtmLeft', $ws_navbarBorderRadiusBtmLeft);
			$ws_navbarBorderRadiusBtmRight = of_get_option('ws_navbarborderradiusbtmright');
				$WPLessPlugin->addVariable('@navbarBorderRadiusBtmRight', $ws_navbarBorderRadiusBtmRight);	
		}

	$ws_navbarPadding = of_get_option('ws_navbarpadding');	
		if ( $ws_navbarPadding == 'default' ) {
			$WPLessPlugin->addVariable('@navbarPaddingTop', '0px');
			$WPLessPlugin->addVariable('@navbarPaddingBtm', '0px');
			$WPLessPlugin->addVariable('@navbarPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@navbarPaddingRight', '0px');
		}
		if ( $ws_navbarPadding == 'padding' ) {
			$ws_navbarPaddingTop = of_get_option('ws_navbarpaddingtop');
				$WPLessPlugin->addVariable('@navbarPaddingTop', $ws_navbarPaddingTop);	
			$ws_navbarPaddingBtm = of_get_option('ws_navbarpaddingbtm');
				$WPLessPlugin->addVariable('@navbarPaddingBtm', $ws_navbarPaddingBtm);
			$ws_navbarPaddingLeft = of_get_option('ws_navbarpaddingleft');
				$WPLessPlugin->addVariable('@navbarPaddingLeft', $ws_navbarPaddingLeft);	
			$ws_navbarPaddingRight = of_get_option('ws_navbarpaddingright');
				$WPLessPlugin->addVariable('@navbarPaddingRight', $ws_navbarPaddingRight);	
		}
		
	$ws_navbarMargin = of_get_option('ws_navbarmargin');	
		if ( $ws_navbarMargin == 'default' ) {
			$WPLessPlugin->addVariable('@navbarMarginTop', '0px');
			$WPLessPlugin->addVariable('@navbarMarginBtm', '0px');
			$WPLessPlugin->addVariable('@navbarMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@navbarMarginRight', 'auto');
		}
		if ( $ws_navbarMargin == 'margin' ) {
			$ws_navbarMarginTop = of_get_option('ws_navbarmargintop');
				$WPLessPlugin->addVariable('@navbarMarginTop', $ws_navbarMarginTop);	
			$ws_navbarMarginBtm = of_get_option('ws_navbarmarginbtm');
				$WPLessPlugin->addVariable('@navbarMarginBtm', $ws_navbarMarginBtm);
			$ws_navbarMarginLeft = of_get_option('ws_navbarmarginleft');
				$WPLessPlugin->addVariable('@navbarMarginLeft', $ws_navbarMarginLeft);	
			$ws_navbarMarginRight = of_get_option('ws_navbarmarginright');
				$WPLessPlugin->addVariable('@navbarMarginRight', $ws_navbarMarginRight);	
		}				
		
// NAVBAR - Dropdown + Carets
	$ws_navbarCaret = of_get_option('ws_navbarcaret');
		$WPLessPlugin->addVariable('@navbarCaret', $ws_navbarCaret);
	$ws_navbarDropdownCaret = of_get_option('ws_navbardropdowncaret');
		$WPLessPlugin->addVariable('@navbarDropdownCaret', $ws_navbarDropdownCaret);
	$ws_navbarDropdownBorderRadius = of_get_option('ws_navbardropdownborderradius');
		$WPLessPlugin->addVariable('@navbarDropdownBorderRadius', $ws_navbarDropdownBorderRadius);	
	$ws_navbarDropdownMarginTop = of_get_option('ws_navbardropdownmargintop');
		$WPLessPlugin->addVariable('@navbarDropdownMarginTop', $ws_navbarDropdownMarginTop);						
	
// NAVBAR - Scheme - Default (Light)
    	$ws_navbarDefaultBackground = of_get_option('ws_navbardefaultbackground');
		$WPLessPlugin->addVariable('@navbarBackground', $ws_navbarDefaultBackground);	
    	$ws_navbarDefaultBorder = of_get_option('ws_navbardefaultborder');
		$WPLessPlugin->addVariable('@navbarBorder', $ws_navbarDefaultBorder);			
					
    	$ws_navbarDefaultText = of_get_option('ws_navbardefaulttext');
		$WPLessPlugin->addVariable('@navbarText', $ws_navbarDefaultText);	
    	$ws_navbarDefaultLinkColor = of_get_option('ws_navbardefaultlinkcolor'); 
		$WPLessPlugin->addVariable('@navbarLinkColor', $ws_navbarDefaultLinkColor);
    	$ws_navbarDefaultLinkColorHover = of_get_option('ws_navbardefaultlinkcolorhover');
		$WPLessPlugin->addVariable('@navbarLinkColorHover', $ws_navbarDefaultLinkColorHover);	
    	$ws_navbarDefaultLinkColorActive = of_get_option('ws_navbardefaultlinkcoloractive');
		$WPLessPlugin->addVariable('@navbarLinkColorActive', $ws_navbarDefaultLinkColorActive);
		
    	$ws_navbarDefaultLinkBackgroundHover = of_get_option('ws_navbardefaultlinkbackgroundhover');
	    	if ( $ws_navbarDefaultLinkBackgroundHover == '' ) {
		    	$WPLessPlugin->addVariable('@navbarLinkBackgroundHover', 'transparent');
	    	}
	    	if ( $ws_navbarDefaultLinkBackgroundHover != '' ) {
		    	$WPLessPlugin->addVariable('@navbarLinkBackgroundHover', $ws_navbarDefaultLinkBackgroundHover);
	    	}
    	$ws_navbarDefaultLinkBackgroundActive = of_get_option('ws_navbardefaultlinkbackgroundactive');
		$WPLessPlugin->addVariable('@navbarLinkBackgroundActive', $ws_navbarDefaultLinkBackgroundActive); 	
		
    	$ws_navbarDefaultBrandColor = of_get_option('ws_navbardefaultbrandcolor');
		$WPLessPlugin->addVariable('@navbarBrandColor', $ws_navbarDefaultBrandColor);	
		
	/* NOTE: The @navbarBackgroundHighlight + the navbar border are auto-generated in the ws.variables.less file.
		They are derived from the base background color. */		
		
// NAVBAR - Scheme - Inverse (Dark)
	
    	$ws_navbarInverseBackground = of_get_option('ws_navbarinversebackground');
		$WPLessPlugin->addVariable('@navbarInverseBackground', $ws_navbarInverseBackground);
    	$ws_navbarInverseBorder = of_get_option('ws_navbarinverseborder');
		$WPLessPlugin->addVariable('@navbarInverseBorder', $ws_navbarInverseBorder);
						
    	$ws_navbarInverseText = of_get_option('ws_navbarinversetext');
		$WPLessPlugin->addVariable('@navbarInverseText', $ws_navbarInverseText);	
    	$ws_navbarInverseLinkColor = of_get_option('ws_navbarinverselinkcolor'); 
		$WPLessPlugin->addVariable('@navbarInverseLinkColor', $ws_navbarInverseLinkColor);
    	$ws_navbarInverseLinkColorHover = of_get_option('ws_navbarinverselinkcolorhover');
		$WPLessPlugin->addVariable('@navbarInverseLinkColorHover', $ws_navbarInverseLinkColorHover);	
    	$ws_navbarInverseLinkColorActive = of_get_option('ws_navbarinverselinkcoloractive');
		$WPLessPlugin->addVariable('@navbarInverseLinkColorActive', $ws_navbarInverseLinkColorActive);
	
	$ws_navbarInverseLinkBackgroundHover = of_get_option('ws_navbarinverselinkbackgroundhover');	
	    	if ( $ws_navbarInverseLinkBackgroundHover == '' ) {
		    	$WPLessPlugin->addVariable('@navbarInverseLinkBackgroundHover', 'transparent');
	    	}
	    	if ( $ws_navbarInverseLinkBackgroundHover != '' ) {
		    	$WPLessPlugin->addVariable('@navbarInverseLinkBackgroundHover', $ws_navbarInverseLinkBackgroundHover);
	    	}
	$ws_navbarInverseLinkBackgroundActive = of_get_option('ws_navbarinverselinkbackgroundactive');
		$WPLessPlugin->addVariable('@navbarInverseLinkBackgroundActive', $ws_navbarInverseLinkBackgroundActive);
	
    	$ws_navbarInverseBrandColor = of_get_option('ws_navbarinversebrandcolor');
		$WPLessPlugin->addVariable('@navbarInverseBrandColor', $ws_navbarInverseBrandColor);	
		
	/* NOTE: The @navbarInverseBackgroundHighlight + the navbar border are auto-generated in the ws.variables.less file.
		They are derived from the base background color. */	
		
// NAVBAR - Background
	$ws_navbarOption = of_get_option('ws_navbaroption');
	$ws_navbarScheme = of_get_option('ws_navbarscheme');
	if ( $ws_navbarScheme == 'navbar-light' ) {
		$ws_navbarBackgroundColor = of_get_option('ws_navbardefaultbackground');
	}
	if ( $ws_navbarScheme == 'navbar-dark' ) {
		$ws_navbarBackgroundColor = of_get_option('ws_navbarinversebackground');
	}
	$ws_navbarPatternSheer = of_get_option('ws_navbarpatternsheer');
		$ws_navbarPatternSheerURL = "url('../img/patterns-sheer/$ws_navbarPatternSheer')";
	$ws_navbarPatternOpaque = of_get_option('ws_navbarpatternopaque');
		$ws_navbarPatternOpaqueURL = "url('../img/patterns-opaque/$ws_navbarPatternOpaque')";
	$ws_navbarUpload = of_get_option('ws_navbarupload');
		$ws_navbarUploadURL = "url('$ws_navbarUpload')";
	$ws_navbarRepeat = of_get_option('ws_navbarrepeat');
	$ws_navbarAttach = of_get_option('ws_navbarattach');
	
	$WPLessPlugin->addVariable('@navbarGradient', $ws_navbarBackgroundColor);
	
		if ( $ws_navbarOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@navbarBackgroundColor', 'transparent');
			$WPLessPlugin->addVariable('@navbarPattern', 'none');
			$WPLessPlugin->addVariable('@navbarRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@navbarAttach', $ws_navbarAttach);
		};
		if ( $ws_navbarOption == 'color' || $ws_navbarOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@navbarBackgroundColor', $ws_navbarBackgroundColor);
			$WPLessPlugin->addVariable('@navbarPattern', 'none');
			$WPLessPlugin->addVariable('@navbarRepeat', $ws_navbarRepeat);
			$WPLessPlugin->addVariable('@navbarAttach', $ws_navbarAttach);
		}; 
		if ( $ws_navbarOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@navbarBackgroundColor', $ws_navbarBackgroundColor);
			$WPLessPlugin->addVariable('@navbarPattern', $ws_navbarPatternSheerURL);
			$WPLessPlugin->addVariable('@navbarRepeat', $ws_navbarRepeat);
			$WPLessPlugin->addVariable('@navbarAttach', $ws_navbarAttach);
		};
		if ( $ws_navbarOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@navbarBackgroundColor', $ws_navbarBackgroundColor);
			$WPLessPlugin->addVariable('@navbarPattern', $ws_navbarPatternOpaqueURL);
			$WPLessPlugin->addVariable('@navbarRepeat', $ws_navbarRepeat);
			$WPLessPlugin->addVariable('@navbarAttach', $ws_navbarAttach);
		};
	    	if ( $ws_navbarOption == 'uploadsheer' || $ws_navbarOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@navbarBackgroundColor', $ws_navbarBackgroundColor);
			$WPLessPlugin->addVariable('@navbarPattern', $ws_navbarUploadURL);
			$WPLessPlugin->addVariable('@navbarRepeat', $ws_navbarRepeat);
			$WPLessPlugin->addVariable('@navbarAttach', $ws_navbarAttach);
		};
		
// FEATURE - Display
	$ws_featureContainer = of_get_option('ws_featurecontainer');
		$WPLessPlugin->addVariable('@featureContainer', $ws_featureContainer);	
			
	$ws_featureBoxShadow = of_get_option('ws_featureboxshadow');	
		if ( $ws_featureBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@featureBoxShadow', 'none');		
		}	
		if ( $ws_featureBoxShadow == 'shadow' ) {
			$ws_featureBoxShadowInset = of_get_option('ws_featureboxshadowinset');
			$ws_featureBoxShadowHorz = of_get_option('ws_featureboxshadowhorz');
			$ws_featureBoxShadowVert = of_get_option('ws_featureboxshadowvert');
			$ws_featureBoxShadowBlur = of_get_option('ws_featureboxshadowblur');
			$ws_featureBoxShadowSpread = of_get_option('ws_featureboxshadowspread');
			$ws_featureBoxShadowOpacity = of_get_option('ws_featureboxshadowopacity');
			if ( $ws_featureBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@featureBoxShadow', 'inset '.$ws_featureBoxShadowHorz.' '.$ws_featureBoxShadowVert.' '.$ws_featureBoxShadowBlur.' '.$ws_featureBoxShadowSpread.' rgba(0,0,0,'.$ws_featureBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@featureBoxShadow', ''.$ws_featureBoxShadowHorz.' '.$ws_featureBoxShadowVert.' '.$ws_featureBoxShadowBlur.' '.$ws_featureBoxShadowSpread.' rgba(0,0,0,'.$ws_featureBoxShadowOpacity.')');
			 }
		}
	
	$ws_featureBorderRadius = of_get_option('ws_featureborderradius');	
		if ( $ws_featureBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@featureBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@featureBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@featureBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@featureBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_featureBorderRadius == 'radius' ) {
			$ws_featureBorderRadiusTopLeft = of_get_option('ws_featureborderradiustopleft');
				$WPLessPlugin->addVariable('@featureBorderRadiusTopLeft', $ws_featureBorderRadiusTopLeft);
			$ws_featureBorderRadiusTopRight = of_get_option('ws_featureborderradiustopright');
				$WPLessPlugin->addVariable('@featureBorderRadiusTopRight', $ws_featureBorderRadiusTopRight);	
			$ws_featureBorderRadiusBtmLeft = of_get_option('ws_featureborderradiusbtmleft');
				$WPLessPlugin->addVariable('@featureBorderRadiusBtmLeft', $ws_featureBorderRadiusBtmLeft);
			$ws_featureBorderRadiusBtmRight = of_get_option('ws_featureborderradiusbtmright');
				$WPLessPlugin->addVariable('@featureBorderRadiusBtmRight', $ws_featureBorderRadiusBtmRight);	
		}

	$ws_featurePadding = of_get_option('ws_featurepadding');	
		if ( $ws_featurePadding == 'default' ) {
			$WPLessPlugin->addVariable('@featurePaddingTop', '20px');
			$WPLessPlugin->addVariable('@featurePaddingBtm', '20px');
			$WPLessPlugin->addVariable('@featurePaddingLeft', '0px');
			$WPLessPlugin->addVariable('@featurePaddingRight', '0px');
		}
		if ( $ws_featurePadding == 'padding' ) {
			$ws_featurePaddingTop = of_get_option('ws_featurepaddingtop');
				$WPLessPlugin->addVariable('@featurePaddingTop', $ws_featurePaddingTop);	
			$ws_featurePaddingBtm = of_get_option('ws_featurepaddingbtm');
				$WPLessPlugin->addVariable('@featurePaddingBtm', $ws_featurePaddingBtm);
			$ws_featurePaddingLeft = of_get_option('ws_featurepaddingleft');
				$WPLessPlugin->addVariable('@featurePaddingLeft', $ws_featurePaddingLeft);	
			$ws_featurePaddingRight = of_get_option('ws_featurepaddingright');
				$WPLessPlugin->addVariable('@featurePaddingRight', $ws_featurePaddingRight);	
		}
		
	$ws_featureMargin = of_get_option('ws_featuremargin');	
		if ( $ws_featureMargin == 'default' ) {
			$WPLessPlugin->addVariable('@featureMarginTop', '0px');
			$WPLessPlugin->addVariable('@featureMarginBtm', '0px');
			$WPLessPlugin->addVariable('@featureMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@featureMarginRight', 'auto');
		}
		if ( $ws_featureMargin == 'margin' ) {
			$ws_featureMarginTop = of_get_option('ws_featuremargintop');
				$WPLessPlugin->addVariable('@featureMarginTop', $ws_featureMarginTop);	
			$ws_featureMarginBtm = of_get_option('ws_featuremarginbtm');
				$WPLessPlugin->addVariable('@featureMarginBtm', $ws_featureMarginBtm);
			$ws_featureMarginLeft = of_get_option('ws_featuremarginleft');
				$WPLessPlugin->addVariable('@featureMarginLeft', $ws_featureMarginLeft);	
			$ws_featureMarginRight = of_get_option('ws_featuremarginright');
				$WPLessPlugin->addVariable('@featureMarginRight', $ws_featureMarginRight);	
		}	

// FEATURE - Background		
	$ws_featureOption = of_get_option('ws_featureoption');
	$ws_featureBackground = of_get_option('ws_featurebackground');
	$ws_featurePatternSheer = of_get_option('ws_featurepatternsheer');
		$ws_featurePatternSheerURL = "url('../img/patterns-sheer/$ws_featurePatternSheer')";
	$ws_featurePatternOpaque = of_get_option('ws_featurepatternopaque');
		$ws_featurePatternOpaqueURL = "url('../img/patterns-opaque/$ws_featurePatternOpaque')";	
	$ws_featureUpload = of_get_option('ws_featureupload');
		$ws_featureUploadURL = "url('$ws_featureUpload')";
	$ws_featureRepeat = of_get_option('ws_featurerepeat');
	$ws_featureAttach = of_get_option('ws_featureattach');
	
	$WPLessPlugin->addVariable('@featureGradient', $ws_featureBackground);
	
		if ( $ws_featureOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@featureBackground', 'transparent');
			$WPLessPlugin->addVariable('@featurePattern', 'none');
			$WPLessPlugin->addVariable('@featureRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@featureAttach', $ws_featureAttach);
		};
		if ( $ws_featureOption == 'color' || $ws_featureOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@featureBackground', $ws_featureBackground);
			$WPLessPlugin->addVariable('@featurePattern', 'none');
			$WPLessPlugin->addVariable('@featureRepeat', $ws_featureRepeat);
			$WPLessPlugin->addVariable('@featureAttach', $ws_featureAttach);
		};
		if ( $ws_featureOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@featureBackground', $ws_featureBackground);
			$WPLessPlugin->addVariable('@featurePattern', $ws_featurePatternSheerURL);
			$WPLessPlugin->addVariable('@featureRepeat', $ws_featureRepeat);
			$WPLessPlugin->addVariable('@featureAttach', $ws_featureAttach);
		};
		if ( $ws_featureOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@featureBackground', $ws_featureBackground);
			$WPLessPlugin->addVariable('@featurePattern', $ws_featurePatternOpaqueURL);
			$WPLessPlugin->addVariable('@featureRepeat', $ws_featureRepeat);
			$WPLessPlugin->addVariable('@featureAttach', $ws_featureAttach);
		};		
	    	if ( $ws_featureOption == 'uploadsheer' || $ws_featureOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@featureBackground', $ws_featureBackground);
			$WPLessPlugin->addVariable('@featurePattern', $ws_featureUploadURL);
			$WPLessPlugin->addVariable('@featureRepeat', $ws_featureRepeat);
			$WPLessPlugin->addVariable('@featureAttach', $ws_featureAttach);
		};						
					
// CONTENT - Display
	$ws_contentContainer = of_get_option('ws_contentcontainer');
		$WPLessPlugin->addVariable('@contentContainer', $ws_contentContainer);
		
	$ws_contentBoxShadow = of_get_option('ws_contentboxshadow');	
		if ( $ws_contentBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@contentBoxShadow', 'none');		
		}	
		if ( $ws_contentBoxShadow == 'shadow' ) {
			$ws_contentBoxShadowInset = of_get_option('ws_contentboxshadowinset');
			$ws_contentBoxShadowHorz = of_get_option('ws_contentboxshadowhorz');
			$ws_contentBoxShadowVert = of_get_option('ws_contentboxshadowvert');
			$ws_contentBoxShadowBlur = of_get_option('ws_contentboxshadowblur');
			$ws_contentBoxShadowSpread = of_get_option('ws_contentboxshadowspread');
			$ws_contentBoxShadowOpacity = of_get_option('ws_contentboxshadowopacity');
			if ( $ws_contentBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@contentBoxShadow', 'inset '.$ws_contentBoxShadowHorz.' '.$ws_contentBoxShadowVert.' '.$ws_contentBoxShadowBlur.' '.$ws_contentBoxShadowSpread.' rgba(0,0,0,'.$ws_contentBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@contentBoxShadow', ''.$ws_contentBoxShadowHorz.' '.$ws_contentBoxShadowVert.' '.$ws_contentBoxShadowBlur.' '.$ws_contentBoxShadowSpread.' rgba(0,0,0,'.$ws_contentBoxShadowOpacity.')');
			 }
		}
	
	$ws_contentBorderRadius = of_get_option('ws_contentborderradius');	
		if ( $ws_contentBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@contentBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@contentBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@contentBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@contentBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_contentBorderRadius == 'radius' ) {
			$ws_contentBorderRadiusTopLeft = of_get_option('ws_contentborderradiustopleft');
				$WPLessPlugin->addVariable('@contentBorderRadiusTopLeft', $ws_contentBorderRadiusTopLeft);
			$ws_contentBorderRadiusTopRight = of_get_option('ws_contentborderradiustopright');
				$WPLessPlugin->addVariable('@contentBorderRadiusTopRight', $ws_contentBorderRadiusTopRight);	
			$ws_contentBorderRadiusBtmLeft = of_get_option('ws_contentborderradiusbtmleft');
				$WPLessPlugin->addVariable('@contentBorderRadiusBtmLeft', $ws_contentBorderRadiusBtmLeft);
			$ws_contentBorderRadiusBtmRight = of_get_option('ws_contentborderradiusbtmright');
				$WPLessPlugin->addVariable('@contentBorderRadiusBtmRight', $ws_contentBorderRadiusBtmRight);	
		}

	$ws_contentPadding = of_get_option('ws_contentpadding');	
		if ( $ws_contentPadding == 'default' ) {
			$WPLessPlugin->addVariable('@contentPaddingTop', '0px');
			$WPLessPlugin->addVariable('@contentPaddingBtm', '0px');
			$WPLessPlugin->addVariable('@contentPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@contentPaddingRight', '0px');
		}
		if ( $ws_contentPadding == 'padding' ) {
			$ws_contentPaddingTop = of_get_option('ws_contentpaddingtop');
				$WPLessPlugin->addVariable('@contentPaddingTop', $ws_contentPaddingTop);	
			$ws_contentPaddingBtm = of_get_option('ws_contentpaddingbtm');
				$WPLessPlugin->addVariable('@contentPaddingBtm', $ws_contentPaddingBtm);
			$ws_contentPaddingLeft = of_get_option('ws_contentpaddingleft');
				$WPLessPlugin->addVariable('@contentPaddingLeft', $ws_contentPaddingLeft);	
			$ws_contentPaddingRight = of_get_option('ws_contentpaddingright');
				$WPLessPlugin->addVariable('@contentPaddingRight', $ws_contentPaddingRight);	
		}
		
	$ws_contentMargin = of_get_option('ws_contentmargin');	
		if ( $ws_contentMargin == 'default' ) {
			$WPLessPlugin->addVariable('@contentMarginTop', '0px');
			$WPLessPlugin->addVariable('@contentMarginBtm', '0px');
			$WPLessPlugin->addVariable('@contentMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@contentMarginRight', 'auto');
		}
		if ( $ws_contentMargin == 'margin' ) {
			$ws_contentMarginTop = of_get_option('ws_contentmargintop');
				$WPLessPlugin->addVariable('@contentMarginTop', $ws_contentMarginTop);	
			$ws_contentMarginBtm = of_get_option('ws_contentmarginbtm');
				$WPLessPlugin->addVariable('@contentMarginBtm', $ws_contentMarginBtm);
			$ws_contentMarginLeft = of_get_option('ws_contentmarginleft');
				$WPLessPlugin->addVariable('@contentMarginLeft', $ws_contentMarginLeft);	
			$ws_contentMarginRight = of_get_option('ws_contentmarginright');
				$WPLessPlugin->addVariable('@contentMarginRight', $ws_contentMarginRight);	
		}
		
// CONTENT - Background
	$ws_contentOption = of_get_option('ws_contentoption');
	$ws_contentBackground = of_get_option('ws_contentbackground');
	$ws_contentPatternSheer = of_get_option('ws_contentpatternsheer');
		$ws_contentPatternSheerURL = "url('../img/patterns-sheer/$ws_contentPatternSheer')";
	$ws_contentPatternOpaque = of_get_option('ws_contentpatternopaque');
		$ws_contentPatternOpaqueURL = "url('../img/patterns-opaque/$ws_contentPatternOpaque')";
	$ws_contentUpload = of_get_option('ws_contentupload');
		$ws_contentUploadURL = "url('$ws_contentUpload')";
	$ws_contentRepeat = of_get_option('ws_contentrepeat');
	$ws_contentAttach = of_get_option('ws_contentattach');
	
	$WPLessPlugin->addVariable('@contentGradient', $ws_contentBackground);
	
		if ( $ws_contentOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@contentBackground', 'transparent');
			$WPLessPlugin->addVariable('@contentPattern', 'none');
			$WPLessPlugin->addVariable('@contentRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@contentAttach', $ws_contentAttach);
		};
		if ( $ws_contentOption == 'color' || $ws_contentOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@contentBackground', $ws_contentBackground);
			$WPLessPlugin->addVariable('@contentPattern', 'none');
			$WPLessPlugin->addVariable('@contentRepeat', $ws_contentRepeat);
			$WPLessPlugin->addVariable('@contentAttach', $ws_contentAttach);
		}; 
		if ( $ws_contentOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@contentBackground', $ws_contentBackground);
			$WPLessPlugin->addVariable('@contentPattern', $ws_contentPatternSheerURL);
			$WPLessPlugin->addVariable('@contentRepeat', $ws_contentRepeat);
			$WPLessPlugin->addVariable('@contentAttach', $ws_contentAttach);
		};
		if ( $ws_contentOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@contentBackground', $ws_contentBackground);
			$WPLessPlugin->addVariable('@contentPattern', $ws_contentPatternOpaqueURL);
			$WPLessPlugin->addVariable('@contentRepeat', $ws_contentRepeat);
			$WPLessPlugin->addVariable('@contentAttach', $ws_contentAttach);
		};
	    	if ( $ws_contentOption == 'uploadsheer' || $ws_contentOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@contentBackground', $ws_contentBackground);
			$WPLessPlugin->addVariable('@contentPattern', $ws_contentUploadURL);
			$WPLessPlugin->addVariable('@contentRepeat', $ws_contentRepeat);
			$WPLessPlugin->addVariable('@contentAttach', $ws_contentAttach);
		};
		
// SIDEBAR
	$ws_sidebarOption = of_get_option('ws_sidebaroption');
	$ws_sidebarBackground = of_get_option('ws_sidebarbackground');
	$ws_sidebarPatternSheer = of_get_option('ws_sidebarpatternsheer');
		$ws_sidebarPatternSheerURL = "url('../img/patterns-sheer/$ws_sidebarPatternSheer')";
	$ws_sidebarPatternOpaque = of_get_option('ws_sidebarpatternopaque');
		$ws_sidebarPatternOpaqueURL = "url('../img/patterns-opaque/$ws_sidebarPatternOpaque')";
	$ws_sidebarUpload = of_get_option('ws_sidebarupload');
		$ws_sidebarUploadURL = "url('$ws_sidebarUpload')";
	$ws_sidebarRepeat = of_get_option('ws_sidebarrepeat');
	$ws_sidebarAttach = of_get_option('ws_sidebarattach');
	
	$WPLessPlugin->addVariable('@sidebarGradient', $ws_sidebarBackground);
	
		if ( $ws_sidebarOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@sidebarBackground', 'transparent');
			$WPLessPlugin->addVariable('@sidebarPattern', 'none');
			$WPLessPlugin->addVariable('@sidebarRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};
		if ( $ws_sidebarOption == 'color' || $ws_sidebarOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@sidebarBackground', $ws_sidebarBackground);
			$WPLessPlugin->addVariable('@sidebarPattern', 'none');
			$WPLessPlugin->addVariable('@sidebarRepeat', $ws_sidebarRepeat);
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		}; 
		if ( $ws_sidebarOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@sidebarBackground', $ws_sidebarBackground);
			$WPLessPlugin->addVariable('@sidebarPattern', $ws_sidebarPatternSheerURL);
			$WPLessPlugin->addVariable('@sidebarRepeat', $ws_sidebarRepeat);
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};
		if ( $ws_sidebarOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@sidebarBackground', $ws_sidebarBackground);
			$WPLessPlugin->addVariable('@sidebarPattern', $ws_sidebarPatternOpaqueURL);
			$WPLessPlugin->addVariable('@sidebarRepeat', $ws_sidebarRepeat);
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};
	    	if ( $ws_sidebarOption == 'uploadsheer' || $ws_sidebarOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@sidebarBackground', $ws_sidebarBackground);
			$WPLessPlugin->addVariable('@sidebarPattern', $ws_sidebarUploadURL);
			$WPLessPlugin->addVariable('@sidebarRepeat', $ws_sidebarRepeat);
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};
		if ( $ws_sidebarOption == 'auto' && $ws_contentOption != 'transparent' && $ws_contentOption != 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@sidebarBackground', 'darken(@contentBackground, 3%)');
			$WPLessPlugin->addVariable('@sidebarPattern', 'none');
			$WPLessPlugin->addVariable('@sidebarRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};
		if ( $ws_sidebarOption == 'auto' && $ws_contentOption == 'transparent' || $ws_contentOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@sidebarBackground', 'transparent');
			$WPLessPlugin->addVariable('@sidebarPattern', 'none');
			$WPLessPlugin->addVariable('@sidebarRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@sidebarAttach', $ws_sidebarAttach);
		};		
			
// FOOTER - Display
	//$ws_footerHeight = of_get_option('ws_footerheight');
		//$WPLessPlugin->addVariable('@footerHeight', $ws_footerHeight);
	$ws_footerContainer = of_get_option('ws_footercontainer');
		$WPLessPlugin->addVariable('@footerContainer', $ws_footerContainer);	
		
	$ws_footerBoxShadow = of_get_option('ws_footerboxshadow');	
		if ( $ws_footerBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@footerBoxShadow', 'none');		
		}	
		if ( $ws_footerBoxShadow == 'shadow' ) {
			$ws_footerBoxShadowInset = of_get_option('ws_footerboxshadowinset');
			$ws_footerBoxShadowHorz = of_get_option('ws_footerboxshadowhorz');
			$ws_footerBoxShadowVert = of_get_option('ws_footerboxshadowvert');
			$ws_footerBoxShadowBlur = of_get_option('ws_footerboxshadowblur');
			$ws_footerBoxShadowSpread = of_get_option('ws_footerboxshadowspread');
			$ws_footerBoxShadowOpacity = of_get_option('ws_footerboxshadowopacity');
			if ( $ws_footerBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@footerBoxShadow', 'inset '.$ws_footerBoxShadowHorz.' '.$ws_footerBoxShadowVert.' '.$ws_footerBoxShadowBlur.' '.$ws_footerBoxShadowSpread.' rgba(0,0,0,'.$ws_footerBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@footerBoxShadow', ''.$ws_footerBoxShadowHorz.' '.$ws_footerBoxShadowVert.' '.$ws_footerBoxShadowBlur.' '.$ws_footerBoxShadowSpread.' rgba(0,0,0,'.$ws_footerBoxShadowOpacity.')');
			 }
		}
	
	$ws_footerBorderRadius = of_get_option('ws_footerborderradius');	
		if ( $ws_footerBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@footerBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@footerBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@footerBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@footerBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_footerBorderRadius == 'radius' ) {
			$ws_footerBorderRadiusTopLeft = of_get_option('ws_footerborderradiustopleft');
				$WPLessPlugin->addVariable('@footerBorderRadiusTopLeft', $ws_footerBorderRadiusTopLeft);
			$ws_footerBorderRadiusTopRight = of_get_option('ws_footerborderradiustopright');
				$WPLessPlugin->addVariable('@footerBorderRadiusTopRight', $ws_footerBorderRadiusTopRight);	
			$ws_footerBorderRadiusBtmLeft = of_get_option('ws_footerborderradiusbtmleft');
				$WPLessPlugin->addVariable('@footerBorderRadiusBtmLeft', $ws_footerBorderRadiusBtmLeft);
			$ws_footerBorderRadiusBtmRight = of_get_option('ws_footerborderradiusbtmright');
				$WPLessPlugin->addVariable('@footerBorderRadiusBtmRight', $ws_footerBorderRadiusBtmRight);	
		}

	$ws_footerPadding = of_get_option('ws_footerpadding');	
		if ( $ws_footerPadding == 'default' ) {
			$WPLessPlugin->addVariable('@footerPaddingTop', '0px');
			$WPLessPlugin->addVariable('@footerPaddingBtm', '20px');
			$WPLessPlugin->addVariable('@footerPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@footerPaddingRight', '0px');
		}
		if ( $ws_footerPadding == 'padding' ) {
			$ws_footerPaddingTop = of_get_option('ws_footerpaddingtop');
				$WPLessPlugin->addVariable('@footerPaddingTop', $ws_footerPaddingTop);	
			$ws_footerPaddingBtm = of_get_option('ws_footerpaddingbtm');
				$WPLessPlugin->addVariable('@footerPaddingBtm', $ws_footerPaddingBtm);
			$ws_footerPaddingLeft = of_get_option('ws_footerpaddingleft');
				$WPLessPlugin->addVariable('@footerPaddingLeft', $ws_footerPaddingLeft);	
			$ws_footerPaddingRight = of_get_option('ws_footerpaddingright');
				$WPLessPlugin->addVariable('@footerPaddingRight', $ws_footerPaddingRight);	
		}
		
	$ws_footerMargin = of_get_option('ws_footermargin');	
		if ( $ws_footerMargin == 'default' ) {
			$WPLessPlugin->addVariable('@footerMarginTop', '0px');
			$WPLessPlugin->addVariable('@footerMarginBtm', '0px');
			$WPLessPlugin->addVariable('@footerMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@footerMarginRight', 'auto');
		}
		if ( $ws_footerMargin == 'margin' ) {
			$ws_footerMarginTop = of_get_option('ws_footermargintop');
				$WPLessPlugin->addVariable('@footerMarginTop', $ws_footerMarginTop);	
			$ws_footerMarginBtm = of_get_option('ws_footermarginbtm');
				$WPLessPlugin->addVariable('@footerMarginBtm', $ws_footerMarginBtm);
			$ws_footerMarginLeft = of_get_option('ws_footermarginleft');
				$WPLessPlugin->addVariable('@footerMarginLeft', $ws_footerMarginLeft);	
			$ws_footerMarginRight = of_get_option('ws_footermarginright');
				$WPLessPlugin->addVariable('@footerMarginRight', $ws_footerMarginRight);	
		}
		
// FOOTER - Typography
	$ws_footerColor = of_get_option('ws_footercolor');
		$WPLessPlugin->addVariable('@footerColor', $ws_footerColor);
	$ws_footerLinkColor = of_get_option('ws_footerlinkcolor');
		$WPLessPlugin->addVariable('@footerLinkColor', $ws_footerLinkColor);	
	$ws_footerHeadingsColor = of_get_option('ws_footerheadingscolor');
		$WPLessPlugin->addVariable('@footerHeadingsColor', $ws_footerHeadingsColor);		
			
// FOOTER	- Background
	$ws_footerOption = of_get_option('ws_footeroption');
	$ws_footerBackground = of_get_option('ws_footerbackground');
	$ws_footerPatternSheer = of_get_option('ws_footerpatternsheer');
		$ws_footerPatternSheerURL = "url('../img/patterns-sheer/$ws_footerPatternSheer')";
	$ws_footerPatternOpaque = of_get_option('ws_footerpatternopaque');
		$ws_footerPatternOpaqueURL = "url('../img/patterns-opaque/$ws_footerPatternOpaque')";
	$ws_footerUpload = of_get_option('ws_footerupload');
		$ws_footerUploadURL = "url('$ws_footerUpload')";
	$ws_footerRepeat = of_get_option('ws_footerrepeat');
	$ws_footerAttach = of_get_option('ws_footerattach');
	
	$WPLessPlugin->addVariable('@footerGradient', $ws_footerBackground);
	
		if ( $ws_footerOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@footerBackground', 'transparent');
			$WPLessPlugin->addVariable('@footerPattern', 'none');
			$WPLessPlugin->addVariable('@footerRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@footerAttach', $ws_footerAttach);
		};
		if ( $ws_footerOption == 'color' || $ws_footerOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@footerBackground', $ws_footerBackground);
			$WPLessPlugin->addVariable('@footerPattern', 'none');
			$WPLessPlugin->addVariable('@footerRepeat', $ws_footerRepeat);
			$WPLessPlugin->addVariable('@footerAttach', $ws_footerAttach);
		}; 
		if ( $ws_footerOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@footerBackground', $ws_footerBackground);
			$WPLessPlugin->addVariable('@footerPattern', $ws_footerPatternSheerURL);
			$WPLessPlugin->addVariable('@footerRepeat', $ws_footerRepeat);
			$WPLessPlugin->addVariable('@footerAttach', $ws_footerAttach);
		};
		if ( $ws_footerOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@footerBackground', $ws_footerBackground);
			$WPLessPlugin->addVariable('@footerPattern', $ws_footerPatternOpaqueURL);
			$WPLessPlugin->addVariable('@footerRepeat', $ws_footerRepeat);
			$WPLessPlugin->addVariable('@footerAttach', $ws_footerAttach);
		};
	    	if ( $ws_footerOption == 'uploadsheer' || $ws_footerOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@footerBackground', $ws_footerBackground);
			$WPLessPlugin->addVariable('@footerPattern', $ws_footerUploadURL);
			$WPLessPlugin->addVariable('@footerRepeat', $ws_footerRepeat);
			$WPLessPlugin->addVariable('@footerAttach', $ws_footerAttach);
		};	
		
// COLOPHON - Display
	//$ws_colophonHeight = of_get_option('ws_colophonheight');
		//$WPLessPlugin->addVariable('@colophonHeight', $ws_colophonHeight);
	$ws_colophonContainer = of_get_option('ws_colophoncontainer');
		$WPLessPlugin->addVariable('@colophonContainer', $ws_colophonContainer);	
		
	$ws_colophonBoxShadow = of_get_option('ws_colophonboxshadow');	
		if ( $ws_colophonBoxShadow == 'none' ) {
			$WPLessPlugin->addVariable('@colophonBoxShadow', 'none');		
		}	
		if ( $ws_colophonBoxShadow == 'shadow' ) {
			$ws_colophonBoxShadowInset = of_get_option('ws_colophonboxshadowinset');
			$ws_colophonBoxShadowHorz = of_get_option('ws_colophonboxshadowhorz');
			$ws_colophonBoxShadowVert = of_get_option('ws_colophonboxshadowvert');
			$ws_colophonBoxShadowBlur = of_get_option('ws_colophonboxshadowblur');
			$ws_colophonBoxShadowSpread = of_get_option('ws_colophonboxshadowspread');
			$ws_colophonBoxShadowOpacity = of_get_option('ws_colophonboxshadowopacity');
			if ( $ws_colophonBoxShadowInset == '1' ) { 
				$WPLessPlugin->addVariable('@colophonBoxShadow', 'inset '.$ws_colophonBoxShadowHorz.' '.$ws_colophonBoxShadowVert.' '.$ws_colophonBoxShadowBlur.' '.$ws_colophonBoxShadowSpread.' rgba(0,0,0,'.$ws_colophonBoxShadowOpacity.')');
			 } else { 
				$WPLessPlugin->addVariable('@colophonBoxShadow', ''.$ws_colophonBoxShadowHorz.' '.$ws_colophonBoxShadowVert.' '.$ws_colophonBoxShadowBlur.' '.$ws_colophonBoxShadowSpread.' rgba(0,0,0,'.$ws_colophonBoxShadowOpacity.')');
			 }
		}
	
	$ws_colophonBorderRadius = of_get_option('ws_colophonborderradius');	
		if ( $ws_colophonBorderRadius == 'none' ) {
			$WPLessPlugin->addVariable('@colophonBorderRadiusTopLeft', '0px');
			$WPLessPlugin->addVariable('@colophonBorderRadiusTopRight', '0px');
			$WPLessPlugin->addVariable('@colophonBorderRadiusBtmLeft', '0px');
			$WPLessPlugin->addVariable('@colophonBorderRadiusBtmRight', '0px');		
		}	
		if ( $ws_colophonBorderRadius == 'radius' ) {
			$ws_colophonBorderRadiusTopLeft = of_get_option('ws_colophonborderradiustopleft');
				$WPLessPlugin->addVariable('@colophonBorderRadiusTopLeft', $ws_colophonBorderRadiusTopLeft);
			$ws_colophonBorderRadiusTopRight = of_get_option('ws_colophonborderradiustopright');
				$WPLessPlugin->addVariable('@colophonBorderRadiusTopRight', $ws_colophonBorderRadiusTopRight);	
			$ws_colophonBorderRadiusBtmLeft = of_get_option('ws_colophonborderradiusbtmleft');
				$WPLessPlugin->addVariable('@colophonBorderRadiusBtmLeft', $ws_colophonBorderRadiusBtmLeft);
			$ws_colophonBorderRadiusBtmRight = of_get_option('ws_colophonborderradiusbtmright');
				$WPLessPlugin->addVariable('@colophonBorderRadiusBtmRight', $ws_colophonBorderRadiusBtmRight);	
		}

	$ws_colophonPadding = of_get_option('ws_colophonpadding');	
		if ( $ws_colophonPadding == 'default' ) {
			$WPLessPlugin->addVariable('@colophonPaddingTop', '30px');
			$WPLessPlugin->addVariable('@colophonPaddingBtm', '15px');
			$WPLessPlugin->addVariable('@colophonPaddingLeft', '0px');
			$WPLessPlugin->addVariable('@colophonPaddingRight', '0px');
		}
		if ( $ws_colophonPadding == 'padding' ) {
			$ws_colophonPaddingTop = of_get_option('ws_colophonpaddingtop');
				$WPLessPlugin->addVariable('@colophonPaddingTop', $ws_colophonPaddingTop);	
			$ws_colophonPaddingBtm = of_get_option('ws_colophonpaddingbtm');
				$WPLessPlugin->addVariable('@colophonPaddingBtm', $ws_colophonPaddingBtm);
			$ws_colophonPaddingLeft = of_get_option('ws_colophonpaddingleft');
				$WPLessPlugin->addVariable('@colophonPaddingLeft', $ws_colophonPaddingLeft);	
			$ws_colophonPaddingRight = of_get_option('ws_colophonpaddingright');
				$WPLessPlugin->addVariable('@colophonPaddingRight', $ws_colophonPaddingRight);	
		}
		
	$ws_colophonMargin = of_get_option('ws_colophonmargin');	
		if ( $ws_colophonMargin == 'default' ) {
			$WPLessPlugin->addVariable('@colophonMarginTop', '0px');
			$WPLessPlugin->addVariable('@colophonMarginBtm', '0px');
			$WPLessPlugin->addVariable('@colophonMarginLeft', 'auto');
			$WPLessPlugin->addVariable('@colophonMarginRight', 'auto');
		}
		if ( $ws_colophonMargin == 'margin' ) {
			$ws_colophonMarginTop = of_get_option('ws_colophonmargintop');
				$WPLessPlugin->addVariable('@colophonMarginTop', $ws_colophonMarginTop);	
			$ws_colophonMarginBtm = of_get_option('ws_colophonmarginbtm');
				$WPLessPlugin->addVariable('@colophonMarginBtm', $ws_colophonMarginBtm);
			$ws_colophonMarginLeft = of_get_option('ws_colophonmarginleft');
				$WPLessPlugin->addVariable('@colophonMarginLeft', $ws_colophonMarginLeft);	
			$ws_colophonMarginRight = of_get_option('ws_colophonmarginright');
				$WPLessPlugin->addVariable('@colophonMarginRight', $ws_colophonMarginRight);	
		}
		
// COLOPHON - Typography
	$ws_colophonColor = of_get_option('ws_colophoncolor');
		$WPLessPlugin->addVariable('@colophonColor', $ws_colophonColor);
	$ws_colophonLinkColor = of_get_option('ws_colophonlinkcolor');
		$WPLessPlugin->addVariable('@colophonLinkColor', $ws_colophonLinkColor);		
		
// COLOPHON - Background		
	$ws_colophonOption = of_get_option('ws_colophonoption');
	$ws_colophonBackground = of_get_option('ws_colophonbackground');
	$ws_colophonBackgroundOpacity = of_get_option('ws_colophonbackgroundopacity');
	$ws_colophonPatternSheer = of_get_option('ws_colophonpatternsheer');
		$ws_colophonPatternSheerURL = "url('../img/patterns-sheer/$ws_colophonPatternSheer')";
	$ws_colophonPatternOpaque = of_get_option('ws_colophonpatternopaque');
		$ws_colophonPatternOpaqueURL = "url('../img/patterns-opaque/$ws_colophonPatternOpaque')";
	$ws_colophonUpload = of_get_option('ws_colophonupload');
		$ws_colophonUploadURL = "url('$ws_colophonUpload')";
	$ws_colophonRepeat = of_get_option('ws_colophonrepeat');
	$ws_colophonAttach = of_get_option('ws_colophonattach');
	
	$WPLessPlugin->addVariable('@colophonGradient', $ws_colophonBackground);
	
		if ( $ws_colophonOption == 'transparent' ) {
			$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
	    		$WPLessPlugin->addVariable('@colophonBackgroundOpacity', '0');
			$WPLessPlugin->addVariable('@colophonPattern', 'none');
			$WPLessPlugin->addVariable('@colophonRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
		if ( $ws_colophonOption == 'color' || $ws_colophonOption == 'gradient' ) {
			$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
			$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', 'none');
			$WPLessPlugin->addVariable('@colophonRepeat', $ws_colophonRepeat);
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		}; 
		if ( $ws_colophonOption == 'patternsheer' ) {
			$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
			$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', $ws_colophonPatternSheerURL);
			$WPLessPlugin->addVariable('@colophonRepeat', $ws_colophonRepeat);
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
		if ( $ws_colophonOption == 'patternopaque' ) {
			$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
			$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', $ws_colophonPatternOpaqueURL);
			$WPLessPlugin->addVariable('@colophonRepeat', $ws_colophonRepeat);
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
	    	if ( $ws_colophonOption == 'uploadsheer' || $ws_colophonOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
	    		$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', $ws_colophonUploadURL);
			$WPLessPlugin->addVariable('@colophonRepeat', $ws_colophonRepeat);
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
		if ( $ws_colophonOption == 'auto' && $ws_footerOption != 'transparent' && $ws_footerOption != 'uploadopaque' && $ws_footerOption != 'patternopaque' ) {
	    		$WPLessPlugin->addVariable('@colophonBackground', 'darken(@footerBackground, 20%)');
	    		$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', 'none');
			$WPLessPlugin->addVariable('@colophonRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
		if ( $ws_colophonOption == 'auto' && $ws_footerOption == 'patternopaque' || $ws_footerOption == 'uploadopaque' ) {
	    		$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
	    		$WPLessPlugin->addVariable('@colophonBackgroundOpacity', $ws_colophonBackgroundOpacity);
			$WPLessPlugin->addVariable('@colophonPattern', 'none');
			$WPLessPlugin->addVariable('@colophonRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};	
		if ( $ws_colophonOption == 'auto' && $ws_footerOption == 'transparent' ) {
	    		$WPLessPlugin->addVariable('@colophonBackground', $ws_colophonBackground);
	    		$WPLessPlugin->addVariable('@colophonBackgroundOpacity', '0');
			$WPLessPlugin->addVariable('@colophonPattern', 'none');
			$WPLessPlugin->addVariable('@colophonRepeat', 'no-repeat');
			$WPLessPlugin->addVariable('@colophonAttach', $ws_colophonAttach);
		};
		
/* Enqueue the LESS files */
	wp_enqueue_style('ws_less_framework', 'ws.framework.less', array(), '1.0', 'screen,projection');
	wp_enqueue_style('ws_less_app_custom', 'ws.app.less', array(), '1.0', 'screen,projection');	
	add_action('wp_enqueue_scripts', array($WPLessPlugin, 'processStylesheets'));
	
}

?>