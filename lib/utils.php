<?php
/**
 * Theme wrapper
 *
 * @link http://scribu.net/wordpress/theme-wrappers.html
 */
function ws_template_path() {
  return WordStrap_Wrapping::$main_template;
}

function ws_sidebar_path() {
  return WordStrap_Wrapping::sidebar();
}

class WordStrap_Wrapping {
  // Stores the full path to the main template file
  static $main_template;

  // Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
  static $base;

  static function wrap($template) {
    self::$main_template = $template;

    self::$base = substr(basename(self::$main_template), 0, -4);

    if (self::$base === 'index') {
      self::$base = false;
    }

    $templates = array('base.php');

    if (self::$base) {
      array_unshift($templates, sprintf('base-%s.php', self::$base));
    }

    return locate_template($templates);
  }

  static function sidebar() {
    $templates = array('templates/sidebar.php');

    if (self::$base) {
      array_unshift($templates, sprintf('templates/sidebar-%s.php', self::$base));
    }

    return locate_template($templates);
  }
}
add_filter('template_include', array('WordStrap_Wrapping', 'wrap'), 99);

/**
 * Page titles
 */
function ws_title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      echo get_the_title(get_option('page_for_posts', true));
    } else {
      _e('Latest Posts', 'wordstrap');
    }
  } elseif (is_archive()) {
    	 $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    	 $tax = get_taxonomy( get_query_var('taxonomy') );
      if ($term) {
	   echo $tax->labels->name;
        echo ' Archive: ';
        echo $term->name;
    } elseif (is_post_type_archive()) {
      echo get_queried_object()->labels->name;
    } elseif (is_day()) {
      printf(__('Daily Archives: %s', 'wordstrap'), get_the_date());
    } elseif (is_month()) {
      printf(__('Monthly Archives: %s', 'wordstrap'), get_the_date('F Y'));
    } elseif (is_year()) {
      printf(__('Yearly Archives: %s', 'wordstrap'), get_the_date('Y'));
    } elseif (is_author()) {
    	 echo 'Author Archive: ';
	 $author = get_userdata( get_query_var('author') );
	 if ($author) {
      	echo $author->display_name;
      }
    } elseif (is_category()) {
    	 echo 'Topic Archive: ';
    	 single_cat_title();
    } elseif (is_tag()) {
    	 echo 'Tag Archive: ';
    	 echo '&#34;';
    	 single_tag_title();
    	 echo '&#34;';
    } else {
	 echo 'Archive';
    }
  } elseif (is_search()) {
    printf(__('Search Results for %s', 'wordstrap'), get_search_query());
  } elseif (is_404()) {
    _e('Not Found', 'wordstrap');
  } else {
    the_title();
  }
}

/**
 * Show an admin notice if .htaccess isn't writable
 */
function ws_htaccess_writable() {
  if (!is_writable(get_home_path() . '.htaccess')) {
    if (current_user_can('administrator')) {
      add_action('admin_notices', create_function('', "echo '<div class=\"error\"><p>" . sprintf(__('Please make sure your <a href="%s">.htaccess</a> file is writable ', 'wordstrap'), admin_url('options-permalink.php')) . "</p></div>';"));
    }
  }
}
if (current_theme_supports('h5bp-htaccess')) {
  add_action('admin_init', 'ws_htaccess_writable');
}

/**
 * Return WordPress subdirectory if applicable
 */
function wp_base_dir() {
  preg_match('!(https?://[^/|"]+)([^"]+)?!', site_url(), $matches);
  if (count($matches) === 3) {
    return end($matches);
  } else {
    return '';
  }
}

/**
 * Opposite of built in WP functions for trailing slashes
 */
function leadingslashit($string) {
  return '/' . unleadingslashit($string);
}

function unleadingslashit($string) {
  return ltrim($string, '/');
}

function add_filters($tags, $function) {
  foreach($tags as $tag) {
    add_filter($tag, $function);
  }
}

function is_element_empty($element) {
  $element = trim($element);
  return empty($element) ? false : true;
}