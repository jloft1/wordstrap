<?php get_template_part('templates/structure/head'); ?>
<body <?php body_class(); ?>>

<div id="wrap" <?php ws_wrap_class(); ?>>

<!--[if lt IE 7]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->
  
	<?php get_template_part('templates/structure/header'); ?>
	
	<?php get_template_part('templates/structure/content'); ?>
  
	<?php get_template_part('templates/structure/footer'); ?>
	
</div><!-- /.wrap -->	

</body>
</html>