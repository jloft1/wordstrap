### 2.5.3: 2013 - 03 -24

FIX assets/less/ws.wordstrap.less
Most of the gradient mixin references were missing the final ";" at the end of their properties.

### 2.5.2: 2013 - 03 -24

This commit attempt to cleanup and fix some flaws in the jQuery "affix" feature's implementation.

The flash of unstyled content problem may be fixed. I used the method found here:
http://www.robertmullaney.com/2011/08/29/prevent-flash-of-unstyled-content-fouc-jquery/

It is rather simple. I just added some basic javascript to the <head>.

	<script type="text/javascript">
		$(function() {
			$('.wait-till-load').hide();
			$(window).load(function(){
				$('.wait-till-load').show();
			});
		});
	</script>

The .wait-till-load class was added to the navbar via the filter/hook in the custom.php file.

I also edited the formula for calculating the offset for the affix property. The header top margin was missing.

One thing to watch out for regarding the options panel's interaction with the offset formula…
It seems that even if an HTML element has its padding or margin set in the control panel to use the defaults, the formula is reading what ever value is in the margin and padding fields to make the calculation. It is important that these are 0px when not actually being used.

In the ws.wordstrap.less file, I added styling for the .navbar.affix .navbar-inner.x class, so some of the odd positioning and styling applied by the .contain and .fullwidth classes get cleaned out. I decided to remove the border-radius altogether for fixed/pinned nabbers when they are actually pinned to the top of the browser.

.navbar.affix .navbar-inner.contain, .navbar.affix .navbar-inner.fullwidth { width:100%; margin:0; padding:0; position:relative; left:auto; right:auto; .border-radius(0px); }

I added the ability for themes to include javascript directly into the <head> or in the footer just before the closing </body> tag. One file (javascript-head.php) is called into the head.php file. The other file (javascript-footer.php) is called into the footer.php file. If they are empty, then it just sends no data into the template and works fine.

### 2.5.1: 2013 - 03 -24

Added type="text/javascript" to the affix script call in templates/structure/footer.php.

### 2.5.0: 2013 - 03 -24

Added templates/javascript/javascipt.php
Added to templates/structure/footer.php...
<?php get_template_part('templates/javascript/javascript'); ?>

By adding the javascript template to the footer, child themes may easily add scripts to the theme.

### 2.4.1: 2013 - 03 -24

This is a patch for 2.4. It is another commit related to the .fullwidth vs. .contain classes for structural HTML elements.

The previous commit worked fine in cases where the site's design called for all structural elements to be uniform… either fullwidth or contained. And it actually required the .wrap element to be contained in order for the .contain state to work properly on all elements in terms of the absolute centering.

This commit includes more complicated code that must be repeated for each media query. It targets each structural element with the .contain class and does the full calculation of width, left-margin, and left absolute positioning that we are doing on the .wrap.

One fix that was required for the .banner since the .masthead and .navbar-inner are nested within it. The .banner needed to receive the calculated width that is inclusive of the padding dimensions and then be set to no left/right padding. That way, the .masthead and .navbar-inner content may be offset left/right from the .banner boundaries.

The end result of this commit is that each structural element may operate independently in terms of fullwidth vs. containment. That allows for more design options.

### 2.4.0: 2013 - 03 -23

This is an improvement to the responsive design and the centering of the site content. Most of the structural divs will get one of two classes designating whether is spans the full width (.fullwidth) of the browser or if it should function as a container (.contain) and reveal the body around it (meaning it may be offset from the browser edges). Here is a list of the structural divs:

.banner
.masthead
.navbar-inner
.feature
.content
.superfooter
.colophon

When their class is set to (.contain), the following elements receive the @wrapPaddingLeft and @wrapPaddingRight variables that are set via the WP admin panel: (.banner, .masthead, .navbar-inner, .feature, .content, .superfooter, .colophon)

But when those elements have the (.fullwidth) class, then the padding left/right are set to 0px as no padding is necessary since there are no borders left/right that the content must be offset from. We use these variables:

@wrapPaddingRight0: 0px;
@wrapPaddingLeft0: 0px;

The .wrap div receives special treatment. Here's its CSS for the .fullwidth class:

.fullwidth.wrap { width:100%; margin-left:-50%; position:absolute; left:50%;

The auto padding combined with setting the .wrap as absolutely positioned allows the wrap to remain centered and flow off the left of the browser as needed. (If any of the other structural elements have padding left/right greater than zero, then this trick doesn't work).

Basically, we want to do this so that any imperfections in our @media queries are somewhat hidden. The content will always stay centered as the browser width is adjusted even as the browser starts to get smaller than the site's width itself. Thus, the left/right padding (by all appearances) is flexible and may collapse as needed to keep the content centered. The @media query breakpoints are set so that the content will never disappear from view on the left.

In order to accomplish this same trick when the .wrap functions as a container and has the .contain class, we must use variables to calculate everything.

@wrapPaddingRight30: 30px;
@wrapPaddingLeft30: 30px;
@1200base: (@gridRowWidth1200);
@1200pad:(@1200base + @wrapPaddingLeft30 + @wrapPaddingRight30);

.contain.wrap { width: @1200pad; margin-left: ( (@1200pad / 2) - (@1200pad) ); position:absolute; left:50%; }

This must be recalculated using the appropriate variables at each media query break point.

After the media queries hit @media (max-width:979px), then the padding left/right may be (and should be) explicitly set for the structural items regardless of class. That's because even site designs that include contained elements offset from the browser edges become full width at 767px. Thus, we want all content to have a hard-set left and right offset from the browser edges.


### 2.3.0: 2013 - 03 -23

* Added page templates for full width and no breadcrumb iterations.

### 2.2.0: 2013 - 03 -23

* Fixed the styles so that they are flexible enough to accommodate full-width and contain-width designs.

### 2.1.0: 2013 - 03 -23

* Overhaul the media queries: They are now greatly simplified and rely better on BootStrap. The break points are now just 1200, 980, 768, and 480.
* Removed comments from the page templates by default. Now comments can be added via an includes template part in the structure folder.
* Moved the loop start and end in some cases back to within the entry rather than wrapping the whole page in the loop.