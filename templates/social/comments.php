<?php ws_comment_button_classes();
// Do not delete these lines
  if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');

  if ( post_password_required() ) { ?>
  	<div class="alert alert-primary"><?php _e("This post is password protected. Enter the password to view comments.","wordstrap"); ?></div>
  <?php
    return;
  }
?>

<!-- You can start editing here. -->
<hr>
<div id="comments" class="comment-container">


<?php if ( have_comments() ) : ?>

	<h3 class="comments">
	<i class="icon-comments"></i>

	<?php
	if (function_exists('is_product')) {
		if (is_product()) {
		comments_number('<span>' . __("No","wordstrap") . '</span> ' . __("Reviews","wordstrap") . '', '<span>' . __("One","wordstrap") . '</span> ' . __("Review","wordstrap") . '', '<span>%</span> ' . __("Reviews","wordstrap") );?> <?php _e("for","wordstrap"); ?> &#8220;<?php the_title(); ?>&#8221;
		<?php }
		else {
		comments_number('<span>' . __("No","wordstrap") . '</span> ' . __("Responses","wordstrap") . '', '<span>' . __("One","wordstrap") . '</span> ' . __("Response","wordstrap") . '', '<span>%</span> ' . __("Responses","wordstrap") );?> <?php _e("to","wordstrap"); ?> &#8220;<?php the_title(); ?>&#8221;
	<?php }
		} else {
		comments_number('<span>' . __("No","wordstrap") . '</span> ' . __("Responses","wordstrap") . '', '<span>' . __("One","wordstrap") . '</span> ' . __("Response","wordstrap") . '', '<span>%</span> ' . __("Responses","wordstrap") );?> <?php _e("to","wordstrap"); ?> &#8220;<?php the_title(); ?>&#8221;
	<?php } ?>

	</h3><br>

	<nav class="comment-nav">
		<ul class="clearfix">
	  		<li><?php previous_comments_link( __("Older comments","wordstrap") ) ?></li>
	  		<li><?php next_comments_link( __("Newer comments","wordstrap") ) ?></li>
	 	</ul>
	</nav>

	<ol class="commentlist">
		<?php wp_list_comments('type=comment&callback=ws_comments'); ?>
	</ol>

	<nav class="comment-nav">
		<ul class="clearfix">
	  		<li><?php previous_comments_link( __("Older comments","wordstrap") ) ?></li>
	  		<li><?php next_comments_link( __("Newer comments","wordstrap") ) ?></li>
		</ul>
	</nav>

<?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
    	<!-- If comments are open, but there are no comments. -->

	<?php else : ?>
	<!-- If comments are closed. -->

	<?php endif; // coments_open ?>

<?php endif; // have comments ?>


<?php if ( comments_open() ) : ?>

	<section class="respond respond-form well">

		<h3 class="comment-form-title">
		<?php
		if (function_exists('is_product')) {
			if (is_product()) {
			comment_form_title( __("Review this product","wordstrap"), __("Leave a Reply to","wordstrap") . ' %s' );
			} else {comment_form_title( __("Leave a Reply","wordstrap"), __("Leave a Reply to","wordstrap") . ' %s' );}

		} else {comment_form_title( __("Leave a Reply","wordstrap"), __("Leave a Reply to","wordstrap") . ' %s' );} ?>
		</h3>

		<div class="cancel-comment-reply">
			<p class="small"><?php cancel_comment_reply_link( __("Cancel","wordstrap") ); ?></p>
		</div>

		<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>

		  	<div class="help">
		  		<p><?php _e("You must be","wordstrap"); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e("logged in","wordstrap"); ?></a> <?php _e("to post a comment","wordstrap"); ?>.</p>
		  	</div>

		<?php else : ?>

			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="form-vertical commentform">

			<?php if ( is_user_logged_in() ) : ?>

				<p class="comments-logged-in-as"><?php _e("Logged in as","wordstrap"); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e("Log out of this account","wordstrap"); ?>"><?php _e("Log out","wordstrap"); ?> &raquo;</a></p>

			<?php else : ?>

				<ul class="comment-form-elements clearfix">

					<li>
						<div class="control-group">
						  <label for="author"><?php _e("Name","wordstrap"); ?> <?php if ($req) echo "(required)"; ?></label>
						  <div class="input-prepend">
						  	<span class="add-on"><i class="icon-user"></i></span>
						  	<input type="text" name="author" class="author" value="<?php echo esc_attr($comment_author); ?>" placeholder="<?php _e("Your Name","wordstrap"); ?>" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
						  </div>
					  	</div>
					</li>

					<li>
						<div class="control-group">
						  <label for="email"><?php _e("Mail","wordstrap"); ?> <?php if ($req) echo "(required)"; ?></label>
						  <div class="input-prepend">
						  	<span class="add-on"><i class="icon-envelope-alt"></i></span>
						  	<input type="email" name="email" class="email" value="<?php echo esc_attr($comment_author_email); ?>" placeholder="<?php _e("Your Email","wordstrap"); ?>" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
						  	<span class="help-inline">(<?php _e("will not be published","wordstrap"); ?>)</span>
						  </div>
					  	</div>
					</li>

					<li>
						<div class="control-group">
						  <label for="url"><?php _e("Website","wordstrap"); ?></label>
						  <div class="input-prepend">
						  <span class="add-on"><i class="icon-home"></i></span>
						  	<input type="url" name="url" class="url" value="<?php echo esc_attr($comment_author_url); ?>" placeholder="<?php _e("Your Website","wordstrap"); ?>" tabindex="3" />
						  </div>
					  	</div>
					</li>

				</ul>

			<?php endif; // If registration required and user is logged in ?>

			<div class="clearfix">
				<div class="input">
					<textarea name="comment" class="comment" rows="8" placeholder="<?php _e("Your Comment Here...","wordstrap"); ?>" tabindex="4"></textarea>
				</div>
			</div>

			<input class="btn submitform <?php ws_button_class();?>" name="submit" type="submit" tabindex="5" value="<?php _e("Submit Comment","wordstrap"); ?>" />
			<?php comment_id_fields(); ?>

			<?php
				//comment_form();
				do_action('comment_form()', $post->ID);

			?>

			<div class="clearfix"></div>

			</form>

		<?php endif; // If registration required and user is not logged in ?>

	</section> <!-- /.respond -->

<?php endif; // if comments open ?>


</div> <!-- /.comment-container -->


