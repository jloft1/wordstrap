<div class="entry-meta well well-small">
	<div class="entry-date pull-left">
		<i class="icon-calendar"></i>&nbsp;
		<span class="prefix">POSTED: </span>
		<time class="updated" datetime="<?php echo get_the_time('c'); ?>">
			<?php echo sprintf(__('%s', 'wordstrap'), get_the_date(), get_the_time()); ?>
		</time>
	</div>
	<div class="entry-author vcard pull-left">
		<i class="icon-user"></i>&nbsp;
		<span class="prefix">AUTHOR: </span>
		<?php echo __('', 'wordstrap'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?php the_author_meta('display_name'); ?></a>
	</div>
	<?php if( current_user_can( 'edit_posts' ) ) { ?>
	<div class="entry-edit pull-right">
		<?php edit_post_link('EDIT','<i class="icon-cog"></i>&nbsp;<span class="prefix">','</span>'); ?>
	</div>
	<?php } ?>
</div>
