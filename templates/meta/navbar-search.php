<form role="search" method="get" class="navbar-search searchform" action="<?php echo home_url('/'); ?>">
  <label class="hide" for="s"><?php _e('Search for:', 'wordstrap'); ?></label>
  <div class="search-wrap">
  <input type="text" value="<?php if (is_search()) { echo get_search_query(); } ?>" name="s" id="s" class="search-query" placeholder="<?php _e('Search', 'wordstrap'); ?> <?php bloginfo('name'); ?>">
  <button class="btn"><i class="icon-search"></i></button>
  </div>
</form>