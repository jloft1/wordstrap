<div class="entry-meta well well-small">
	<div class="entry-categories pull-left">
		<i class="icon-folder-close"></i>&nbsp;	
		<span class="prefix">TOPIC(S): </span>
		<?php the_category(' &bull; ',''); ?>
	</div>
	<div class="entry-tags pull-left">
		<?php the_tags('<i class="icon-tags"></i>&nbsp;<span class="prefix">TAG(S): </span>',' &bull; ',''); ?>
	</div>	
</div>