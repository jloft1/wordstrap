<?php get_template_part('templates/structure/feature-nosubtitle'); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">

		<?php if (!have_posts()) : ?>
			<div class="alert alert-block fade in">
				<a class="close" data-dismiss="alert">&times;</a>
				<p><?php _e('Sorry, no results were found.', 'wordstrap'); ?></p>
			</div><!-- /.alert -->
			<?php get_search_form(); ?>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header>
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<?php get_template_part('templates/meta/archive-meta'); ?>
				</header>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- /.entry-summary -->
				<a href="<?php the_permalink(); ?>" title="READ MORE of <?php the_title(); ?>" class="btn readmore">Read More</a>
			</article><!-- /article -->

			<hr>

		<?php endwhile; ?>

		<?php if ($wp_query->max_num_pages > 1) : ?>
			<nav class="post-nav">
				<ul class="pager">
					<?php if (get_next_posts_link()) : ?>
						<li class="previous"><?php next_posts_link(__('&larr; Older posts', 'wordstrap')); ?></li>
					<?php else: ?>
						<li class="previous disabled"><a><?php _e('&larr; Older posts', 'wordstrap'); ?></a></li>
					<?php endif; ?>
					<?php if (get_previous_posts_link()) : ?>
						<li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'wordstrap')); ?></li>
					<?php else: ?>
						<li class="next disabled"><a><?php _e('Newer posts &rarr;', 'wordstrap'); ?></a></li>
					<?php endif; ?>
				</ul>
			</nav><!-- /.post-nav -->
		<?php endif; ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/sidebar'); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>