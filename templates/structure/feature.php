<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo ws_title(); ?></span><br>
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<span class="feature-subtitle"><?php the_subtitle(); ?></span>
						<?php }
					} ?>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>