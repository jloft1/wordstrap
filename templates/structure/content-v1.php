<?php tha_content_before(); ?>
<div id="content" <?php ws_content_class(); ?> role="document">
	<div class="container">
		<div class="row">
			<?php tha_content_top(); ?>
				<?php include ws_template_path(); ?>
			<?php tha_content_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.content -->
<?php tha_content_after(); ?>